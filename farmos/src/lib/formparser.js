/* eslint-disable no-continue */
/* eslint-disable no-plusplus */

export function tillageParser(app) {
  const ret = [];
  for (let i = 1; i < 4; i++) {
    const type = app.getAnswer(`tillage/type_${i}`);
    if (!type) {
      continue;
    }

    const date = app.getAnswer(`tillage/date_${i}`);
    if (!date) {
      throw new Error(`empty date of tillage ${i}`);
    }

    const depth = app.getAnswer(`tillage/depth_${i}`);
    if (!depth) {
      throw new Error(`empty depth of tillage ${i}`);
    }

    const tillage = {
      type,
      date,
      depth,
    };
    ret.push(tillage);
  }

  return ret;
}

export function amendmentParser(app) {
  const ret = [];
  for (let i = 1; i < 4; i++) {
    const name = app.getAnswer(`amendments/name_${i}`);
    if (!name) {
      continue;
    }

    const date = app.getAnswer(`amendments/amendment_date_${i}`);
    if (!date) {
      throw new Error(`empty date of amendment ${i}`);
    }

    const method = app.getAnswer(`amendments/method_${i}`);
    if (!method) {
      throw new Error(`empty method of amendment ${i}`);
    }

    const nutrients = app.getAnswer(`amendments/nutrients_${i}`);
    const n = app.getAnswer(`amendments/n_${i}`);
    const p = app.getAnswer(`amendments/p_${i}`);
    const k = app.getAnswer(`amendments/k_${i}`);
    const trace = app.getAnswer(`amendments/nutrients_trace_${i}`);

    const amendment = {
      name,
      date,
      method,
      nutrients,
      n,
      p,
      k,
      trace,
    };
    ret.push(amendment);
  }

  return ret;
}

export function weeklyAmendmentParser(app) {
  const ret = [];
  for (let i = 1; i < 4; i++) {
    const name = app.getAnswer(`amendments/name_${i}`);
    if (!name) {
      continue;
    }

    const nutrients = app.getAnswer(`amendments/nutrients_${i}`);
    const n = app.getAnswer(`amendments/n_${i}`);
    const p = app.getAnswer(`amendments/p_${i}`);
    const k = app.getAnswer(`amendments/k_${i}`);
    const trace = app.getAnswer(`amendments/nutrients_trace_${i}`);

    const amendment = {
      name,
      nutrients,
      n,
      p,
      k,
      trace,
    };
    ret.push(amendment);
  }

  return ret;
}
