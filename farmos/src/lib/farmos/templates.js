function log({
  timestamp,
  logName,
  type,
  fieldId,
  assetId,
  note: { content, format = 'plain_text' } = {},
  data,
  done = 1,
  movementFieldId,
  logCategory,
} = {}) {
  if (!logName) {
    throw Error('name missing');
  }
  if (!timestamp) {
    throw Error('timestamp missing');
  }

  const obj = {
    name: logName,
    timestamp,
    done,
  };

  Object.assign(obj, fieldId ? { area: [{ id: fieldId }] } : undefined);
  Object.assign(obj, assetId ? { asset: [{ id: assetId }] } : undefined);
  Object.assign(obj, logCategory ? { log_category: [{ id: logCategory }] } : undefined);
  // Object.assign(obj, data ? { data } : undefined);
  Object.assign(
    obj,
    movementFieldId ? { movement: { area: [{ id: movementFieldId }] } } : undefined,
  );

  Object.assign(obj, content ? { notes: { value: content, format } } : undefined);
  Object.assign(obj, type ? { type } : undefined);
  Object.assign(obj, data ? { data: JSON.stringify(data) } : undefined);

  return obj;
}

export function taxonomyTerm(vocabularyId, name, description) {
  return {
    name,
    description,
    vocabulary: {
      id: vocabularyId,
    },
  };
}

export function input(
  timestamp,
  note,
  name,
  materialId,
  fieldId,
  method,
  source,
  purpose,
  assetId,
  quantities,
  data,
) {
  const r = log({
    timestamp,
    logName: name,
    type: 'farm_input',
    fieldId,
    assetId,
    note: {
      content: note,
      format: 'plain_text',
    },
    done: 1,
    data,
  });

  Object.assign(r, { material: [{ id: materialId }] });
  Object.assign(r, quantities ? { quantity: quantities } : undefined);
  Object.assign(r, purpose ? { input_purpose: purpose } : undefined);
  Object.assign(r, method ? { input_method: method } : undefined);
  Object.assign(r, source ? { input_source: source } : undefined);

  return r;
}

export function transplanting(fieldId, name, assetId, instanceId, timestamp) {
  const r = log({
    timestamp,
    logName: name,
    type: 'farm_transplanting',
    done: 1,
    assetId,
    movementFieldId: fieldId,
    data: {
      instanceId,
    },
  });

  return r;
}

export function planting(name, crop, cropId, instanceId) {
  return {
    name,
    type: 'planting',
    crop: [
      {
        id: cropId,
      },
    ],
    data: JSON.stringify({
      instanceId,
    }),
  };
}

export function seeding(cropName, assetId, fieldId, timestamp, instanceId) {
  const r = log({
    logName: cropName,
    movementFieldId: fieldId,
    type: 'farm_seeding',
    assetId,
    timestamp,
    done: 1,
    data: {
      instanceId,
    },
  });
  return r;
}

export function tillage(
  timestamp,
  type,
  depth,
  fieldId,
  tillageCsv,
  logCategory,
  inchesId,
  assetId,
  instanceId,
) {
  const stir = tillageCsv.find(c => c[0] === type);
  if (!stir) {
    throw new Error(`cannot find stir value for: ${type}`);
  }

  const [, name, stirValue] = stir;
  const r = log({
    timestamp,
    logName: name,
    type: 'farm_activity',
    done: 1,
    fieldId,
    assetId,
    logCategory,
    data: [
      {
        instanceId,
      },
    ],
  });
  Object.assign(r, {
    quantity: [
      {
        measure: 'length',
        value: depth,
        unit: {
          id: inchesId,
        },
        label: 'Depth',
      },

      {
        measure: 'rating',
        value: stirValue,
        label: 'STIR',
      },
    ],
  });

  return r;
}
