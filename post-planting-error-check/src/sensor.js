/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';
// import serial from './lib/serial';
import { app, serial } from '@oursci/scripts'; // expose this to android for calling onDataAvailable
import surveyHelper from '../.oursci-surveys/survey';
import errorCheck from './errorcheck';

// import sendDevice from './lib/sendDevice';
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

export default serial;

const uuidSnippet = '28c68e37';
const form = 'post-planting-anon-test';
if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

export const survey = surveyHelper();

const result = {
  errors: [],
};

export function err(msg) {
  result.errors.push(msg);
}
export const regex = {
  anyOptional: /^.*$/,
  any: /^.+$/,
  number: /^\d+$/,
  numberOptional: /^\d*$/,
  date: /^\d{4}-\d{2}-\d{2}$/,
  optionalDate: /^(\d{4}-\d{2}-\d{2})|()$/,
  month_year: /^\d{4}-\d{2}$/,
  field_id: /^.*\(server: (\d+), tid: (\d+)\)$/,
};
errorCheck();

if (result.errors.length > 0) {
  app.error();
}

app.result(result);
