/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import sleep from './lib/utils'; // use: await sleep(1000);

import axios from 'axios';

/*
import {
  xhr
} from 'winjs';
*/

export default serial; // expose this to android for calling onDataAvailable

const FARMOS_BASE_URL = 'https://goatfarm.farmos.net';

// const postData = async (url = '', data = {}) => {
//   // Default options are marked with *
//   return fetch(url, {
//     method: 'POST', // *GET, POST, PUT, DELETE, etc.
//     mode: 'cors', // no-cors, cors, *same-origin
//     cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//     credentials: 'same-origin', // include, *same-origin, omit
//     headers: {
//       'Content-Type': 'application/json; charset=utf-8',
//       // 'Content-Type': 'application/x-www-form-urlencoded',
//     },
//     redirect: 'follow', // manual, *follow, error
//     referrer: 'no-referrer', // no-referrer, *client
//     body: JSON.stringify(data), // body data type must match "Content-Type" header
//   }).then(response => response.json()); // parses response to JSON
// };

const generateData = (nEntries, dcOffset = 0) => {
  return Array.from(Array(nEntries), (x) => Math.random().toFixed(4) * 100).map((y) => dcOffset + y);
};

(async () => {
  try {
    const scanData = JSON.parse(app.getAnswer('scan')).data;
    const [lat, lon] = app.getAnswer('location').split(';');
    console.log(app.getAnswer('location'));
    const quantityPayload = {
      measure: 'weight',
      value: scanData.median_940,
      units: 'lbs',
      label: 'approximately 1 butt-load of GOAT manure',
    };

    // const quantityId = await postData(`${FARMOS_BASE_URL}/quantity`, quantityPayload);
    const logPayload = {
      timestamp: Math.floor(Date.now() / 1e3),
      title: 'GOAT log',
      type: 'idk',
      quantities: [
        quantityPayload,
      ],
      location: {
        lat,
        lon,
      },
    };
    console.log(scanData.median_940);
    const logResponse = await axios.post(`${FARMOS_BASE_URL}/api/oursci`, logPayload);
    const result = {
      timestamp: Math.floor(Date.now() / 1e3),
      message: 'success',
    };
    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();

// list of all scripts saved to app.our-sci.net based on this script (only changing the protocol sent to device in sensor.js)

/*
"id": "environmental_only",
"name": "Environmental measurements only",
"description": "Measure temperature, pressure, humidity, and VOCs only",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-3",
"name": "Reflectometer standard 3",
"description": "Standard reflectometer measurement for field and lab use",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardBlank-3",
"name": "Reflectometer blank 3",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateBlankLocal-3",
"name": "Reflectometer blank saved as local variable",
"description": "Standard reflectomter blank offset which saves result to the local Environment variable. standard measurement uses if found, then resets.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRaw-3",
"name": "Reflectometer raw 3",
"description": "outputs raw data only, use standard reflectometer measurement for normal use",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawNoHeat-3",
"name": "Reflectometer raw no heat calibration 3",
"description": "outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawHeatCal-3",
"name": "Reflectometer raw used for heat calibration 3",
"description": "outputs raw data only without heat (voltage) adjustement, repeats 12 times over 24 minutes.  Use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardHeatTest-3",
"name": "Reflectometer test of 12 measurements over a range of measurements",
"description": "Used to test the heat calibration StandardRawHeatCal.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhite-3",
"name": "Reflectometer calibration white 3",
"description": "Calibrate a device to user calibration card, using the white square.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlack-3",
"name": "Reflectometer calibration black 3",
"description": "Calibrate a device to user calibration card, using the black square.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlank-3",
"name": "Reflectometer calibration blank 3",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhiteMaster-3",
"name": "Reflectometer calibration white master 3",
"description": "Calibrate the master device to master calibration card, using the white square.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMaster-3",
"name": "Reflectometer calibration black master 3",
"description": "Calibrate the master device to master calibration card, using the black square.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMasterMaster-3",
"name": "Reflectometer Master device - Master card calibration 3",
"description": "ONLY used to calibrate the master card.  Force saves 0 / 100 values to the device.",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-firmware-1-18",
"name": "Reflectometer measurement firmware 1.18",
"description": "only for beta devices on old firmware 1.18",
"version": "2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "heat-cal",
"name": "Heat Calibration",
"description": "Calibrate devices for different temperatures (3 hour test)",
"version": "1.0",
"action": "Run Calibration",
"requireDevice": "true"
*/

/*
"id": "standard-chlorophyll-3",
"name": "Chlorophyll standard 3",
"description": "Chlorophyll content using reflectometer for field and lab use",
"version": "3.0.0",
"action": "Run Measurement",
"requireDevice": "true"
  */