const path = require("path");
const webpack = require("webpack");
const fs = require("fs-cli");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WebpackDevServer = require("webpack-dev-server");

externals = {
  serialport: "SerialPort",
  fs: "fs"
};

var config = {
  entry: {
    app: ["./src/processor.js"]
  },
  devtool: "inline-source-map",
  output: {
    path: path.resolve(__dirname, "public/"),
    filename: "processor-browser-debug.js"
  },
  externals: externals,
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ["url-loader?limit=10000&mimetype=application/font-woff"]
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: ["file-loader"]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
};

options = {
  contentBase: "public",
  historyApiFallback: true,
  host: "127.0.0.1"
};

WebpackDevServer.addDevServerEntrypoints(config, options);
var compiler = webpack(config);

const devServerOptions = Object.assign({}, options, {
  stats: {
    colors: true
  }
});

//compiler.run((err, status) => {});
const server = new WebpackDevServer(compiler, devServerOptions);

server.listen(9091, "127.0.0.1", function() {
  console.log("Starting server on http://localhost:9091");
});
