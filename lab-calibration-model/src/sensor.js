/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

const result = {};

// answers which come from normal survey answers
result.test = app.getAnswer('test');

// answers which come from measurement scripts only
const requiredAnswers = [
  //  'test',
  'blank',
  'standard1',
  'standard2',
  'standard3',
  'standard4',
  'standard5',
  'standard6',
  'standard7',
];

(async () => {
  requiredAnswers.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });
  console.log(result);
  app.result(result);
})();