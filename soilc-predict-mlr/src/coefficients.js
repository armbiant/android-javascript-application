// Coefficients for linear regression model
const linear_intercept = 1.995428;

const classCoeffs_lin = {
  'Clay Loam': 0.85979341018798,
  Loam: 0.699975954948688,
  'Loamy Sand': -0.276474430660692,
  Sand: -0.421286142387115,
  SandyClayLoam: 0.116275524681482,
  SiltyClayLoam: 1.12658188339897,
  'Silt Loam': 0.947400061260916,
  'Sandy Loam': -0.072395458729348,
};

const slopeCoeffs_lin = {
  flat: -0.638068163510493,
  gentle: -0.532311687092869,
  hilly: -0.469187146468346,
  moderate: -0.536219076403391,
  rolling: -0.591120692308023,
};

const coeffs_lin = {
  median_365: -0.015099291742293,
  median_385: -0.20378368894257,
  median_450: 0.120748645027738,
  median_500: 0.122843613731032,
  median_530: -0.098691245124978,
  median_587: 0.023852250852373,
  median_632: -0.103471482949587,
  median_850: -0.157737243028772,
  median_880: 0.023547986179283,
  median_940: 0.109575238734458,
};

// Coefficients for logistic regression model
const logistic_intercept = 36.81554;

const classCoeffs_log = {
  'Clay Loam': -15.76599304,
  Loam: -15.69990681,
  'Loamy Sand': -19.87584986,
  Sand: -35.35526598,
  'Sandy Clay Loam': -17.81511652,
  'Silty Clay Loam': -2.954910143,
  'Silt Loam': -2.647953235,
  'Sandy Loam': -18.51488297,
};

const slopeCoeffs_log = {
  flat: -16.61476075,
  gentle: -15.30812614,
  hilly: -32.06172828,
  moderate: -15.98251391,
  rolling: 0.065317735,
};

const coeffs_log = {
  median_365: 1.218214683,
  median_385: -2.4615156,
  median_450: 1.027460614,
  median_500: 0.840093446,
  median_530: -0.918635412,
  median_587: 0.17404612,
  median_632: -0.707424558,
  median_850: -1.030282671,
  median_880: 0.297519628,
  median_940: 0.616088074,

// export { linear_intercept, logistic_intercept, coeffs_lin, classCoeffs_lin, slopeCoeffs_lin, coeffs_log, classCoeffs_log, slopeCoeffs_log, }