/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

// import { sprintf } from 'sprintf-js';
// import _ from 'lodash';

import {
  app
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error.length > 0) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // ui.info('DEBUG', JSON.stringify(result, null, 2));

  const ranking = [];

  if (result.name_field1 && result.striga_field1 === 'No') {
    ranking.push({
      name: result.name_field1,
      value: result.prob_field1,
    });
  }

  if (result.name_field2 && result.striga_field2 === 'No') {
    ranking.push({
      name: result.name_field2,
      value: result.prob_field2,
    });
  }

  if (result.name_field3 && result.striga_field3 === 'No') {
    ranking.push({
      name: result.name_field3,
      value: result.prob_field3,
    });
  }

  ranking.sort((a, b) => b.value - a.value);
  let rankingText = '';

  if (ranking.length > 0) {
    rankingText = ranking.map((r, idx) => `${idx + 1} ${r.name}`).join('<br>');
  } else {
    rankingText = 'No ranking available.';
  }

  let box = '';
  box += `Ranking: <br>${rankingText}<br>`;
  box += `<p style="font-size: 140%">Summary: ${'The field ranked #1 will respond best to fertilizer use'}</p>`;

  ui.info('Rank by Soil Carbon', box);
  box = '';

  box = '';

  // Field 1 results
  // Results of Logistic Regression Model
  // Management recommendation based on predicted SOC and striga
  if (result.striga_field1 === 'Yes') {
    box +=
      '<p style="font-size: 140%">Recommendation: Rotate with a legume crop to reduce striga before planting maize here again</p><br>';
    app.csvExport('recommendation_field1', 'rotate');
  } else if (result.prob_field1 >= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: Add fertilizer: this field should respond well to fertilizer, for best results apply NPK fertilizer at planting and Urea 4-6 after planting</p><br>';
    app.csvExport('recommendation_field1', 'add_fertilizer');
  } else if (result.prob_field1 <= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: This field needs rehabilitation before fertilizer should be used, try planting legumes or applying compost</p><br>';
    app.csvExport('recommendation_field1', 'no_fertilizer');
  }

  box += `Striga: ${result.striga_field1}<br>`;

  // Estimated SOC levels
  if (result.prob_field1 >= 0.4) {
    box += 'Estimated Soil Carbon Value: Greater than 1%<br>';
  } else if (result.prob_field1 <= 0.4) {
    box += 'Estimated Soil Carbon Value: Less than 1%<br>';
  }

  box += `Slope: ${result.slope_field1}<br>`;
  box += `Soil texture: ${result.soil_texture_1}`;

  ui.info(result.name_field1, box);
  box = '';

  app.csvExport('name_field1', result.name_field1);
  app.csvExport('striga_field1', result.striga_field1);
  app.csvExport('slope_field1', result.slope_field1);
  app.csvExport('soil_texture_1', result.soil_texture_1);
  app.csvExport('intercept_log_field1', result.logistic_intercept);
  app.csvExport('prob_field1', result.prob_field1);

  // Results from Linear Regression Model

  // ui.info('Field 1 - Estimated C', result.estC_field1.toFixed(3));
  // ui.info('to', result.maxC_field1.toFixed(3));

  app.csvExport('slope_lin_field1', result.slope_lin_field1);
  app.csvExport('soilClass_lin_field1', result.soilClass_lin_field1);
  app.csvExport('intercept_lin_field1', result.linear_intercept);
  // app.csvExport('minC_field1', result.minC_field1);
  // app.csvExport('maxC_field1', result.maxC_field1);
  app.csvExport('estC_field1', result.estC_field1);

  // Field 2 results
  // Results of Logistic Regression Model

  box = '';

  // Field 2 results
  // Results of Logistic Regression Model
  // Management recommendation based on predicted SOC and striga
  if (result.striga_field2 === 'Yes') {
    box +=
      '<p style="font-size: 140%">Recommendation: Rotate with a legume crop to reduce striga before planting maize here again</p><br>';
    app.csvExport('recommendation_field2', 'rotate');
  } else if (result.prob_field2 >= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: Add fertilizer: this field should respond well to fertilizer, for best results apply NPK fertilizer at planting and Urea 4-6 after planting</p><br>';
    app.csvExport('recommendation_field2', 'add_fertilizer');
  } else if (result.prob_field2 <= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: This field needs rehabilitation before fertilizer should be used, try planting legumes or applying compost</p><br>';
    app.csvExport('recommendation_field2', 'no_fertilizer');
  }

  box += `Striga: ${result.striga_field2}<br>`;

  // Estimated SOC levels
  if (result.prob_field2 >= 0.4) {
    box += 'Estimated Soil Carbon Value: Greater than 1%<br>';
  } else if (result.prob_field2 <= 0.4) {
    box += 'Estimated Soil Carbon Value: Less than 1%<br>';
  }

  box += `Slope: ${result.slope_field2}<br>`;
  box += `Soil texture: ${result.soil_texture_2}`;

  ui.info(result.name_field2, box);
  box = '';

  app.csvExport('name_field2', result.name_field2);
  app.csvExport('striga_field2', result.striga_field2);
  app.csvExport('slope_field2', result.slope_field2);
  app.csvExport('soil_texture_2', result.soil_texture_2);
  app.csvExport('intercept_log_field2', result.logistic_intercept);
  app.csvExport('prob_field2', result.prob_field2);

  // Results from Linear Regression Model

  // ui.info('Field 2 - Estimated SOC', result.estC_field2.toFixed(3));
  // ui.info('to', result.maxC_field2.toFixed(3));

  app.csvExport('slope_lin_field2', result.slope_lin_field2);
  app.csvExport('soilClass_lin_field2', result.soilClass_lin_field2);
  app.csvExport('intercept_lin_field2', result.linear_intercept);
  // app.csvExport('minC_field2', result.minC_field2);
  // app.csvExport('maxC_field2', result.maxC_field2);
  app.csvExport('estC_field2', result.estC_field2);

  // Field 3 results
  box = '';

  // Results of Logistic Regression Model
  // ui.info('Name - Field 1', result.name_field1);
  // Management recommendation based on predicted SOC and striga
  if (result.striga_field3 === 'Yes') {
    box +=
      '<p style="font-size: 140%">Recommendation: Rotate with a legume crop to reduce striga before planting maize here again</p><br>';
    app.csvExport('recommendation_field3', 'rotate');
  } else if (result.prob_field3 >= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: Add fertilizer: this field should respond well to fertilizer, for best results apply NPK fertilizer at planting and Urea 4-6 after planting</p><br>';
    app.csvExport('recommendation_field3', 'add_fertilizer');
  } else if (result.prob_field3 <= 0.4) {
    box +=
      '<p style="font-size: 140%">Recommendation: This field needs rehabilitation before fertilizer should be used, try planting legumes or applying compost</p><br>';
    app.csvExport('recommendation_field3', 'no_fertilizer');
  }

  box += `Striga: ${result.striga_field3}<br>`;

  // Estimated SOC levels
  if (result.prob_field3 >= 0.4) {
    box += 'Estimated Soil Carbon Value: Greater than 1%<br>';
  } else if (result.prob_field3 <= 0.4) {
    box += 'Estimated Soil Carbon Value: Less than 1%<br>';
  }

  box += `Slope: ${result.slope_field3}<br>`;
  box += `Soil texture: ${result.soil_texture_3}`;

  ui.info(result.name_field3, box);
  box = '';

  app.csvExport('name_field3', result.name_field3);
  app.csvExport('striga_field3', result.striga_field3);
  app.csvExport('slope_field3', result.slope_field3);
  app.csvExport('soil_texture_3', result.soil_texture_3);
  app.csvExport('intercept_log_field3', result.logistic_intercept);
  app.csvExport('prob_field3', result.prob_field3);

  // Results from Linear Regression Model

  // ui.info('Field 3 - SOC ranges from', result.minC_field3.toFixed(3));
  // ui.info('to', result.maxC_field3.toFixed(3));

  app.csvExport('slope_lin_field3', result.slope_lin_field3);
  app.csvExport('soilClass_lin_field3', result.soilClass_lin_field3);
  app.csvExport('intercept_lin_field3', result.linear_intercept);
  // app.csvExport('minC_field3', result.minC_field3);
  // app.csvExport('maxC_field3', result.maxC_field3);
  app.csvExport('estC_field3', result.estC_field3);

  let adjText1 = 'wl - adj - orig<br>';
  let adjText2 = 'wl - adj - orig<br>';
  let adjText3 = 'wl - adj - orig<br>';
  const wlList = ['365', '385', '450', '500', '530', '587', '632', '850', '880', '940'];
  wlList.forEach((wl) => {
    adjText1 += `${wl} ${Math.round(result[`nm${wl}_field1`]*100, 3)/100} ${Math.round(result[`nm${wl}_orig_field1`]*100, 3)/100}<br>`;
    adjText2 += `${wl} ${Math.round(result[`nm${wl}_field2`]*100, 3)/100} ${Math.round(result[`nm${wl}_orig_field2`]*100, 3)/100}<br>`;
    adjText3 += `${wl} ${Math.round(result[`nm${wl}_field3`]*100, 3)/100} ${Math.round(result[`nm${wl}_orig_field3`]*100, 3)/100}<br>`;
  });

  ui.info('Field1', adjText1);
  ui.info('Field2', adjText2);
  ui.info('Field3', adjText3);

  if (result.error && result.error.length > 0) {
    // <------ this is new, check if length is 0
    // Display each entry from the results error array with an error card
    app.error(); // this is new, indicating that there is an error
    result.error.forEach((msg) => {
      ui.error('Error', msg);
    });
    app.save();
    return;
  }

  app.save();
})();