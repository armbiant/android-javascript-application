/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  const Organic_Matter =
    100 * ((result.loi_pre - result.loi_post) / (result.loi_pre - result.loi_crucible));
  // Traditional conversion factor for %OM to %TOC is OM = TOC x 1.724, may vary based on soil type and type of OM;
  const Total_C = Organic_Matter / 1.724;

  ui.info('Organic Matter %', MathMore.MathROUND(Organic_Matter, 2));
  app.csvExport('Organic_Matter', MathMore.MathROUND(Organic_Matter, 2));
  ui.info('Total Organic C %', MathMore.MathROUND(Total_C, 2));
  app.csvExport('Total Organic C %', MathMore.MathROUND(Total_C, 2));

  app.save();
})();
