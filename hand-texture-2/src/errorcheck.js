import { err, survey, regex } from './sensor';

function seqCheck(seq) {
  seq.forEach((s) => {
    if (survey[s] === null || survey[s] === undefined || survey[s] === '') {
      err(`Missing answer for: ${s}`);
    }
  });
}

function sel(name, eq = 'yes') {
  if (survey[name] && survey[name].toLowerCase() === eq.toLowerCase()) {
    return true;
  }
  return false;
}

function yes(input) {
  if (input && (input === 'yes' || input === 'Yes')) {
    return true;
  }
  return false;
}

export default function errorCheck() {}
