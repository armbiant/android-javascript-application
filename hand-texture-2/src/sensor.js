import { app, formparser, serial } from '@oursci/scripts';
// import * as coeffs from './coefficients';
import * as defs from '../.oursci-surveys/survey';
import { slopes, soils } from './consts';

export default serial; // expose this to android for calling onDataAvailable

// https://app.our-sci.net/#/survey/by-form-id/build_Soil-C-Prediction-and-Recommendation_1568117894
// if in dev environment, take this survey result as example
const uuidSnippet = 'uuid:d0c257a0-f13d-4a05-b883-6e101177a12c';
const form = 'soil-carbon-based-maize-recommendations';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {};
result.log = [];
result.error = [];
const survey = defs.survey();
// hello!
// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

(async () => {
  try {
    // Use `app.getAnswer('field_id')` to retreive a response from the survey by field ID
    // Call `appendResult(title, content)` to add an entry to the results array
    // Call `app.progress(progressVal)` to set the script progress in the app, where progressVal is an integer from 0 to 100
    // `app.getMeta()` can be used to reference the survey's structure (see docs for more info)

    // show spad 632 of scan 1

    // https://gitlab.com/our-sci/resources/blob/master/resources/soil_classes.csv

    // Coefficients for linear regression model

    console.log(JSON.stringify(survey, null, 2));

    const name_field2 = survey['field_2_group/nickname_2'];
    console.log(name_field2);
    result.name_field2 = survey['field_2_group/nickname_2'];

    const ball = survey['field_2_group/ball_2'];
    console.log(ball);
    result.ball = survey['field_2_group/ball_2'];
    // appendResult('Ball:', Ball);

    const ribbon = survey['field_2_group/ribbon_2'];
    console.log(ribbon);
    result.ribbon = survey['field_2_group/ribbon_2'];
    // appendResult('Ribbon:', Ribbon);

    const ribbon_length = survey['field_2_group/ribbon_length_2'];
    console.log(ribbon_length);
    result.ribbon_length = survey['field_2_group/ribbon_length_2'];
    // appendResult('Ribbon Length:', Ribbon_length);

    const excessive_wetting = survey['field_2_group/excessive_wetting_2'];
    console.log(excessive_wetting);
    result.excessive_wetting = survey['field_2_group/excessive_wetting_2'];
    // appendResult('Feel When Excessively Wet:', excessive_wetting);

    let soil_texture_2 = '';
    if (ball === 'No') {
      soil_texture_2 = 'sand';
    } else if (ball === 'Yes' && ribbon === 'No') {
      soil_texture_2 = 'loamy_sand';
    } else if (ribbon_length === 'short' && excessive_wetting === 'gritty') {
      soil_texture_2 = 'sandy_loam';
    } else if (ribbon_length === 'short' && excessive_wetting === 'smooth') {
      soil_texture_2 = 'silt_loam';
    } else if (ribbon_length === 'short' && excessive_wetting === 'neither') {
      soil_texture_2 = 'loam';
    } else if (ribbon_length === 'medium' && excessive_wetting === 'gritty') {
      soil_texture_2 = 'sandy_clay_loam';
    } else if (ribbon_length === 'medium' && excessive_wetting === 'smooth') {
      soil_texture_2 = 'silty_clay_loam';
    } else if (ribbon_length === 'medium' && excessive_wetting === 'neither') {
      soil_texture_2 = 'clay_loam';
    } else if (ribbon_length === 'long' && excessive_wetting === 'gritty') {
      soil_texture_2 = 'sandy_clay';
    } else if (ribbon_length === 'long' && excessive_wetting === 'smooth') {
      soil_texture_2 = 'silty_clay';
    } else if (ribbon_length === 'long' && excessive_wetting === 'neither') {
      soil_texture_2 = 'clay';
    } else {
      result.error = ['Input missing answer'];
      app.error();
    }
    // console.log('Soil Texture: ', soil_texture_1);
    // appendResult('Soil Texture:', soil_texture_1);

    // to pass to processor
    result.soil_texture_2 = soil_texture_2;

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.error();
    app.result(result);
  }
})();
