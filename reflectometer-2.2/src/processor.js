/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import wavelengthProcessor from './lib/process-wavelengths';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // ///////////////////////////////////////////////////////
  // Loop through all of the measurements that were taken (if there were multiple measurements)
  // and calculate the median values for each wavelength
  for (let j = 0; j < result.sample.length; j++) {
    let toDevice = '';
    const meas = result.sample[j];

    // ///////////////////////////////////////////////////////
    // create a space in result.json to enter processed data
    meas.processed = {};

    // ///////////////////////////////////////////////////////
    // Determine what the wavelengths for each pulse set are and add it to the result.
    // This is passed to the script to process the results and to generate the averages at each pulse set.
    let wavelengths = [];
    if (
      meas.calibration === 1 ||
      meas.calibration === 2 ||
      meas.calibration === 3 ||
      meas.calibration === 4 ||
      meas.calibration === 5
    ) {
      wavelengths = [
        850,
        880,
        370,
        395,
        420,
        605,
        650,
        730,
        530,
        940,
        850,
        880,
        370,
        395,
        420,
        605,
        650,
        730,
        530,
        940,
      ];
    } else {
      // includes if it's undefined
      wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
    }

    // ///////////////////////////////////////////////////////
    // Process the results - clean up outliers, straighten due to heating, and produce medians, etc. for each wavelength and see if it's a calibration script
    meas.processed = wavelengthProcessor(meas, wavelengths);
    console.log(meas.processed);

    // ///////////////////////////////////////////////////////
    // If it's a calibration script, figure out which and save data back to device accordingly
    // calibration using white card (max signal)
    // Assemble the data to save to device and save back to device
    if (meas.calibration === 1) {
      let set_ir_high = 'set_ir_high+';
      for (let i = 0; i < 10; i++) {
        set_ir_high += i + 1 + '+';
        set_ir_high += meas.processed.median[i] + '+';
      }
      set_ir_high += -1 + '+';
      let set_vis_high = 'set_vis_high+';
      for (let i = 0; i < 10; i++) {
        set_vis_high += i + 1 + '+';
        set_vis_high += meas.processed.median[i + 10] + '+';
      }
      set_vis_high += -1 + '+';
      console.log(set_ir_high);
      console.log(set_vis_high);
      // Send the data to the device in the next measurement
      toDevice =
        //      serial.write(set_ir_high);
        //      serial.write(set_vis_high);
    } else if (meas.calibration === 2) {
      // calibration using black card (min signal)
      // Assemble the data to save to device and save back to device
      let set_ir_low = 'set_ir_low+';
      for (let i = 0; i < 10; i++) {
        set_ir_low += i + 1 + '+';
        set_ir_low += meas.processed.median[i] + '+';
      }
      set_ir_low += -1 + '+';
      let set_vis_low = 'set_vis_low+';
      for (let i = 0; i < 10; i++) {
        set_vis_low += i + 1 + '+';
        set_vis_low += meas.processed.median[i + 10] + '+';
      }
      set_vis_low += -1 + '+';
      console.log(set_ir_low);
      console.log(set_vis_low);
      // Send the data to the device
      serial.write(set_ir_low);
      serial.write(set_vis_low);
      // Also identify the QR code which contains the card's master values, and save them to the device
      /*
      const qr = app.getAnswer('cardQr');
      if (qr) {
        serial.write(qr);
      }
      */
    } else if (meas.calibration === 3) {
      // blank used for cuvettes
      // Assemble the data to save to device and save back to device
      let set_ir_blank = 'set_ir_blank+';
      for (let i = 0; i < 10; i++) {
        set_ir_blank += i + 1 + '+';
        set_ir_blank += meas.processed.median[i] + '+';
      }
      set_ir_blank += -1 + '+';
      let set_vis_blank = 'set_vis_blank+';
      for (let i = 0; i < 10; i++) {
        set_vis_blank += i + 1 + '+';
        set_vis_blank += meas.processed.median[i + 10] + '+';
      }
      set_vis_blank += -1 + '+';
      console.log(set_ir_blank);
      console.log(set_vis_blank);
      // Send the data to the device
      serial.write(set_ir_blank);
      serial.write(set_vis_blank);
    } else if (meas.calibration === 4 || meas.calibration === 5) {
      // master calibrations only for creating calibration cards
      // master for white card
      if (meas.calibration === 4) {
        let set_ir_high_master = 'set_ir_high_master+';
        for (let i = 0; i < 10; i++) {
          set_ir_high_master += i + 1 + '+';
          set_ir_high_master += MathMore.MathROUND(meas.processed.median[i], 3) + '+';
        }
        set_ir_high_master += -1 + '+';
        let set_vis_high_master = 'set_vis_high_master+';
        for (let i = 0; i < 10; i++) {
          set_vis_high_master += i + 1 + '+';
          set_vis_high_master += MathMore.MathROUND(meas.processed.median[i + 10], 3) + '+';
        }
        set_vis_high_master += -1 + '+';
        console.log(set_ir_high_master);
        console.log(set_vis_high_master);
        // Save data to result.processed so is displayed an can be printed as QR codes
        meas.processed.masterCard = set_ir_high_master + set_vis_high_master;
      } else if (meas.calibration === 5) {
        // master calibration using black card (min signal)
        let set_ir_low_master = 'set_ir_low_master+';
        for (let i = 0; i < 10; i++) {
          set_ir_low_master += i + 1 + '+';
          set_ir_low_master += MathMore.MathROUND(meas.processed.median[i], 3) + '+';
        }
        set_ir_low_master += -1 + '+';
        let set_vis_low_master = 'set_vis_low_master+';
        for (let i = 0; i < 10; i++) {
          set_vis_low_master += i + 1 + '+';
          set_vis_low_master += MathMore.MathROUND(meas.processed.median[i + 10], 3) + '+';
        }
        set_vis_low_master += -1 + '+';
        console.log(set_ir_low_master);
        console.log(set_vis_low_master);
        // Save data to result.processed so is displayed an can be printed as QR codes
        meas.processed.masterCard = set_ir_low_master + set_vis_low_master;
      }
    }

    /*
    // FOR BLACK MASTER MASTER ONLY
    // manually set device to master (0 - 100 for low - high)
    // set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+
    const set_ir_high_master =
      'set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_ir_low_master = 'set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    const set_vis_high_master =
      'set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_vis_low_master = 'set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    serial.write(set_ir_high_master);
    serial.write(set_ir_low_master);
    serial.write(set_vis_high_master);
    serial.write(set_vis_low_master);
        */

    // Display memory in console to confirm saved values
    // currently causing issues with the black calibration, for now just leaving memory blank.
    result.memory = {};
  }

  console.log(result);















  for (let k = 0; k < 9; k++) {
    ui.info('sample values flat', JSON.stringify(result.sample[0].processed.sample_values_flat[k]));
    ui.info('sample values flat no string', result.sample[0].processed.sample_values_flat[k]);
    ui.info('sample values perc', JSON.stringify(result.sample[0].processed.sample_values_perc[k]));
    ui.info('sample values raw', JSON.stringify(result.sample[0].processed.sample_values_raw[k]));
    ui.info('sample values voltage', JSON.stringify(result.sample[0].processed.sample_voltage_raw[k]));
    ui.info('sample values adjustments', JSON.stringify(result.sample[0].processed.sample_values_adjustments[k]));
    ui.info('sample values slope', JSON.stringify(result.sample[0].processed.sample_values_slope[k]));
    ui.info('sample values yint', JSON.stringify(result.sample[0].processed.sample_values_yint[k]));
    ui.info('sample values center', JSON.stringify(result.sample[0].processed.sample_values_center[k]));
    ui.info('sample values time', JSON.stringify(result.sample[0].processed.sample_values_time[k]));
  }

  // ///////////////////////////////////////////////////////
  // Device info, but if there are several measurements, just print this once (not every time)
  ui.info(
    'Device Info',
    ' ID: ' +
    result.device_id +
    '<br>Name: ' +
    result.device_name +
    ', Version: ' +
    result.device_version +
    ', Firmware: ' +
    result.device_firmware,
  );
  if (typeof result.device_battery !== 'undefined') {
    app.csvExport('device_battery', result.device_battery);
  }
  app.csvExport('device_id', result.device_id);
  app.csvExport('device_version', result.device_version);
  app.csvExport('device_firmware', result.device_firmware);

  // ///////////////////////////////////////////////////////
  // Repeat through all of the measurements which were taken.  If there are multiple, make sure the are all outputted.
  for (let j = 0; j < result.sample.length; j++) {

    const numProtocols = result.sample.length; // we need this to not output extra .0/.1/.2 for every value unless we need to.

    if (numProtocols > 1) {
      // only show which measurement this is if there is more than one, otherwise it's an annoying info box!
      ui.info('Results of measurement ' + (j + 1) + ' of ' + numProtocols, '');
    }

    const meas = result.sample[j];
    //    const data = meas.data_raw;
    const data = meas.voltage_raw;
    const processed = meas.processed;
    const wavelengths = processed.wavelengths;

    // ///////////////////////////////////////////////////////
    // Error checking
    // Make sure this is the right device for this protocol.  Save errors to the warnings array to be displayed and saved later
    if (typeof result.device_name === 'undefined') {
      const warningText =
        '"device_name" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
      processed.warnings.push(warningText);
    }
    if (result.device_name.toString() !== 'Reflectometer') {
      const warningText =
        'This script was intended to be used on the reflectometer!  This device is a ' +
        result.device_name.toString();
      processed.warnings.push(warningText);
    }
    if (typeof result.device_version === 'undefined') {
      const warningText =
        '"device_version" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
      processed.warnings.push(warningText);
    }
    if (typeof result.device_id === 'undefined') {
      const warningText =
        '"device_id" is expected from the device, but I don\'t see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.  If using Our Sci firmware, use the set_device_info+<deviceID>+ command to assigned ID';
      processed.warnings.push(warningText);
    }
    if (typeof result.device_firmware === 'undefined') {
      const warningText =
        '"device_firmware" is expected from the device, but I don\'t see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.';
      processed.warnings.push(warningText);
    }

    // Print all errors and info staetments's up to this point and save them to the database
    if (typeof processed.warnings[0] !== 'undefined') {
      for (let i = 0; i < processed.warnings.length; i++) {
        ui.warning('Warning', processed.warnings[i]);
        if (numProtocols === 1) {
          app.csvExport('warnings', processed.warnings.toString());
        } else {
          app.csvExport('warnings.' + j, processed.warnings.toString());
        }
      }
    }
    if (typeof processed.info[0] !== 'undefined') {
      for (let i = 0; i < processed.info.length; i++) {
        ui.warning('Info', processed.info[i]);
        if (numProtocols === 1) {
          app.csvExport('info' + j, processed.info.toString());
        } else {
          app.csvExport('info.' + j, processed.info.toString());
        }
      }
    }
    // ///////////////////////////////////////////////////////

    // ///////////////////////////////////////////////////////
    // identify calibration type for this measurement (if any), and save to the database
    if (meas.calibration === 1) {
      ui.info('this is a white card calibration', ' ');
    } else if (meas.calibration === 2) {
      ui.info('this is a black card calibration', ' ');
    } else if (meas.calibration === 3) {
      ui.info('this is a blank calibration', ' ');
    } else if (meas.calibration === 4) {
      ui.info('this is a white MASTER card calibration', ' ');
    } else if (meas.calibration === 5) {
      ui.info('this is a black MASTER card calibration', ' ');
    }
    if (typeof meas.calibration !== 'undefined') {
      app.csvExport('calibration', meas.calibration);
    }
    // ///////////////////////////////////////////////////////

    // ///////////////////////////////////////////////////////
    // Display and save the data
    // If it's a calibration 1 or 2, then it's 0 - 65535 for the results
    // If it's a calibration 3 or 0/undefined (normal measurement), it's -20 - 120
    // If master calibration of cards, display the calibration values so they can be copied to a QR code and printed on the card.
    if (meas.calibration === 4 || meas.calibration === 5) {
      ui.info('Calibration Card Values', processed.masterCard);
      app.csvExport('calibration_card_values', processed.masterCard);
    }
    // Save the median and stdev from each wavelength
    for (let i = 0; i < processed.median.length; i++) {
      if (numProtocols === 1) {
        app.csvExport('median_' + processed.wavelengths[i], processed.median[i]);
      } else {
        app.csvExport('median_' + processed.wavelengths[i] + '.' + j, processed.median[i]);
      }
    }
    for (let i = 0; i < processed.median.length; i++) {
      if (numProtocols === 1) {
        app.csvExport('three_stdev_' + processed.wavelengths[i], processed.three_stdev[i]);
      } else {
        app.csvExport('three_stdev_' + processed.wavelengths[i] + '.' + j, processed.three_stdev[i]);
      }
    }

    // FOR NOW this is auto-scaling... but that makes readability hard for users... better to pre-scale consistently.
    // problem is, we need another identifier for the scale (we can base it on calibration, but when we request raw normal measurements it gets out of scale)
    // needs some work here.

    const minAvg = Math.min(...processed.median);
    const maxAvg = Math.max(...processed.median);

    // ///////////////////////////////////////////////////////
    // If there were questions indicating that this should have calibrations applied, then do so
    if (processed.test === "Polyphenols") {} else if (processed.test === "Protein") {} else if (processed.test === "Antioxidants") {}

    // ///////////////////////////////////////////////////////
    // If and only if the user has a questions saying that this is a chlorophyll measurement, then display the chlorophyll + related info at the top + save info to CSV
    //    /*
    if (processed.is_chlorophyll === 1) {
      const spad_names = ['chlorophyll', 'carotenoids', 'anthocyanins'];
      const spad_results = [processed.spad[6], processed.spad[3], processed.spad[4]];
      for (let i = 0; i < spad_results.length; i++) {
        if (Number.isNaN(spad_results[i])) {
          spad_results[i] = -1;
          ui.warning('Warning', spad_names + ' is out of range.  Leaf may be too thick.  Value set to -1');
        }
      }

      ui.info('Chlorophyll ' + MathMore.MathROUND(processed.spad[6]), '');
      if (numProtocols === 1) {
        app.csvExport('chlorophyll_spad', processed.spad[6]);
      } else {
        app.csvExport('chlorophyll_spad.' + j, processed.spad[6]);
      }

      ui.barchart({
          series: [
            spad_results,
          ],
        },
        'Leaf Information', {
          axisX: {
            labelInterpolationFnc(value, index) {
              return `${spad_names[index]}`;
            },
          },
        });
      ui.info('Carotenoids ' + MathMore.MathROUND(processed.spad[3]), '');
      ui.info('Anthocyanins ' + MathMore.MathROUND(processed.spad[4], 2), '');
      if (numProtocols === 1) {
        app.csvExport('carotenoid_spad', processed.spad[3]);
        app.csvExport('anthocyanin_spad', processed.spad[4]);
      } else {
        app.csvExport('carotenoid_spad.' + j, processed.spad[3]);
        app.csvExport('anthocyanin_spad.' + j, processed.spad[4]);
      }
    }


    ui.plot({
        series: [processed.median],
      },
      'Average Spectral Values', {
        min: minAvg,
        max: maxAvg,
        axisX: {
          labelInterpolationFnc(value, index) {
            return `${wavelengths[index]}`;
          },
        },
      },
    );
    ui.plot({
        series: [data],
      },
      'Raw Spectral Results', {
        //    min: 0,
        //    max: 66000,
      },
    );
    const minStd = Math.min(...processed.three_stdev);
    const maxStd = Math.max(...processed.three_stdev);
    ui.plot({
        series: [processed.three_stdev],
      },
      'Spectral Noise (3 st. dev.)', {
        min: minStd,
        max: maxStd,
        axisX: {
          labelInterpolationFnc(value, index) {
            return `${wavelengths[index]}`;
          },
        },
      },
    );
    ui.info('Three stdev', processed.three_stdev.map(e => sprintf('%.2f,', e)).join('<br>'));
    ui.info('Medians', processed.median.map(e => sprintf('%.2f,', e)).join('<br>'));
    if (typeof result.memory !== 'undefined' && Object.keys(result.memory).length > 0) {
      // only print if 'memory' exists and there's something in it...
      ui.info('Memory', JSON.stringify(result.memory, null, 2).replace(/(?:\r\n|\r|\n)/g, '<br>'));
    }

    if (typeof meas.temperature !== 'undefined') {
      ui.info(
        'Environmental conditions',
        sprintf(
          '%.2f C, %.2f %% humidity, %.2f mB pressure, %.2f IAQ VOCs',
          meas.temperature,
          meas.humidity,
          meas.pressure,
          meas.voc,
        ),
      );
      if (numProtocols === 1) {
        app.csvExport('temperature', meas.temperature);
        app.csvExport('humidity', meas.humidity);
        app.csvExport('pressure', meas.pressure);
        app.csvExport('voc', meas.voc);
      } else {
        app.csvExport('temperature.' + j, meas.temperature);
        app.csvExport('humidity.' + j, meas.humidity);
        app.csvExport('pressure.' + j, meas.pressure);
        app.csvExport('voc.' + j, meas.voc);
      }
    }
  }
  // make sure this is at the very end!
})();
app.save();