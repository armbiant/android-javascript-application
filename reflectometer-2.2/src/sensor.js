/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

import calibrations from './lib/calibrations';
import app from './lib/app';
import serial from './lib/serial';
import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  // ///////////////////////////////////////////////////////
  // Multiple measurements are saved from this single measurement script
  // Select a serial.write command below based on what you are updating to the Our Sci database.
  // Then copy paste the associated manifest (listed at the very bottom) and save it to the manifest.json file in src folder
  // Finally hit ctrl-b "compile and upload" and check the website app.our-sci.net to confirm.

  // standard measurement for user device on sample showing relative values (0 - 100)
  //  serial.write(JSON.stringify(sendDevice().standardBlank));
  //  serial.write(JSON.stringify(sendDevice().standard));
  //  serial.write(JSON.stringify(sendDevice().standardRaw));
  serial.write(JSON.stringify(sendDevice().standardRawHeatCal));
  //  serial.write(JSON.stringify(sendDevice().standardApplyCalibration));
  console.log(JSON.stringify(sendDevice().standardRawHeatCal));

  // white and black calibrations
  // the lights are in numerical order, rather than wavelength order...
  //  serial.write(JSON.stringify(sendDevice().calibrateAllWhite));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlack)); // FOR MASTER MASTER, UNCOMMENT SECTION FORCING 0 / 100 VALUES BELOW
  //  console.log(JSON.stringify(sendDevice().calibrateAllBlack));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlank));
  //  ser ial.write(JSON.stringify(sendDevice().calibrateAllWhiteMaster));
  //  serial.write(JSON.stringify(sendDevice().calibrateAllBlackMaster));

  //  other legacy protocols
  //  serial.write(JSON.stringify(sendDevice().oldReflectometer2_flat));

  // ///////////////////////////////////////////////////////
  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + count / 617.0 * 100.0 * 0.9);
    });
    app.progress(100);
    // if this is just a single measurement, then define meas as the measurement.  If there are multiple, note that they also need to be pulled out here.

    for (let j = 0; j < result.sample.length; j++) {
      const meas = result.sample[j];
      // ///////////////////////////////////////////////////////
      // now pass the information about questions to the processor script via results.json
      if (typeof app.getAnswer('is_chlorophyll') !== 'undefined') {
        meas.processed.is_chlorophyll = 1;
        console.log('has chlorophyll' + meas.processed.is_chlorophyll);
      }
      // check to see if we should be outputting calibrated values (polyphenols, proteins, and anti-oxidents)
      else if (typeof app.getAnswer('test') !== 'undefined') {
        meas.processed.test = app.getAnswer('test');
        console.log('is this lab test ' + meas.processed.is_chlorophyll);
      } else {
        console.log('not a special measurement');
      }
    }

    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();

/*
"id": "standard-2-2",
"name": "Reflectometer standard 2-2",
"description": "Standard reflectometer measurement for field and lab use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardBlank-2-2",
"name": "Reflectometer blank 2-2",
"description": "Standard reflectomter measurement with blank offset, used on samples in cuvettes or similar situations in which blanks are useful.  Use the calibrateAllBlank measurement on the blank itself to save the blank values to the device",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRaw-2-2",
"name": "Reflectometer raw 2-2",
"description": "outputs raw data only, use standard reflectometer measurement for normal use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standardRawNoHeat-2-2",
"name": "Reflectometer raw no heat calibration 2-2",
"description": "outputs raw data only, use standard reflectometer measurement for normal use.  Heat calibration not applied.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhite-2-2",
"name": "Reflectometer calibration white 2-2",
"description": "Calibrate a device to user calibration card, using the white square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlack-2-2",
"name": "Reflectometer calibration black 2-2",
"description": "Calibrate a device to user calibration card, using the black square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlank-2-2",
"name": "Reflectometer calibration blank 2-2",
"description": "Create and save a blank value, to be applied as an offset when using the standard_blank measurement on samples.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllWhiteMaster-2-2",
"name": "Reflectometer calibration white master 2-2",
"description": "Calibrate the master device to master calibration card, using the white square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMaster-2-2",
"name": "Reflectometer calibration black master 2-2",
"description": "Calibrate the master device to master calibration card, using the black square.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "calibrateAllBlackMasterMaster-2-2",
"name": "Reflectometer Master device - Maste card calibration 2-2",
"description": "ONLY used to calibrate the master card.  Force saves 0 / 100 values to the device.",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "standard-firmware-1-18",
"name": "Reflectometer measurement firmware 1.18",
"description": "only for beta devices on old firmware 1.18",
"version": "2.0",
"action": "Run Measurement",
"requireDevice": "true"
*/

/*
"id": "heat-cal",
"name": "Heat Calibration",
"description": "Calibrate devices for different temperatures (3 hour test)",
"version": "1.0",
"action": "Run Calibration",
"requireDevice": "true"
*/

/*
"id": "standard-chlorophyll-2-2",
"name": "Chlorophyll standard 2-2",
"description": "Chlorophyll content using reflectometer for field and lab use",
"version": "2.1",
"action": "Run Measurement",
"requireDevice": "true"
  */