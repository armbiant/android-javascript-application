import farmos from '@oursci/farmos';
import { app } from '@oursci/scripts';

export default async (serverId, fieldId, meta) => {
  const {
    url, username, password, farmosCookie, farmosToken,
  } = app.getCredentials(serverId);

  console.log('Field ID', `using field ID: ${fieldId}`);

  const farm = await farmos(url, username, password, farmosToken, {
    configCrop: 'none',
    configFieldId: fieldId,
    configInstanceId: meta.instanceId,
    configClearEntrieswithInstanceId: true,
  });

  return {
    url,
    async planting(crop, date) {
      await farm.configureFieldAndCrop(fieldId, crop, meta.instanceId, false);
      return farm.submitPlanting(date);
    },
    async seeding(name, assetId, date, source, quantities = []) {
      return farm.submitSeeding(date.unix(), assetId, true, source, quantities);
    },
    async input(name, assetId, date, cat, method, source, purpose, materials, quantities) {
      return farm.submitInput(
        name,
        assetId,
        fieldId,
        date,
        cat,
        method,
        source,
        purpose,
        materials,
        quantities,
      );
    },
    async observation(name, date, cat, quantities, data) {
      return farm.submitObservation(name, null, fieldId, date, cat, quantities, data);
    },
    async activity(name, date, cat, quantities = [], data, assetId = null, harvest) {
      return farm.submitActivity(name, assetId, fieldId, date, cat, quantities, data, harvest);
    },
  };
};
