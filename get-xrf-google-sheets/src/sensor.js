/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This example demonstrates how to fetch data in the sensor script
 * from google sheets.
 * Using the sheet from
 * https://docs.google.com/spreadsheets/d/1clTpjjA7Bb-lSKMlOcub219zdlQEYBnWOyVIOHg_RZ0/edit#gid=236866563
 *
 * Which contains multiple pages (makes processing a bit more challenging)
 */

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

let data1 = [];
let data2 = [];
const result = {};
result.error = {};
const sheetId = '1clTpjjA7Bb-lSKMlOcub219zdlQEYBnWOyVIOHg_RZ0'; // stays the same unless we change google doc location
let sheet1 = '';
let sheet2 = '';

const requiredAnswers = ['sample_id', 'sample_type', 'Depth'];

// get info from the sheet with the lower set of elements (Na, P, S...)
(async () => {
  requiredAnswers.forEach((a) => {
    let v;
    //    console.log(typeof app.getAnswer('sample_id'));
    if (typeof app.getAnswer(a) !== 'undefined') {
      // does it exist?
      v = app.getAnswer(a);
    }
    console.log(v);
    result[a] = v;
  });

  // go see what type of sample it is (if sample_type is carrot or spinach then we pull data from one sheet, if depth is 3 (0 - 6'') or 9 (6 - 12'') we pull data from a different sheet
  // if there is no sample_type, then it check to see if it's soil.  If there is neither, then let the user know
  if (result.sample_type) {
    result.sample_type = 'food';
    sheet1 = 'food, helium';
    sheet2 = 'food, no helium heavy metals';
    console.log(result.sample_type);
  } else if (result.Depth) {
    result.sample_type = 'soil';
    sheet1 = 'soil, helium';
    sheet2 = 'soil, no helium heavy metals';
    if (result.Depth === '3') {
      result.sample_id = `${result.sample_id}a`;
    } else if (result.Depth === '9') {
      result.sample_id = `${result.sample_id}b`;
    }
  } else {
    console.log('Neither sample_type nor Depth found, check survey type');
    result.error['missing data'] = 'Sample ID, Depth, or Sample Type not found.  Check those are answered and try again';
  }

  // get the sample ID, we'll need to filter our data not related to this sample.
  const sample_id = result.sample_id;
  console.log(`type ${result.sample_type}, id ${result.sample_id}, depth ${result.Depth}`);

  try {
    //    const gsheet = await sheets(sheetId);
    const gsheet = await sheets(sheetId);
    console.log('success');
    //    console.log(gsheet.ta.sheets(sheet1).all()); // debug
    // console.log(gsheet.ta.sheets(sheet1).toArray()); // debug
    data1 = gsheet.ta.sheets(sheet1).toArray();
    data2 = gsheet.ta.sheets(sheet2).toArray();
  } catch (e) {
    result.error = e;
    console.log(e);
  }

  // filter results to only those with current survey id, and combine both normal and heavy metals data (data1 + data2)
  // Get rid of the extra info the XRF adds to the element names so that it's easier to sort and combine metals
  for (let i = 0; i < data1.length; i++) {
    data1[i][0] = data1[i][0].slice(0, 2);
  }
  for (let i = 0; i < data2.length; i++) {
    data2[i][0] = data2[i][0].slice(0, 2);
  }
  data1 = data1.filter(a => a[3] === sample_id);
  data2 = data2.filter(a => a[3] === sample_id);
  console.log(data1);
  console.log(data2);
  if (result.sample_type === 'food') {
    // remove overlapping metals... these are measured in both XRF runs, but results are more accurate in the non-heavy metals measurement
    data1 = data1.filter(
      a =>
      a[0] === 'Na' ||
      a[0] === 'Mg' ||
      a[0] === 'Al' ||
      a[0] === 'Si' ||
      a[0] === 'P ' ||
      a[0] === 'S ' ||
      a[0] === 'Cl' ||
      a[0] === 'Rh' ||
      a[0] === 'K ' ||
      a[0] === 'Ca' ||
      a[0] === 'Mn' ||
      a[0] === 'Fe' ||
      a[0] === 'Ni' ||
      a[0] === 'Cu' ||
      a[0] === 'Zn' ||
      a[0] === 'Fe',
    );
    data2 = data2.filter(
      a =>
      a[0] === 'As' ||
      a[0] === 'Pb' ||
      a[0] === 'Se' ||
      a[0] === 'Mo',
    );
  } else if (result.sample_type === 'soil') {
    // remove overlapping metals... these are measured in both XRF runs, but results are more accurate in the non-heavy metals measurement
    // also mark the sample ID with a or b depending on the depth (that's how files are saved on XRF so needs to happen here to find them on google sheets)
    //    data1 = data1.filter(a => a[3] === sample_id);
    //    data2 = data2.filter(a => a[3] === sample_id);
    //    console.log('data1');
    //    console.log(data1);
    //    console.log('data2');
    //    console.log(data2);
    data1 = data1.filter(
      a =>
      a[0] === 'Na' ||
      a[0] === 'Mg' ||
      a[0] === 'Al' ||
      a[0] === 'Si' ||
      a[0] === 'P ' ||
      a[0] === 'S ' ||
      a[0] === 'Rh' ||
      a[0] === 'K ' ||
      a[0] === 'Ca' ||
      a[0] === 'Ba' ||
      a[0] === 'Ti' ||
      a[0] === 'V' ||
      a[0] === 'Cr' ||
      a[0] === 'Mn' ||
      a[0] === 'Fe' ||
      a[0] === 'Ni' ||
      a[0] === 'Cu' ||
      a[0] === 'Zn',
    );
    data2 = data2.filter(
      a =>
      a[0] === 'As' ||
      a[0] === 'Pb' ||
      a[0] === 'Se' ||
      a[0] === 'Rb' ||
      a[0] === 'Sr' ||
      a[0] === 'Mo',
    );
    // the data coming from the XRF calibration for soil only was in % for the following elements.
    const inPercent = ['Mg', 'Al', 'P ', 'S ', 'K ', 'Ca', 'Fe'];
    // for consistency, we're going to print everything in parts per million, so let's convert them back...
    for (let i = 0; i < data1.length; i++) { // go through all values in i
      for (let j = 0; j < inPercent.length; j++) { // cross check with values in inPercent
        if (data1[i][0] === inPercent[j]) { // if you find a match, then convert to ppm (multiple by 10,000)
          data1[i][2] = `${data1[i][2] * 10000}`;
          console.log(`converted ${data1[i][0]}`);
        }
      }
    }
    if (!result.Depth) {
      console.log('error - depth value nost filled out');
      result.error.error = 'depth value not filled out';
    }
  }
  const data = data1.concat(data2);
  result.data = data;
  console.log('filtered + combined');
  console.log(result.data);
  app.result(result);
})();


/*
{
  "sample_id": "921.4",
  "sample_type": "carrot"
}
*/