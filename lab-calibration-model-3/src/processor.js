/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/*
import {
  sprintf
} from 'sprintf-js';
import _ from 'lodash';
*/

import {
  app,
  math
} from "@oursci/scripts";
import * as ui from "@oursci/measurements-ui";
import "@oursci/measurements-ui/dist/styles.min.css";

const Chartist = require("chartist");

(() => {
  const result = (() => {
    if (typeof processor === "undefined") {
      return require("../data/result.json");
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error("Error", JSON.stringify(result.error));
    app.save();
    return;
  }

  let standardSolutions = [];

  // NOTE for standard solutions, location 0 on the array is intentionally left undefined so that i=1 i<standard.length works for everything in the loop.
  if (result.test_poly_anti_prot === "antioxidants") {
    standardSolutions = [0, 12.5, 25, 50, 100, 200]; // uM iron equivalents
  } else if (result.test_poly_anti_prot === "polyphenols") {
    standardSolutions = [0, 12.5, 25, 50, 75, 125, 150, 200]; // ug gallic acid per ml
  } else if (result.test_poly_anti_prot === "proteins") {
    standardSolutions = [0, 250, 125, 62.5, 31.25, 15.63]; // uL protein per ml
  } else if (result.test_poly_anti_prot === "lowry_proteins") {
    standardSolutions = [0, 50, 150, 250, 400, 500]; // ug protein per mL
  } else {
    standardSolutions = [];
    ui.error(
      "Standard not identified",
      "The model was unable to identify measurements to use within this survey.  Make sure you have run all measurements in the survey.  If you are not using a standard survey, you need to include some measurements to run the model on!"
    );
  }

  // ui.warning(`${result.test} standard`, '');
  // ui.info('standardSolutions', standardSolutions);
  const numberStandards = standardSolutions.length;

  // STANDARDS CALIBRATION
  // ///////////////////////////////////////////////////////
  // Using a linear model
  // create fit data for the temperature and median response, display the results, save them to CSV, and prepare them to be saved back to the device

  //  delete result.standard3; // test if one is missing.
  //  result.standard3 = 0; // test if one is zero.
  //  result.standard3 = ''; // test if one is empty.
  const regData = [];
  const regFit = [];
  const xData = [];
  //  const xAbsData = [];
  const yData = [];

  let wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
  let ledNumber = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]; // location of the wavelength in the array
  if (
    result.test_poly_anti_prot === "antioxidants" ||
    result.test_poly_anti_prot === "proteins"
  ) {
    wavelengths = [587];
    ledNumber = [5]; // location of the wavelength in the array
    ui.info(result.test_poly_anti_prot, "");
  } else if (result.test_poly_anti_prot === "lowry_proteins") {
    wavelengths = [632];
    ledNumber = [6]; // location of the wavelength in the array
    ui.info(result.test_poly_anti_prot, "");
  } else if (result.test_poly_anti_prot === "polyphenols") {
    wavelengths = [850];
    ledNumber = [7]; // location of the wavelength in the array
    ui.info(result.test_poly_anti_prot, "");
  }
  //  const wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
  //  const ledNumber = [3, 4, 5, 9, 6, 7, 8, 1, 2, 10];
  let toDevice_cal1 = "set_user_defined+";
  let toDevice_cal2 = "set_user_defined+";
  let toDevice_cal3 = "set_user_defined+";
  const missingFlag = [];
  // loop through all of the wavelengths to run regressions, display, and save to csv
  for (let z = 0; z < wavelengths.length; z++) {
    //    regData.push([]);
    regData.push([]);
    xData.push([]);
    //    xAbsData.push([]);
    yData.push([]);
    // loop through the number of measurements collected to put into the heat correction model
    for (let i = 0; i < standardSolutions.length; i++) {
      // if that data point is not there, remove it from the model and display... allows users to remove bad data points.  Notify them that this happened, however.
      // we already quality checked the measurement in sensor.js, so now we only need to know if it's 'undefined'
      if (result[`standard${i}`] === "undefined") {
        // mark bad points with -999999, remove later after loop
        //        regData[z].push(-999999, -999999);
        xData[z].push(-999999);
        //        xAbsData[z].push(-999999);
        yData[z].push(-999999);
        regData[z].push(-999999);
        // notify just once (when first identified).
        if (missingFlag.includes(i) !== true) {
          // ui.warning(
          //   `Standard ${standardSolutions[i]} not found`,
          //   'This data point in the standard set was not collected or removed.  If this was done in error, re-measure that standard.  If intentionally removed, you may continue.',
          // );
        }
        // mark flag to keep track of missing points
        missingFlag.push(i);
      } else {
        const meas = result[`standard${i}`].data;
        const median = meas[`median_${wavelengths[z]}`];
        //        const abs = meas[`abs_${wavelengths[z]}`];
        regData[z].push([Number(median), standardSolutions[i]]);
        xData[z].push(Number(median));
        //        xAbsData[z].push(Number(abs));
        yData[z].push(standardSolutions[i]);
      }
    }

    //    ui.info('xdata',xData[z]);
    //    ui.info('ydata',yData[z]);
    //    ui.info('bothdata', regData[z]);
    // remove missing points from all arrays
    //    regData[z] = regData[z].filter(val => val !== -999999);
    xData[z] = xData[z].filter((val) => val !== -999999);
    //    xAbsData[z] = xData[z].filter(val => val !== -999999);
    yData[z] = yData[z].filter((val) => val !== -999999);
    regData[z] = regData[z].filter((val) => val !== -999999);
    //    ui.info('bothdata', regData[z]);

    // run the model (linear regression)
    ui.info("Data", `standard: ${yData[z]}<br>reflectance: ${xData[z]}`);
    //    const regResults = MathMore.MathLINREG(xData[z], yData[z]);
    const regResults = math.MathPOLYREG(regData[z], 2);
    const slopesNormalized = [];
    slopesNormalized[0] = regResults.slopes[0]; // / average_value;
    slopesNormalized[1] = regResults.slopes[1]; // / average_value;
    slopesNormalized[2] = regResults.slopes[2]; // / average_value;
    // save results to csv and display
    regFit.push(regResults.rsquared);
    app.csvExport(
      `rsquared_${wavelengths[z]}`,
      math.MathROUND(regResults.rsquared, 4)
    );
    app.csvExport(`yint_${wavelengths[z]}`, slopesNormalized[0]);
    app.csvExport(`slope1_${wavelengths[z]}`, slopesNormalized[1]);
    app.csvExport(`slope2_${wavelengths[z]}`, slopesNormalized[2]);
    // Error check if r2 is too low
    let poorFit = 0;
    let color = "inherit";
    let note = "OK";
    if (regResults.rsquared < 0.95) {
      color = "red";
      note = "<-- bad fit, re-run!";
      ui.error(
        `Poor fit! r2 = ${math.MathROUND(regFit[z], 3)} at ${wavelengths[z]}nm`,
        "Evaluate data and re-run the calibration standards"
      );
      poorFit += 1;
    } else if (regResults.rsquared >= 0.95 && regResults.rsquared < 0.985) {
      color = "orange";
      note = "<-- not great fit, consider re-running";
      ui.warning(
        `Not great fit.  r2 = ${math.MathROUND(regFit[z], 3)} at ${
          wavelengths[z]
        }nm`,
        "Consider re-running the calibration standards"
      );
    } else if (regResults.rsquared >= 0.985) {
      color = "green";
    }
    const text = `<span style="background-color:${color}">`;
    ui.info(
      `Calibration at ${wavelengths[z]}`,
      `Test: ${result.test_poly_anti_prot}<br>
      r-squared: ${text}${math.MathROUND(
        regResults.rsquared,
        3
      )}</span> ${note}<br>
      y int: ${slopesNormalized[0]}<br>
      slope1: ${slopesNormalized[1]}<br>
      slope2: ${slopesNormalized[2]}<br>
      y int raw: ${regResults.slopes[0]}<br>
      slope1 raw: ${regResults.slopes[1]}<br>
      slope2 raw: ${regResults.slopes[2]}`
    );
    // Note the number of poor fits.
    app.csvExport("poorFit", poorFit);
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_cal1 += `${ledNumber[z] + 1}+${slopesNormalized[0]}+`;
    toDevice_cal2 += `${ledNumber[z] + 11}+${slopesNormalized[1]}+`;
    toDevice_cal3 += `${ledNumber[z] + 21}+${slopesNormalized[2] * 1000}+`;
    //  toDevice += z + 21 + '+' + slopesNormalized[2] * 1000 + '+';
    //    toDevice += z + 31 + '+' + slopesNormalized[3] * 1000000 + '+';
    //    toDevice += z + 41 + '+' + slopesNormalized[4] * 1000000000 + '+';
    // output individual plots for each wavelength
    ui.plotxy(xData[z], yData[z], `Fit for ${wavelengths[z]}`);
    //    ui.plotxy(xAbsData[z], yData[z], 'Fit using Absorbance ' + wavelengths[z]);
  }

  // assemble the plots so they show all in one graph
  //  ui.multixy(xData, yData, 'Fits for Wavelengths', wavelengths.map(v => 'Fit for ' + v));
  // close out the text to be sent to device, export to CSV (important, also gets picked up by next measurement script), and display for santify check
  toDevice_cal1 += "-1+";
  toDevice_cal2 += "-1+";
  toDevice_cal3 += "-1+";
  app.csvExport("toDevice_cal1", toDevice_cal1); // toDevice saved to csv to check if needed
  app.csvExport("toDevice_cal2", toDevice_cal2); // toDevice saved to csv to check if needed
  app.csvExport("toDevice_cal3", toDevice_cal3); // toDevice saved to csv to check if needed
  app.setEnv(
    "toDevice",
    toDevice_cal1 + toDevice_cal2 + toDevice_cal3,
    "data to send to device"
  ); // toDevice saved to environment variable, which is then saved to device by 'toDevice' measurement
  ui.warning(
    "Notes on saving to device",
    "Y Intercept values are saved in userdef[1] - userdef[10], and slope values are saved to userdef[11] - userdef[20].  They can be accessed by using the 'recall' function."
  );
  ui.info("Save yint to Device", toDevice_cal1);
  ui.info("Save slopes to Device", toDevice_cal2);
  ui.info("Save slopes to Device", toDevice_cal3);
  //  ui.info('Save to Device coefficient 3', toDevice);
  //  ui.info('Save to Device coefficient 4', toDevice);
  //  ui.info('Save to Device coefficient 5', toDevice);
})();
// make sure this is at the very end!
app.save();