/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * !!!IMPORTANT!!! for testing
 * Add crentials to mock/credentials.json (don't commit to repo)
 * format in mock/credentials.demo.json
 */
// https://test.farmos.net/admin/structure/taxonomy/farm_log_categories

import moment from 'moment';

import { app, formparser, serial } from '@oursci/scripts';
import { survey as surveyInit } from './survey';
import { regex } from './surveyUtils';
import farmos from './farmoshelper';
import errorCheck from './errorcheck';

export default serial; // expose this to android for calling onDataAvailable

// if in dev environment, take this survey result as example
const uuidSnippet = 'uuid:9cc9f9d0';
const form = 'build_Catch-up-Management-Survey_1569352697';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

export const survey = surveyInit();

const result = {
  log: [],
  error: [],
};

function errorExit(e) {
  app.error();
  result.error.push(e.message);
  app.result(result);
}

export function err(msg) {
  result.error.push(msg);
}

const config = {
  server: 0,
  fieldId: 0,
};

const meta = app.getMeta();
const field = survey.field_id;
const category = {
  planting: 'Plantings',
  tillage: 'Tillage',
  animals: 'Animals',
};
console.log(`field: ${field}`);

const [, serverId, fieldId] = regex.field_id.exec(field);

/**
 * Date Survey started
 */
const surveyStarted = moment.unix(Number.parseFloat(meta.started) / 1000);

function quantity(label, value, unit, measure = 'value', tillageType) {
  return {
    label,
    value: Number.parseFloat(value).toFixed(2),
    unit,
    measure,
    tillageType,
  };
}

function log(title, content) {
  console.log();
  console.log(`=== ${title} ===`);
  console.log(content);
  result.log.push({
    title,
    content,
  });
}

function error(msg) {
  throw new Error(msg);
}

async function main() {
  app.progress(10);

  console.log(`server: ${serverId}, fieldId: ${fieldId}`);
  const farm = await farmos(serverId, fieldId, meta);

  app.progress(20);
  result.url = farm.url;

  const seedings = [];
  const inputs = [];
  const observations = [];
  const activities = [];

  const managementPractices = (survey.management_practices || '').split(' ');

  const assets = {};
  let firstPlantingDate = null;
  for (let i = 1; i < 4; i++) {
    if (i === 1) {
      firstPlantingDate = moment(survey['cash_planting_group/planting_date1']);
    }

    const crop = survey[`cash_planting_group/cash_crop${i}`];
    const cultivar = survey[`cash_planting_group/cultivar${i}`];
    const name = cultivar ? `${crop} / ${cultivar}` : crop;
    const plantingDate = moment(survey['cash_planting_group/planting_date1']);
    if (!crop) {
      break;
    }
    const assetId = await farm.planting(name, plantingDate);
    assets[i] = assetId;
    log('Planting', `planting crop ${name} assetId: ${assetId}`);
    const gmoAnswer = survey[`cash_planting_group/seed_gmo${i}`];
    const gmo = gmoAnswer ? gmoAnswer.toLowerCase() === 'yes' : false;

    const seeding = await farm.seeding(name, assetId, plantingDate, gmo ? 'GMO' : 'Non-GMO');
    log('Seeding', `${name}: farmOS ID: ${seeding.id}`);
    seedings.push(seeding);

    const treatment = {
      materials: formparser.getItemLabels(`cash_planting_group/seed_treatment${i}`),
      treatmentOther: survey[`cash_planting_group/seed_treatment_other${i}`],
      treatmentName: survey[`cash_planting_group/seed_treatment_name${i}`],
    };

    const treatmentName = `Seedling Treatment: ${plantingDate.format('YYYY-MM-DD')} ${name}`;

    let materials;
    if (treatment.materials) {
      materials = treatment.materials.filter(t => t.toLowerCase() !== 'other');
      if (treatment.treatmentOther) {
        materials.push(treatment.treatmentOther);
      }
    }

    // name, assetId, date, cat, method, source, purpose, materials, quantities
    const input = await farm.input(
      treatmentName,
      assetId,
      plantingDate,
      null,
      null,
      null,
      'Seedling Treatment',
      materials,
    );
    inputs.push(input);

    log('Input: Seeding Treatment', treatmentName);
  }

  app.progress(30);

  {
    const seedingCost = survey['cash_planting_group/cashcrop_seed_cost'];
    const plantingCost = survey['cash_planting_group/cashcrop_planting_cost'];

    if (seedingCost || plantingCost) {
      const quantities = [];
      const name = 'Cost for Plantings';
      if (seedingCost) {
        quantities.push(quantity('Total cost for Seeding', seedingCost, 'USD'));
      }

      if (plantingCost) {
        quantities.push(quantity('Application cost for Seeding', plantingCost, 'USD'));
      }

      const observation = await farm.observation(
        name,
        firstPlantingDate,
        category.planting,
        quantities,
      );
      observations.push(observation);
    }
  }

  app.progress(40);

  if (managementPractices.includes('weed_control')) {
    formparser
      .getItemLabels(
        'weed_control/weed_control_type',
        item => item !== 'herbicide' && item !== 'cultivation',
      )
      .forEach(async (a) => {
        activities.push(await farm.activity(`${a}`, firstPlantingDate, category.planting));
        log('Weed Control', `${a}`);
      });

    const weedControlTypes = (survey['weed_control/weed_control_type'] || '').split(' ');

    if (weedControlTypes.includes('herbicide')) {
      const cost = survey['weed_control/herbicide_cost'];
      const applicationCost = survey['weed_control/herb_application_cost'];

      const quantities = [];
      if (cost) {
        quantities.push(quantity('herbicide cost', cost, 'USD'));
      }

      if (applicationCost) {
        quantities.push(quantity('application cost', applicationCost, 'USD'));
      }

      if (quantities.length > 0) {
        inputs.push(
          await farm.input(
            'Application Cost for Weed Control',
            null,
            firstPlantingDate,
            category.planting,
            null,
            null,
            'Weed Control',
            [],
            quantities,
          ),
        );
      }

      for (let i = 1; i < 5; i++) {
        const date = moment(survey[`weed_control/herb_date${i}`]);
        const l = survey[`weed_control/herbicide_name${i}`];
        if (!l) {
          break;
        }
        const herbicide = {
          label: l,
          unit: formparser.getItemLabels(`weed_control/herbicide_units${i}`)[0],
          value: survey[`weed_control/herbicide_rate${i}`],
        };
        const quantitiy = quantity(herbicide.label, herbicide.value, herbicide.unit, 'rate');

        inputs.push(
          await farm.input(
            'Herbicide Application',
            null,
            date,
            category.planting,
            null,
            null,
            'Weed Control',
            [],
            [quantity],
          ),
        );
      }

      log('Weed Control', 'Herbicide');
    }

    if (weedControlTypes.includes('cultivation')) {
      for (let i = 1; i < 5; i++) {
        const tillageType = survey[`weed_control/type_${i}`];
        const tillageDepth = survey[`weed_control/depth_${i}`];
        const date = moment(survey[`weed_control/tillage_date${i}`]);

        if (!tillageType) {
          break;
        }
        activities.push(
          await farm.activity('Tillage', date, category.tillage, [
            quantity(tillageType, tillageDepth, 'inches', 'Depth', tillageType),
          ]),
        );

        log('Tillage', `Pass ${i} - ${tillageType}`);
      }
    }

    if (managementPractices.includes('amendment')) {
      for (let i = 1; i < 5; i++) {
        const name = survey[`amendments/name_${i}`];
        if (!name) {
          break;
        }

        const date = moment(survey[`amendments/amendment_date${i}`]);

        const materials = formparser.getItemLabels(`amendments/amendments_type${i}`);
        const unit = formparser.getItemLabels(`amendments/units_${i}`)[0];
        const rate = survey[`amendments/rate_${i}`];
        const method = formparser.getItemLabels(`amendments/application-method-${i}`);
        const traces = {
          n: survey[`amendments/n_${i}`],
          p: survey[`amendments/p_${i}`],
          k: survey[`amendments/k_${i}`],
          ca: survey[`amendments/ca_${i}`],
          mg: survey[`amendments/mg_${i}`],
          cu: survey[`amendments/cu_${i}`],
          s: survey[`amendments/s_${i}`],
          zn: survey[`amendments/zn_${i}`],
          mn: survey[`amendments/mn_${i}`],
        };

        const quantities = [];
        Object.keys(traces).forEach((t) => {
          console.log(t);
          const v = traces[t];
          if (v) {
            quantities.push(quantity(t, v, 'lbs per acre', 'rate'));
          }
        });

        quantities.push(quantity('Application Rate', rate, unit, 'rate'));
        const cost = survey[`amendments/amendment_${i}_cost`];
        if (cost) {
          quantities.push(quantity('Product cost per acre', cost, 'USD', 'rate'));
        }

        const input = await farm.input(
          `Amendment: ${name}`,
          null,
          date,
          category.planting,
          method.join(', '),
          null,
          null,
          materials,
          quantities,
        );
        inputs.push(input);

        log('Amendment', name);
      }
      const cost = survey['amendments/amendment_application_cost'];
      if (cost) {
        const observation = farm.observation(
          'Total application Cost for Amendments',
          firstPlantingDate,
          category.planting,
          [quantity('Total application cost', cost, 'USD')],
        );
        observations.push(observation);
      }
    }

    if (managementPractices.includes('pest_disease_control')) {
      const reason = formparser.getItemLabels('pest_disease_control_group/pest_disease_reason')[0];
      const cost = survey['pest_disease_control_group/pest_disease_cost'];
      const applicationCost = survey['pest_disease_control_group/pest_disease_application_cost'];

      const quantities = [];

      if (cost) {
        quantities.push(quantity('Cost', cost, 'USD'));
      }

      if (applicationCost) {
        quantities.push(quantity('Application Cost', applicationCost, 'USD'));
      }

      for (let i = 1; i < 4; i++) {
        const date = moment(survey[`pest_disease_control_group/pest_disease_date${i}`]);

        const controlled = survey[`pest_disease_control_group/pest_disease_controlled_${i}`];
        if (!controlled) {
          break;
        }
        const materials = formparser.getItemLabels(
          `pest_disease_control_group/pest_disease_type${i}`,
        );
        const product = survey[`pest_disease_control_group/pest_disease_product${i}`];
        const units = formparser.getItemLabels(
          `pest_disease_control_group/pest_disease_units${i}`,
        )[0];
        const rate = survey[`pest_disease_control_group/pest_disease_rate${i}`];
        const q = quantity(product, rate, units, 'rate');
        await farm.input(
          `Pest/Disease Control ${date.format('YYYY-MM-DD')} / ${controlled}`,
          null,
          date,
          category.planting,
          null,
          product,
          reason,
          materials,
          [q],
        );

        log('Pest/Disease Control', `${date.format('YYYY-MM-DD')} / ${controlled}`);
      }
      await farm.observation(
        `Cost of Pest/Disease Control ${firstPlantingDate.format('YYYY-MM-DD')}`,
        firstPlantingDate,
        category.planting,
        quantities,
      );

      log(
        'Cost Pest/Disease Control',
        `Cost: USD ${cost} / Application Cost: USD ${applicationCost}`,
      );
    }

    if (managementPractices.includes('cover_crop_planting')) {
      const seedingCost = survey['cover_crop_group/covercrop_seed_cost'];
      const plantingCost = survey['cover_crop_group/covercrop_planting_cost'];

      for (let i = 1; i < 3; i++) {
        const diversity = survey[`cover_crop_group/covercrop_diversity${i}`];
        if (!diversity) {
          continue;
        }

        const date = moment(survey[`cover_crop_group/covercrop_date${i}`]);
        const species = survey[`cover_crop_group/covercrop_species${i}`];
        const seedingMethod = formparser.getItemLabels(
          `cover_crop_group/covercrop_seeding_method${i}`,
        );
        const termination = formparser.getItemLabels(`cover_crop_group/covercrop_termination${i}`);
        const terminationDate = survey[`cover_crop_group/covercrop_termination_date${i}`];

        const quantities = [];
        if (seedingCost) {
          quantities.push(quantity('Seeding Cost', seedingCost, 'USD'));
        }

        if (plantingCost) {
          quantities.push(quantity('Planting Cost', plantingCost, 'USD'));
        }

        const covercrop = await farm.planting(species, date);
        log('Cover Crop Planting', species);
        await farm.seeding(
          `Covercrop seeding: ${species}, ${seedingMethod.join(', ')}`,
          covercrop,
          date,
          null,
          quantities,
        );
        log('Cover Crop Seeding', `${species}, ${seedingMethod.join(', ')}`);
        if (termination) {
          await farm.activity(
            `Planned termination: ${termination}`,
            moment(terminationDate),
            category.planting,
            [],
            covercrop,
          );
          log('Planned: Cover Crop Termination', moment(terminationDate));
        }
      }
    }
  }

  app.progress(60);

  if (managementPractices.includes('livestock_mgmt')) {
    const livestock = [];

    await farm.observation('Livestock Management', firstPlantingDate, category.animals, [], {
      'livestock_management_group/livestock_type1':
        survey['livestock_management_group/livestock_type1'],
      'livestock_management_group/livestock_type_other1':
        survey['livestock_management_group/livestock_type_other1'],
      'livestock_management_group/livestock_in1':
        survey['livestock_management_group/livestock_in1'],
      'livestock_management_group/livestock_out1':
        survey['livestock_management_group/livestock_out1'],
      'livestock_management_group/livestock_number1':
        survey['livestock_management_group/livestock_number1'],
      'livestock_management_group/livestock_weight1':
        survey['livestock_management_group/livestock_weight1'],
      'livestock_management_group/feed_source1': survey['livestock_management_group/feed_source1'],
      'livestock_management_group/insecticide1': survey['livestock_management_group/insecticide1'],
      'livestock_management_group/livestock_management':
        survey['livestock_management_group/livestock_management'],
      'livestock_management_group/paddock_size1':
        survey['livestock_management_group/paddock_size1'],
      'livestock_management_group/residual_forage_percent1':
        survey['livestock_management_group/residual_forage_percent1'],
      'livestock_management_group/livestock2': survey['livestock_management_group/livestock2'],
      'livestock_management_group/livestock_type2':
        survey['livestock_management_group/livestock_type2'],
      'livestock_management_group/livestock_type_other2':
        survey['livestock_management_group/livestock_type_other2'],
      'livestock_management_group/livestock_in2':
        survey['livestock_management_group/livestock_in2'],
      'livestock_management_group/livestock_out2':
        survey['livestock_management_group/livestock_out2'],
      'livestock_management_group/livestock_number2':
        survey['livestock_management_group/livestock_number2'],
      'livestock_management_group/livestock_weight2':
        survey['livestock_management_group/livestock_weight2'],
      'livestock_management_group/feed_source2': survey['livestock_management_group/feed_source2'],
      'livestock_management_group/insecticide2': survey['livestock_management_group/insecticide2'],
      'livestock_management_group/livestock_management2':
        survey['livestock_management_group/livestock_management2'],
      'livestock_management_group/paddock_size2':
        survey['livestock_management_group/paddock_size2'],
      'livestock_management_group/residual_forage_percent2':
        survey['livestock_management_group/residual_forage_percent2'],
      'livestock_management_group/livestock3': survey['livestock_management_group/livestock3'],
      'livestock_management_group/livestock_more':
        survey['livestock_management_group/livestock_more'],
    });

    log('Livestock Management', 'logged');
  }

  app.progress(70);

  if (managementPractices.includes('harvest')) {
    for (let i = 1; i < 4; i++) {
      const quantities = [];
      const crops = [];

      const date = moment(survey[`harvest_group/crop_date${i}`]);
      const crop = survey[`harvest_group/crop_harvest${i}`];
      if (!crop) {
        break;
      }

      const unit = formparser.getItemLabels(`harvest_group/crop_units${i}`)[0];

      crops.push(crop);
      const y = survey[`harvest_group/crop_yield${i}`];
      quantities.push(quantity(crop, y, unit, 'rate'));

      // TODO, split this apart
      const harvestCost = survey['harvest_group/harvest_cost'];
      const cleaningCost = survey['harvest_group/cleaning_other_cost'];
      if (harvestCost) {
        quantities.push(quantity('Harvest Cost', harvestCost, 'USD'));
      }

      if (cleaningCost) {
        quantities.push(quantity('Cleaning Cost', cleaningCost, 'USD'));
      }

      const data = {
        preharvest_desiccant: survey['harvest_group/preharvest_desiccant'],
        residue_removal_method: survey['harvest_group/residue_removal_method']
          ? formparser.getItemLabels('harvest_group/residue_removal_method')
          : [],
      };

      log('Harvest', crops.join(', '));
      console.log(`date: ${date}`);

      await farm.activity(
        `Harvest on ${date.format('YYYY-MM-DD')}`,
        date,
        category.planting,
        quantities,
        data,
        null,
        true,
      );
    }
  }

  app.progress(80);
  for (let i = 1; i < 4; i++) {
    const dateraw =
      survey[
        `pest_
    disease_scouting/scout_date${i}`
      ];
    if (!dateraw) {
      continue;
    }
    const date = moment(dateraw);

    const pressure = survey[`pest_disease_scouting/pest_disease_pressure${i}`];
    if (pressure) {
      const p = formparser.getItemLabels('pest_disease_scouting/pest_disease_pressure')[0];
      await farm.observation(`Disease Pressure: ${p}`, date, category.planting, [
        quantity('Pest / Disease Pressure', pressure),
      ]);
      log('Pest / Disease Pressure', p);
    }
  }
  app.progress(90);

  await Promise.all([...seedings, ...inputs, ...observations, ...activities]);
  app.progress(100);
  log('Success', 'All looks good');
}

(async () => {
  try {
    errorCheck(err);
    await main();
    app.result(result);
  } catch (e) {
    console.error(e);
    errorExit(e);
  }
})();
