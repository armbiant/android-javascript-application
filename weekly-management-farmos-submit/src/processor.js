/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';

import { app } from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.log) {
    result.log.forEach((log) => {
      ui.info(log.title, log.content);
    });
  }

  if (result.error && result.error.length > 0) {
    for (let i = 0; i < result.error.length; i++) {
      const element = result.error[i];
      ui.error('Error', element);
    }
    ui.error('Redo', 'Please fix the errors and press the Redo Button');
    app.csvExport('errors', JSON.stringify(result.error));
    app.save();
    return;
  }
  if (result.url) {
    app.csvExport('url', result.url);
  }
  app.csvExport('result', 'success');
  app.save();
})();
