import { err, survey, regex, req, sel } from './sensor';

function yes(input) {
  if (input && (input === 'yes' || input === 'Yes')) {
    return true;
  }
  return false;
}

function checkIrrigation() {
  req('irrigation/rate');
  req('irrigation/type');
}
function checkWeedControl() {
  if (!req('weed_control/weed_control_type')) {
    return;
  }

  const types = survey['weed_control/weed_control_type'].split(' ');
  if (types.includes('herbicide')) {
    req('weed_control/herbicide_type');
    req('weed_control/herbicide_rate');
  }
}

function checkAmendments() {
  for (let i = 1; i < 4; i++) {
    if (i > 1 && !sel(`amendments/amendment_${i}`)) {
      break;
    }
    if (!req(`amendments/amendment_type_${i}`)) {
      return;
    }
    const type = survey[`amendments/amendment_type_${i}`];
    if (type === 'mulch') {
      if (!req(`amendments/mulch_type_${i}`)) {
        return;
      }
      const mulchType = survey[`amendments/mulch_type_${i}`];
      if (mulchType === 'other') {
        req(`amendments/mulch_other_${i}`);
      }
      continue;
    }

    // not mulch
    req(`amendments/name_${i}`);
    req(`amendments/method_${i}`);
    if (!req(`amendments/nutrients_${i}`)) {
      return;
    }

    const nutrients = survey[`amendments/nutrients_${i}`].split(' ');
    /*
    ['N', 'P', 'K'].forEach((n) => {
      if (nutrients.includes(n)) {
        req(`amendments/${n.toLowerCase()}_${i}`);
      }
    });
    */
    if (nutrients.includes('other')) {
      req(`amendments/nutrients_trace_${i}`);
    }
  }
}
function checkPestDiseaseScouting() {
  req('pest_disease_scouting/pest_disease_pressure');
}
function checkPestDiseaseControl() {
  req('pest_disease_control/pest_disease_reason');
  req('pest_disease_control/pest_disease_type');
  // req('pest_disease_control/pest_disease_controlled_1');
}
function checkHarvest() {
  req('harvest_group/harvest_weight_units');
  if (survey['harvest_group/harvest_weight_units'] === 'other') {
    req('harvest_group/harvest_unit_other');
  }

  req('harvest_group/harvest_weight');
  req('harvest_group/harvest_area_units');
  req('harvest_group/harvest_area');
  if (survey['harvest_group/harvest_area_units'] === 'plant') {
    // req('harvest_group/row_spacing_unit');
    // req('harvest_group/row_spacing');
  }
  req('harvest_group/harvest_complete');
}
function checkThinPrune() {
  req('thin_prune_group/pruning_activity');
}

export default function errorCheck() {
  req('planting');
  if (!survey.planting.match(regex.planting_id)) {
    err('unable to parse planting<br>Format: Planting Name (serverid, plantingid)');
  }

  req('which_week');
  if (!req('management_practices')) {
    return;
  }

  const practices = survey.management_practices.split(' ');
  if (practices.includes('irrigation')) {
    checkIrrigation();
  }
  if (practices.includes('weed_control')) {
    checkWeedControl();
  }
  if (practices.includes('amendment')) {
    checkAmendments();
  }
  if (practices.includes('pest_disease_scouting')) {
    checkPestDiseaseScouting();
  }
  if (practices.includes('pest_disease_control')) {
    checkPestDiseaseControl();
  }
  if (practices.includes('harvest')) {
    checkHarvest();
  }
  if (practices.includes('thin_prune')) {
    checkThinPrune();
  }
}
