/* global processor */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

const hres = {
  series: [result.high_res],
};

const lres = {
  series: [result.low_res],
};

ui.plot(hres, 'CO2 concentration in ppm, High Resolution over 16 min 40 s', { min: 0, max: 5000 });
ui.plot(lres, 'CO2 concentration in ppm, Low Resolution over 60h', { min: 0, max: 5000 });

app.save();
