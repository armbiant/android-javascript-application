import 'chartist/dist/chartist.min.css';
import 'chartist-plugin-fill-donut';
import 'materialize-css/dist/css/materialize.css';
import 'materialize-css/dist/js/materialize';

import '../css/style.css';

const Chartist = require('chartist');
const $ = require('jquery');

const container = $('<div></div>');
container.attr('id', 'message-container');
container.prependTo($('body'));

function message(title, text, color, icon) {
  const card = $(`<div style='vertical-align: top; margin: 16px; margin-bottom: 0; background-color: ${color};' class='card wide'><div>`);
  const inner = $("<div class='card-content white-text'></div>");
  card.append(inner);
  inner.append(`<span class='card-title' style='display: flex; '><i class='material-icons' style='font-size: 32px; margin-right: 12px;'>${icon}</i>${title}</span>`);
  inner.append(`<p>${text}</p>`);
  $('body').append(card);
}

module.exports.info = (title, text) => {
  message(title, text, '#03A9F4', 'info');
};

module.exports.warning = (title, text) => {
  message(title, text, '#FFC107', 'warning');
};

module.exports.error = (title, text) => {
  message(title, text, '#F44336', 'error');
};

let donutCount = 0;
let plotCount = 0;

function createCard(title, text, innerId) {
  const card = $("<div style='margin: 16px; margin-bottom: 0;' class='card primary-bg wide'><div>");
  const inner = $("<div class='card-content white-text'></div>");
  card.append(inner);
  inner.append(`<span class='card-title'>${title}</span>`);
  inner.append(`<p>${text}</p>`);
  inner.append(`<div id='${innerId}' class='donut'></div>`);
  return card;
}

module.exports.linspace = (start, stop, n) => {
  const arr = [];
  const increment = (stop - start) / n;

  for (let i = 0; i < n; i += 1) {
    arr.push(i * increment + start);
  }

  return arr;
};

module.exports.plot = (
  data,
  title,
  {
    min = null,
    max = null,
    axisX = {
      labelInterpolationFnc() {
        return null;
      },
    },
  } = {},
) => {
  const id = `plot_${plotCount}`;
  plotCount += 1;

  const card = createCard(title, '', id);
  $('body').append(card);

  $(`#${id}`).addClass('ct-chart ct-golden-section plot');

  Chartist.Line(`#${id}`, data, {
    axisX,
    low: min == null ? undefined : min,
    high: max == null ? undefined : max,
  });
};

module.exports.barchart = (
  data,
  title,
  {
    min = null,
    max = null,
    axisX = {
      labelInterpolationFnc() {
        return null;
      },
    },
    seriesBarDistance = 20,
  } = {},
) => {
  const id = `plot_${plotCount}`;
  plotCount += 1;

  const card = createCard(title, '', id);
  $('body').append(card);

  $(`#${id}`).addClass('ct-chart ct-golden-section plot');

  Chartist.Bar(`#${id}`, data, {
    seriesBarDistance,
    axisX,
    low: min == null ? undefined : min,
    high: max == null ? undefined : max,
  });
};

module.exports.donut = (text, value, pct, icon) => {
  const id = `donut_${donutCount}`;
  donutCount += 1;

  const card = createCard(text, '', id);
  $('body').append(card);

  const x1 = 220 * pct;
  const x2 = 220 * (1 - pct);

  Chartist.Pie(
    `#${id}`,
    {
      series: [x1, x2],
      labels: ['', ''],
    },
    {
      donut: true,
      donutWidth: 20,
      startAngle: 210,
      total: 260,
      showLabel: false,
      plugins: [
        Chartist.plugins.fillDonut({
          items: [
            {
              content: `<i class="material-icons">${icon}</i>`,
              position: 'bottom',
              offsetY: 10,
              offsetX: -2,
            },
            {
              content: value,
            },
          ],
        }),
      ],
    },
  );
};

module.exports.button = (text, cb = () => {}) => {
  const button = $(`<a style="margin: 1.5em" class="waves-effect waves-light btn primary-bg wide">${text}</a>`);
  $('body').append(button);
  button.click(cb);
};

module.exports.chunkArray = (myArray, size) => {
  const target = myArray.slice(0);
  const results = [];

  while (target.length) {
    results.push(target.splice(0, size));
  }

  return results;
};
