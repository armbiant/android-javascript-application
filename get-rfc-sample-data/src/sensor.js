/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';
// import serial from './lib/serial';
// import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

import {
  app,
  oursci
} from '@oursci/scripts'; // expose this to android for calling onDataAvailable
//  math

// export default serial; // expose this to android for calling onDataAvailable

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

result.error = {}; // put error information here.
const requiredAnswersText = ['sample_id'];
// const requiredAnswersNumber = [];

(async () => {
  // First - go get collection survey that we're checking our sample IDs against
  const survey_full = await oursci('2019-rfc-lab-wet-chemistry');
  // just keep the information we need
  // proteins is optional, so create a dummy if it doesn't exist
  if (typeof survey_full['assay.data.proteins'] === 'undefined') {
    survey_full['assay.data.proteins'] = [];
  }
  //  console.log(survey_full);

  const survey = {
    date: survey_full['assay.meta.date'],
    error: survey_full['assay.data.high_range_error'],
    sample_id: survey_full.sample_id,
    sample: survey_full.sample_super_anti_poly,
    dilution: survey_full.dilution_super_anti_poly,
    polyphenols: survey_full['assay.data.polyphenols'],
    antioxidants: survey_full['assay.data.antioxidants'],
    proteins: survey_full['assay.data.proteins'],
  };

  console.log(survey.sample_id);

  // now create a place to keep sample IDs
  result.found = {
    date: [],
    error: [],
    duplicates: {
      polyphenols: [],
      antioxidants: [],
      proteins: [],
      supernatant: [],
    },
    sample_id: [],
    sample: [],
    dilution: [],
    polyphenols: [],
    antioxidants: [],
    proteins: [],
    supernatant: [],
  };

  //  console.log(survey);

  // pull in text and number answers for all our questions

  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a).toString();
      if (app.getAnswer(a) === '') {
        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  // requiredAnswersNumber.forEach((a) => {
  //   let v;
  //   if (typeof app.getAnswer(a) !== 'undefined') {
  //     v = app.getAnswer(a);
  //     if (app.getAnswer(a) === '') {
  //       result.error[a] = 'missing';
  //     } else if (isNaN(app.getAnswer(a))) {
  //       result.error[a] = 'not a number';
  //     }
  //   } else {
  //     v = 'undefined';
  //     result.error[a] = 'undefined';
  //   }
  //   console.log(v);
  //   result[a] = v;
  // });

  //  console.log(Object.keys(result.error));

  // First make sure the sample ID question in this survey isn't missing
  if (!Object.keys(result.error).includes(result.sample_id)) {
    // now let's save all the values for anything with that sample ID
    survey.sample_id.forEach((a, index) => {
      //      console.log(`${a} and ${result.sample_id}`);
      if (a === result.sample_id) {
        console.log(`found ${a}`);
        result.found.date.push(survey.date[index].slice(11, 16));
        result.found.error.push(survey.error[index]);
        result.found.sample_id.push(survey.sample_id[index]);
        result.found.sample.push(survey.sample[index]);
        result.found.dilution.push(survey.dilution[index]);
        result.found.polyphenols.push(survey.polyphenols[index]);
        result.found.antioxidants.push(survey.antioxidants[index]);
        if (survey.proteins[index] === undefined) {
          // proteins are optional
          result.found.proteins.push('');
        } else {
          result.found.proteins.push(survey.proteins[index]);
        }
      }
    });
  }

  //  console.log(result.found);

  // now mark non-error duplicates (same ID AND same sample type AND no errors)
  let count_proteins = 0;
  let count_antioxidants = 0;
  let count_polyphenols = 0;
  let count_supernatant = 0;

  result.found.error[0] = '';
  result.found.error[1] = '';

  result.found.sample.forEach((a, index) => {
    if (!result.found.error[index]) {
      // don't count duplicates if they have errors - only non-errors
      if (a === 'antioxidants') {
        count_antioxidants++;
        if (count_antioxidants > 1) {
          result.found.duplicates[a].push(survey.error[index]);
          result.found.error[index] = 'duplicate';
        }
      } else if (a === 'polyphenols') {
        count_polyphenols++;
        if (count_polyphenols > 1) {
          result.found.duplicates[a].push(survey.error[index]);
          result.found.error[index] = 'duplicate';
        }
      } else if (a === 'proteins') {
        count_proteins++;
        if (count_proteins > 1) {
          result.found.duplicates[a].push(survey.error[index]);
          result.found.error[index] = 'duplicate';
        }
      } else if (a === 'supernatant') {
        count_supernatant++;
        if (count_supernatant > 1) {
          result.found.duplicates[a].push(survey.error[index]);
          result.found.error[index] = 'duplicate';
        }
      }
    }
  });

  // First - let's parse out results from get-rfc-sample-data and make it available in a simpler form
  // RULE - higher dilution samples always is selected.  If tie, then newer sample is selected.

  const count = {
    antioxidants: 0,
    polyphenols: 0,
    proteins: 0,
  };
  const savedValues = {
    antioxidants_value: 0,
    antioxidants_dilution: 0,
    polyphenols_value: 0,
    polyphenols_dilution: 0,
    proteins_value: 0,
    proteins_dilution: 0,
  };

  result.found.sample.forEach((a, index) => {
    if (a === 'antioxidants') {
      if (count.antioxidants === 0 || result.found.dilution[index] > savedValues.antioxidants_dilution) { // if first time, or dilution is higher or equal to old one... then save new value
        savedValues.antioxidants_value = result.found.antioxidants[index];
        savedValues.antioxidants_dilution = result.found.dilution[index];
        app.csvExport('antioxidants_value', savedValues.antioxidants_value);
        app.csvExport('antioxidants_dilution', savedValues.antioxidants_dilution);
        count.antioxidants++;
      }
    }
    if (a === 'polyphenols') {
      if (count.polyphenols === 0 || result.found.dilution[index] > savedValues.polyphenols_dilution) { // if first time, or dilution is higher or equal to old one... then save new value
        savedValues.polyphenols_value = result.found.polyphenols[index];
        savedValues.polyphenols_dilution = result.found.dilution[index];
        app.csvExport('polyphenols_value', savedValues.polyphenols_value);
        app.csvExport('polyphenols_dilution', savedValues.polyphenols_dilution);
        count.polyphenols++;
      }
    }
    if (a === 'proteins') {
      if (count.proteins === 0 || result.found.dilution[index] > savedValues.proteins_dilution) { // if first time, or dilution is higher or equal to old one... then save new value
        savedValues.proteins_value = result.found.proteins[index];
        savedValues.proteins_dilution = result.found.dilution[index];
        app.csvExport('proteins_value', savedValues.proteins_value);
        app.csvExport('proteins_dilution', savedValues.proteins_dilution);
        count.proteins++;
      }
    }
  });
  app.csvExport('found', JSON.stringify(result.found));
  console.log(JSON.stringify(result.found));

  //  console.log(result);
  app.result(result);
})();