/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/*
import {
  sprintf
} from 'sprintf-js';
import _ from 'lodash';
*/

import {
  app
} from '@oursci/scripts';
import * as ui from '@oursci/measurements-ui';
import '@oursci/measurements-ui/dist/styles.min.css';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // if there are duplicate supernatants - show error (use must fix them)
  if (result.found.duplicates.supernatant && result.found.duplicates.supernatant.length) {
    app.error();
    ui.error('Supernatant duplicates', 'There are two supernatants found.  Please review and archive one');
  }
  // if there are any duplicates in anti or poly, show warning + inform use
  if (result.found.duplicates.antioxidants.length || result.found.duplicates.polyphenols.length) {
    //    app.warning();
    ui.warning('Poly or Anti duplicates', 'Please review below.  The samples with highest dilution will be chosen.  If tied, you should archive one to remove the problem');
  }
  // if any of the measurements are missing, show a warning
  if (!(result.found.sample.includes('antioxidants') && result.found.sample.includes('polyphenols') && result.found.sample.includes('supernatant'))) {
    //    app.warning();
    ui.warning('Samples missing', 'Anti, Poly, or Supernatant is missing.  See below for details and follow up on sample ID to identify missing info.');
  }

  let anti_text = '';
  let poly_text = '';
  let prot_text = '';
  let super_text = '';

  ui.info(`Total ${result.found.sample.length} Wet Chem found with ID ${result.sample_id}`, '');
  result.found.sample.forEach((row, index) => {
    if (result.found.sample[index].toString() === 'antioxidants') {
      //    text += `${result.found.sample[index].toString()}: ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antixidants[index].toString()} ${result.found.proteins[index].toString()}<br>`
      anti_text += `${result.found.sample[index].toString()}: <span style=background-color:red>${result.found.error[index].toString()}</span> ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antioxidants[index].toString()} ${result.found.proteins[index].toString()}<br>`;
    } else if (result.found.sample[index].toString() === 'polyphenols') {
      //    text += `${result.found.sample[index].toString()}: ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antixidants[index].toString()} ${result.found.proteins[index].toString()}<br>`
      poly_text += `${result.found.sample[index].toString()}: <span style=background-color:red>${result.found.error[index].toString()}</span> ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antioxidants[index].toString()} ${result.found.proteins[index].toString()}<br>`;
    } else if (result.found.sample[index].toString() === 'supernatant') {
      //    text += `${result.found.sample[index].toString()}: ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antixidants[index].toString()} ${result.found.proteins[index].toString()}<br>`
      super_text += `${result.found.sample[index].toString()}: <span style=background-color:red>${result.found.error[index].toString()}</span> ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}<br>`;
    } else if (result.found.sample[index].toString() === 'proteins') {
      //    text += `${result.found.sample[index].toString()}: ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antixidants[index].toString()} ${result.found.proteins[index].toString()}<br>`
      prot_text += `${result.found.sample[index].toString()}: <span style=background-color:red>${result.found.error[index].toString()}</span> ${result.found.dilution[index].toString()}, ${result.found.date[index].toString()}, ${result.found.polyphenols[index].toString()} ${result.found.antioxidants[index].toString()} ${result.found.proteins[index].toString()}<br>`;
    }
  });

  ui.info('Antioxidants', anti_text);
  ui.info('Polyphenols', poly_text);
  ui.info('Supernatant', super_text);
  ui.info('Proteins', prot_text);
  ui.info('FYI - If multiple samples...', 'The highest dilution is always chosen.  If two have the same dilution, you should archive one.');

  app.save();
})();