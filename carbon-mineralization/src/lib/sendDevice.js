/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */

//  [{ 'environmental_array': [['co2']], pulses: [24], 'pulse_distance': [5000000], 'pulsed_lights': [[1]] }];


export default () => {
  const co2_burst_24 = [{
    environmental_array: [
      ['co2'],
    ],
    pulses: [24],
    pulse_distance: [5000000],
    pulsed_lights: [
      [1],
    ],
  }];

  return {
    co2_burst_24,
  };
};