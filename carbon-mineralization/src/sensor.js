/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

import app from './lib/app';
import serial from './lib/serial';
// import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
import {
  sleep,
} from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM1', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  // ///////////////////////////////////////////////////////
  // Multiple measurements are saved from this single measurement script
  // Select a serial.write command below based on what you are updating to the Our Sci database.
  // Then copy paste the associated manifest (listed at the very bottom) and save it to the manifest.json file in src folder
  // Finally hit ctrl-b "compile and upload" and check the website app.our-sci.net to confirm.
  const send_to_serial = sendDevice().co2_burst_24;
  serial.write(JSON.stringify(send_to_serial));
  console.log(JSON.stringify(send_to_serial));

  // ///////////////////////////////////////////////////////
  // Mark the progress of pulling in the data
  let count = 0;
  try {
    app.progress(0.1);
    const result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + count / 617.0 * 100.0 * 0.9);
    });
    app.progress(100);
    // ///////////////////////////////////////////////////////


    // ///////////////////////////////////////////////////////
    // Create variable "processed", and to pass information from sensor.js to processor.js in results.json
    // result.sample[0].processed = {};

    console.log(result);

    app.result(result);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();