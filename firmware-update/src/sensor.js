/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable no-unreachable */
/* eslint-disable no-await-in-loop */

// import scripts from '@oursci/scripts';
import app from './lib/app';
import serial from './lib/serial';

import { sleep } from './lib/utils'; // use: await sleep(1000);

let fw = '';
if (app.isAndroid) {
  fw = require('./reflectance-spec-firmware.ino.hex');
} else {
  const fs = require('fs');
  fw = fs.readFileSync('./src/reflectance-spec-firmware.ino.hex', 'utf-8').toString();
}

fw = fw.trim();

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  await sleep(1000);

  if (!fw) {
    console.log('unable to load firmware');
  }

  console.log(`fw len: ${fw.length}`);

  const count = 0;
  try {
    app.progress(10);

    const lines = fw.split('\n');

    // start flash
    serial.write('1078+');

    for (;;) {
      const r = await serial.readLine();
      console.log(r);
      if (r === 'waiting for intel hex lines') {
        break;
      }
    }

    for (let i = 0; i < lines.length; i++) {
      serial.write(`${lines[i]}\n`);
      await sleep(2);
      console.log(`sending ${lines[i]}`);
      app.progress(10 + (i * 80.0) / lines.length);
    }

    for (;;) {
      const r = await serial.readLine();
      console.log(r);
      if (r.startsWith(`done, ${lines.length} hex lines`)) {
        break;
      }
    }

    console.log(`:flash ${lines.length}`);
    serial.write(`:flash ${lines.length}\n`);

    await sleep(5000);
    serial.write('device_info+\r');

    app.result({
      status: 'ok',
      len: lines.length,
    });
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
