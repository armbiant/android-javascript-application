/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  console.log(result);
  if (result.errors) {
    if (Object.keys(result.errors).length > 0) {
      Object.keys(result.errors).forEach((k) => {
        console.log(k);
        ui.error(k, result.errors[k]);
      });
      app.save();
      return;
    }
  }

  if (!result.sampleID) {
    ui.error('SampleID missing');
    app.save();
    return;
  }

  if (!result.detail) {
    ui.error('Details missing');
    app.save();
    return;
  }

  // ui.info('sampleID', result.sampleID);

  Object.keys(result.detail).forEach((k) => {
    ui.info(k, result.detail[k]);
    app.csvExport(k, result.detail[k]);
  });

  app.save();
})();
