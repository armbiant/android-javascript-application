/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This example demonstrates how to fetch data in the sensor script
 * from google sheets.
 * Using the sheet from
 * https://docs.google.com/spreadsheets/d/16f-BMZ7O1G3H27fEZEjo43S5U9gclv4Qdu0v8qq6a1U/edit?usp=sharing
 * Which contains multiple pages (makes processing a bit more challenging)
 */
import axios from 'axios';

import app from './lib/app';
import serial from './lib/serial';
import sheets from './lib/sheets';

export default serial; // expose this to android for calling onDataAvailable

const measurement = JSON.parse(app.getAnswer('scan_vis')).data;
console.log(measurement);

let host = app.getEnv('plumber-host', '');
if (!app.isAndroid) {
  host = '02e5a99a.ngrok.io';
}

if (!measurement) {
  app.result({
    error: 'Please run measurement firts',
  });
}

(async () => {
  if (!host) {
    app.setEnv('plumber-host', '');
    app.result({
      error: 'Please set plumber-host Environment Variable',
    });
    return;
  }

  console.log(measurement);
  const res = {};
  const payload = Object.keys(measurement).forEach((k) => {
    if (k.startsWith('median_')) {
      console.log(`k is ${k}`);
      res[`nm${k.split('_')[1]}`] = Number.parseFloat(measurement[k]);
    }
  });

  console.log(res);
  const answer = await axios({
    method: 'post',
    url: `http://${host}/predict`,
    data: res,
  });

  app.result({
    toc: answer.data[0],
  });
})();
