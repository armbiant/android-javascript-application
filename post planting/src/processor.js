/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  Object.keys(result.error).forEach((a) => {
    ui.error(`Answer '${a}' is ${result.error[a]}`, 'Return to the answer and fix it.');
  });

  const plot_area = result.Ridges * (result.ridge_distance / 100) * result.ridge_length;
  ui.info('Plot Area m2', MathMore.MathROUND(plot_area, 2));
  app.csvExport('Plot Area m2', MathMore.MathROUND(plot_area, 2));

  const plant_population = (10000 / plot_area) * result.primary_population;
  ui.info('Plant Population Ha', MathMore.MathROUND(plant_population, 2));
  app.csvExport('Plant Population Ha', MathMore.MathROUND(plant_population, 2));

  const fertilizer_topdress = ((result.fertilizer * 1000) / plot_area) * 10;
  ui.info('Urea Fertilizer kg/ha', MathMore.MathROUND(fertilizer_topdress, 2));
  app.csvExport('Urea Fertilizer kg/ha', MathMore.MathROUND(fertilizer_topdress, 2));

  app.save();
})();
