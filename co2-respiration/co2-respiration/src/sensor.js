/* eslint-disable no-await-in-loop */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
// import { sleep } from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    baudRate: 9600,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  const samplesLen = 10.0;

  try {
    const res = {};
    res.sensor1 = [];
    res.sensor2 = [];
    for (let i = 0; i < 2; i += 1) {
      // skip first 3 lines
      await serial.readLine();
    }

    for (let i = 0; i < samplesLen; i += 1) {
      const l = (await serial.readLine()).trim().split(' ');

      app.progress(i / samplesLen * 100.0);
      console.log(`read ${l}`);
      res.sensor1.push(l[0]);
      res.sensor2.push(l[1]);
    }

    console.log(`res ${JSON.stringify(res)}`);
    app.result(res);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
