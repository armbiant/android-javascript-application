/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  const meas = result.sample[0];
  const processed = meas.processed;
  const data = meas.data_raw;
  const wavelengths = processed.wavelengths;

  // ///////////////////////////////////////////////////////
  // Error checking
  // Make sure this is the right device for this protocol.  Save errors to the warnings array to be displayed and saved later
  if (typeof result.device_name === 'undefined') {
    const warningText = '"device_name" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
    processed.warnings.push(warningText);
  }
  if (result.device_name.toString() !== 'Reflectometer') {
    const warningText = 'This script was intended to be used on the reflectometer!  This device is a ' + result.device_name.toString();
    processed.warnings.push(warningText);
  }
  if (typeof result.device_version === 'undefined') {
    const warningText = '"device_version" is expected from the device, but I don\'t see it in the output result.  Check the protocol and ensure it is requested';
    processed.warnings.push(warningText);
  }
  if (typeof result.device_id === 'undefined') {
    const warningText = '"device_id" is expected from the device, but I don\'t see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.  If using Our Sci firmware, use the set_device_info+<deviceID>+ command to assigned ID';
    processed.warnings.push(warningText);
  }
  if (typeof result.device_firmware === 'undefined') {
    const warningText = '"device_firmware" is expected from the device, but I don\'t see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.';
    processed.warnings.push(warningText);
  }

  // Print all errors and info staetments's up to this point and save them to the database
  if (typeof processed.warnings[0] !== 'undefined') {
    for (let i = 0; i < processed.warnings.length; i++) {
      ui.warning('Warning', processed.warnings[i]);
      app.csvExport('warnings', processed.warnings.toString());
    }
  }
  if (typeof processed.info[0] !== 'undefined') {
    for (let i = 0; i < processed.info.length; i++) {
      ui.warning('Info', processed.info[i]);
      app.csvExport('info', processed.info.toString());
    }
  }
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // identify calibration type for this measurement (if any), and save to the database
  if (meas.calibration === 1) {
    ui.info('this is a white card calibration', ' ');
  } else if (meas.calibration === 2) {
    ui.info('this is a black card calibration', ' ');
  } else if (meas.calibration === 3) {
    ui.info('this is a blank calibration', ' ');
  } else if (meas.calibration === 4) {
    ui.info('this is a white MASTER card calibration', ' ');
  } else if (meas.calibration === 5) {
    ui.info('this is a black MASTER card calibration', ' ');
  }
  if (typeof meas.calibration !== 'undefined') {
    app.csvExport('calibration', meas.calibration);
  }
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Display and save the data
  // If it's a calibration 1 or 2, then it's 0 - 65535 for the results
  // If it's a calibration 3 or 0/undefined (normal measurement), it's -20 - 120
  ui.info(
    'Device Info',
    ' ID: ' +
    result.device_id +
    ' Version: ' +
    result.device_version +
    ',Firmware: ' +
    result.device_firmware,
  );
  if (typeof result.device_battery !== 'undefined') {
    app.csvExport('device_battery', result.device_battery);
  }
  app.csvExport('device_id', result.device_id);
  app.csvExport('device_version', result.device_version);
  app.csvExport('device_firmware', result.device_firmware);
  // If master calibration of cards, display the calibration values so they can be copied to a QR code and printed on the card.
  if (meas.calibration === 4 || meas.calibration === 5) {
    ui.info('Calibration Card Values', processed.masterCard);
    app.csvExport('calibration_card_values', processed.masterCard);
  }
  // Save the median and stdev from each wavelength
  for (let i = 0; i < processed.median.length; i++) {
    app.csvExport('median_' + processed.wavelengths[i], processed.median[i]);
  }
  for (let i = 0; i < processed.median.length; i++) {
    app.csvExport('three_stdev' + processed.wavelengths[i], processed.three_stdev[i]);
  }

  // FOR NOW this is auto-scaling... but that makes readability hard for users... better to pre-scale consistently.
  // problem is, we need another identifier for the scale (we can base it on calibration, but when we request raw normal measurements it gets out of scale)
  // needs some work here.

  const minAvg = Math.min(...processed.median);
  const maxAvg = Math.max(...processed.median);
  ui.plot({
      series: [processed.median],
    },
    'Average Spectral Values', {
      min: minAvg,
      max: maxAvg,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${wavelengths[index]}`;
        },
      },
    },
  );
  ui.plot({
      series: [data],
    },
    'Raw Spectral Results', {
      //    min: 0,
      //    max: 66000,
    },
  );
  const minStd = Math.min(...processed.three_stdev);
  const maxStd = Math.max(...processed.three_stdev);
  ui.plot({
      series: [processed.three_stdev],
    },
    'Spectral Noise (3 st. dev.)', {
      min: minStd,
      max: maxStd,
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${wavelengths[index]}`;
        },
      },
    },
  );

  ui.info('Three stdev', processed.three_stdev.map(e => sprintf('%.2f,', e)).join('<br>'));
  ui.info('Medians', processed.median.map(e => sprintf('%.2f,', e)).join('<br>'));
  if (typeof result.memory !== 'undefined') {
    ui.info('Memory', JSON.stringify(result.memory, null, 2).replace(/(?:\r\n|\r|\n)/g, '<br>'));
  }

  if (typeof meas.temperature !== 'undefined') {
    ui.info('Temperature', sprintf('%.2f C', meas.temperature));
    app.csvExport('temperature', meas.temperature);
    ui.info('Humidity', sprintf('%.2f %%', meas.humidity));
    app.csvExport('humidity', meas.humidity);
    ui.info('Pressure', sprintf('%.2f', meas.pressure));
    app.csvExport('pressure', meas.pressure);
    ui.info('VOC', sprintf('%.2f', meas.voc));
    app.csvExport('voc', meas.voc);
  }
  app.save();

})();