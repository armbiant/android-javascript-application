/* eslint-disable no-await-in-loop */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

import app from './lib/app';
import serial from './lib/serial';
// import { sleep } from './lib/utils'; // use: await sleep(1000);

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    baudRate: 9600,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  const samplesLen = 150.0;
  let count = 0;

  try {
    serial.write('a');
    const res = await serial.readStreamingJson('low_res[*]', (s) => {
      count += 1;
      app.progress(count / samplesLen * 100.0);
      console.log(`received: ${s}`);
    });

    app.result(res);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
