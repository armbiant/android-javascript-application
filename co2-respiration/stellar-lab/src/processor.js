/* global processor */
/* eslint-disable prefer-destructuring */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

const values = result.values;

ui.barchart({ series: [Object.values(values)] }, 'Intensities', {
  min: 0,
  max: 200,
  seriesBarDistance: 2,
  axisX: {
    labelInterpolationFnc(val, index) {
      if (index % 300 === 0) {
        return `${Object.keys(values)[index]}`;
      }
      return null;
    },
  },
});

const parts = 3;
const partLen = Object.keys(values).length / parts;

for (let i = 0; i < parts; i++) {
  const k = Object.keys(values).slice(i * partLen, (i + 1) * partLen);
  const v = Object.values(values).slice(i * partLen, (i + 1) * partLen);

  ui.barchart({ series: [v] }, `Intensities ${i + 1} / ${parts}`, {
    min: 0,
    max: 200,
    seriesBarDistance: 2,
    axisX: {
      labelInterpolationFnc(val, index) {
        if (index % 100 === 0) {
          return `${k[index]}`;
        }
        return null;
      },
    },
  });
}

app.save();
