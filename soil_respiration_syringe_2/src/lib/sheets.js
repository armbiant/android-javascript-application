import tabletop from 'tabletop';

export default id =>
  new Promise((resolve, reject) => {
    const docsUrl = `https://docs.google.com/spreadsheets/d/${id}/edit?usp=sharing`;

    try {
      tabletop.init({
        key: docsUrl,
        callback: (data) => {
          resolve(data);
        },
        simpleSheet: true,
      });
    } catch (error) {
      reject(error);
    }
  });
