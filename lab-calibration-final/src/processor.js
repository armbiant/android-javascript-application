/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    console.log(result);
    app.save();
    return;
  }

  // Loop through all the lights and all the calibration values (5 calibration values, 10 lights) and make sure there are no nans or non-numbers... inform the user if there are.
  let errors = 0;
  for (let j = 1; j < 21; j++) {
    if (isNaN(Number(result['userdef' + j]))) {
      errors += 1;
    }
  }
  if (errors > 0) {
    ui.warning('Something is Wrong :\\', errors + ' saved values  are not numbers (NaN) but should be... something either went wrong with the calibration script or the device did not respond.');
  } else {
    ui.warning('Success!', 'Calibration information has been probably been saved.  Just double check below that the values are those generated during the model fit.');
    for (let j = 1; j < 11; j++) {
      ui.info('slope: ', result['userdef[' + j + 10 + ']'] + '   yint: ' + result['userdef[' + j + ']']);
    }
  }
  ui.info('All stored info on device', JSON.stringify(result));
})();