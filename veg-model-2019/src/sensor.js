/* eslint-disable linebreak-style */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-unreachable */

/**
 * This is a simple serial sensor script
 * The firmware for the corresponding sensor can be found
 * in arduino-sketch/arduino-sketch.ino
 *
 * See the tutorial over here
 * https://gitlab.com/our-sci/measurement-scripts/tree/master/hello-world
 */

/** Greg's notes
 * added //DEBUG to quiet console log statements here and in libraries app, process-wavelengths, and serial.
 */

// import moment from 'moment';
// import _ from 'lodash';
// import mathjs from 'mathjs';
// import serial from './lib/serial';
// import app from './lib/app';
// import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import * as utils from './lib/utils'; // use: await sleep(1000);
// import soilgrid from './lib/soilgrid';
// import weather from './lib/weather';
// import oursci from './lib/oursci';

import {
  app,
  oursci,
} from '@oursci/scripts'; // expose this to android for calling onDataAvailable
//  math

// export default serial; // expose this to android for calling onDataAvailable

/*
// if in dev environment, take this survey result as example
const uuidSnippet = 'b43a8151-b4ad-4b04-bf07-18a8e3fa3024';
const form = '2019-produce-analysis';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

// import surveyHelper from "../.oursci-surveys/survey";
// export const survey = surveyHelper();
*/

const result = {};

function errorExit(error) {
  app.result({
    error,
  });
}

result.error = {}; // put error information here.
const requiredMeasurements = [
  'cup_wt_ohaus',
  'pre_weight_ohaus',
  'post_weight_ohaus',
  'sample_weight_ohaus',
  'get_chemisty', // <-- NOTE THIS chemist'R'y ... we can't change it just be aware of the missing R!
];
const requiredAnswersText = ['sample_type'];
const requiredAnswersNumber = [
  'extractant',
  'cup_wt_manual',
  'pre_weight_manual',
  'post_weight_manual',
  'sample_weight_manual',
];

(async () => {
  requiredMeasurements.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      //      console.log(a);
      //      console.log(app.getAnswer(a));
      v = JSON.parse(app.getAnswer(a));
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  requiredAnswersText.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        result.error[a] = 'missing';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  requiredAnswersNumber.forEach((a) => {
    let v;
    if (typeof app.getAnswer(a) !== 'undefined') {
      v = app.getAnswer(a);
      if (app.getAnswer(a) === '') {
        result.error[a] = 'missing';
      } else if (isNaN(app.getAnswer(a))) {
        result.error[a] = 'not a number';
      }
    } else {
      v = 'undefined';
      result.error[a] = 'undefined';
    }
    console.log(v);
    result[a] = v;
  });

  // set the final antioxidant values to reference...
  result.finalResults = {
    antioxidants: {
      value: '',
      dilution: '',
    },
    polyphenols: {
      value: '',
      dilution: '',
    },
    proteins: {
      value: '',
      dilution: '',
    },
  };

  console.log(`all final results: ${JSON.stringify(result.finalResults)}`);

  // make sure that these exist before you try to overwrite them
  if (typeof result.get_chemisty.data.antioxidants_value !== 'undefined') {
    result.finalResults.antioxidants.value = result.get_chemisty.data.antioxidants_value;
  }
  if (typeof result.get_chemisty.data.antioxidants_dilution !== 'undefined') {
    result.finalResults.antioxidants.dilution = result.get_chemisty.data.antioxidants_dilution;
  }
  if (typeof result.get_chemisty.data.polyphenols_value !== 'undefined') {
    result.finalResults.polyphenols.value = result.get_chemisty.data.polyphenols_value;
  }
  if (typeof result.get_chemisty.data.polyphenols_dilution !== 'undefined') {
    result.finalResults.polyphenols.dilution = result.get_chemisty.data.polyphenols_dilution;
  }
  if (typeof result.get_chemisty.data.proteins_value !== 'undefined') {
    result.finalResults.proteins.value = result.get_chemisty.data.proteins_value;
  }
  if (typeof result.get_chemisty.data.proteins_dilution !== 'undefined') {
    result.finalResults.proteins.dilution = result.get_chemisty.data.proteins_dilution;
  }

  console.log(`all final results: ${JSON.stringify(result.finalResults)}`);

  const types = {
    carrot: {
      norm_weight: 0.883,
    },
    spinach: {
      norm_weight: 0.914,
    },
    kale: {
      norm_weight: 0.896,
    },
    lettuce: {
      norm_weight: 0.95,
    },
    cherry_tomato: {
      norm_weight: 0.95,
    },
    grape: {
      norm_weight: 0.82,
    },
  };

  // Set final normal weight to pass to processor
  console.log(`result sample type: ${result.sample_type}`);
  if (typeof types[result.sample_type] !== 'undefined') {
    result.finalResults.norm_weight = types[result.sample_type].norm_weight;
  } else {
    console.log(
      'no valid sample type - must choose valid sample type (carrot, kale, lettuce, tc. etc.)',
    );
    result.finalResults.norm_weight = 0.92;
  }

  console.log(JSON.stringify(result));
  app.result(result);
})();

// unit tests for updating protocol

/*
missing weights
missing sample type
text instead of numbers for weights
missing measurements
missing 'antioxidant extimate' value (no rfc lab)
no sample type
everything correct (weights, etc. etc.)
*/