/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

/* global processor */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const soilValueMap = {
  sandy: 1,
  silty: 3,
  clay: 2,
};

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }

  const errors = {};

  const field1 = {};
  const field2 = {};

  const weightedValued = 0;

  let soilType = result['field1_soil'];
  const field1_value = soilValueMap[soilType];

  soilType = result['field2_soil'];
  const field2_value = soilValueMap[soilType];

  soilType = result['field3_soil'];
  const field3_value = soilValueMap[soilType];

  const labels = ['Field 1', 'Field 2', 'Field 3'];
  const series = [field1_value, field2_value, field3_value];
  ui.barchart(
    {
      series: [series],
    },
    'Field Values',
    {
      axisX: {
        labelInterpolationFnc(value, index) {
          return `${labels[index]}`;
        },
      },
    },
  );

  ui.donut('Field 1', `VAL ${result.field1.value}`, 0.2);
  ui.info('Field 1 Value', result.field1.value);
  ui.info('Field 2 Value', result.field2.value);
  ui.info('Recommendation for field1', result.recommendations.fieldOne);

  app.save();
})();
