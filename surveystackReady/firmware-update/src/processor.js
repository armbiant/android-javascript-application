/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import app from './lib/app';
import * as ui from './lib/ui';
// import * as MathMore from './lib/math';

/**
 * flatten works for objects and arrays
 * @param {object} data the object you pass
 */
Object.flatten = function (data) {
  var out = {};

  function recurse(cur, prop) {
    if (Object(cur) !== cur) {
      out[prop] = cur;
    } else if (Array.isArray(cur)) {
      for (var i = 0, l = cur.length; i < l; i++)
        recurse(cur[i], prop + "[" + i + "]");
      if (l == 0)
        out[prop] = [];
    } else {
      var isEmpty = true;
      for (var p in cur) {
        isEmpty = false;
        recurse(cur[p], prop ? prop + "." + p : p);
      }
      if (isEmpty && prop)
        out[prop] = {};
    }
  }
  recurse(data, "");
  return out;
};

let info = '';
let warning = '';
let error = '';

(() => {

  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  // /////////////////LOOK THRU MEASUREMENTS/////////////////////////
  // Loop through all of the measurements that were taken (if there were multiple measurements)
  // if there are > 2 measurements, then add the measurement number to the end of the csvExport name (median_650.0, median_650.1... etc.).  Otherwrise, just report the name (median_650)

  let numProtocols = 1;
  if (typeof result.sample === 'object') numProtocols = result.sample.length;
  app.csvExport('protocols', numProtocols); // save the total number of protocols (makes it easier if you're calculating calibrations or other stuff and you need to know it)

  for (let j = 0; j < numProtocols; j++) {
    // if (numProtocols > 2) {
    //   // only show which measurement this is if there is more than two, otherwise it's an annoying info box!
    //   ui.info('Results of measurement ' + (j + 1) + ' of ' + numProtocols, '')
    // }

    // /////////////////PRINT MESSAGES SO FAR, SO THEY ARE ON THE TOP/////////////////////////

    if (Object.keys(result.error).length !== 0) {
      Object.keys(result.error).forEach((msg) => {
        const msgDetails = result.error[msg];
        ui.error(`${msg}`, `${msgDetails}`);
      });
    }
    if (Object.keys(result.warning).length !== 0) {
      Object.keys(result.warning).forEach((msg) => {
        const msgDetails = result.warning[msg];
        ui.warning(`${msg}`, `${msgDetails}`);
      });
    }
    if (Object.keys(result.info).length !== 0) {
      Object.keys(result.info).forEach((msg) => {
        const msgDetails = result.info[msg];
        ui.info(`${msg}`, `${msgDetails}`);
      });
    }

    // /////////////////PRINT WHAT YOU FIND, OBJECT OR NOT/////////////////////////
    if (typeof result !== 'object') { // it's not an object, so just print it
      //    console.log('its not an object, so just print it');
      ui.success(`Success!`, 'No Errors')
      ui.info('Details', `<details><summary>Click to view details</summary>
    <p>
    ${JSON.stringify(result)}      
    </p>
    </details>
      <p>`); //
      app.csvExport('custom_result', JSON.stringify(result));
    } else if (typeof result.sample === 'undefined' || typeof result.sample[0] !== 'object') { // if it's an object, print it's parts using flatten
      ui.success(`Success!`, 'No Errors')
      ui.info('Details', `<details><summary>Click to view details (JSON)</summary>
    <p>
    ${JSON.stringify(result)}      
    </p>
    </details>
      <p>`); //
      //    console.log('its an object, but not a measurement - save dot separated values from result');
      const flatResult = Object.flatten(result);
      //    ui.info('whole thing', `${JSON.stringify(flatResult)}`);
      Object.keys(flatResult).forEach((item) => {
        //      ui.info(`${item}`, `${flatResult[item]}`);
        app.csvExport(item, flatResult[item]);
      });
    }

    // /////////////////EXPORT ERRORS/WARNINGS/INFO/////////////////////////
    if (Object.keys(result.error).length !== 0) app.csvExport('error', result.error);
    if (Object.keys(result.warning).length !== 0) app.csvExport('warning', result.warning);
    if (Object.keys(result.info).length !== 0) app.csvExport('info', result.info);
  }

  app.save();
})();
