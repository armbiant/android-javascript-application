/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/// ////////////// required for surveystack ///////////////////////
// update references to submission, survey, and params

import app from './lib/app'
import serial from './lib/serial'
import * as utils from './lib/utils'

/// /////////////////// FIRMWARE UPDATING SPECIFIC CODE /////////////////////////
let fw = ''
// THIS IS BREAKING ABILITY TO COMPILE
// ON DESKTOP IT'S (APPARENTLY) APP.ISANDROID = TRUE
// it shouldn't be hitting the require statement, that's why I think it's failing.
if (app.isAndroid) {
  console.log('I think this is an android')
  fw = require('./reflectance-spec-firmware.ino.hex')
} else {
  console.log('I think this not an android')
  const fs = require('fs')
  fw = fs.readFileSync('./src/reflectance-spec-firmware.ino.hex', 'utf-8').toString()
}

fw = fw.trim()
/// /////////////////////////////////////////////////////////////////////////////

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none'
  })

  const {
    submission,
    survey,
    params
  } = app.getInputs().props

  // SOME EXAMPLES USING PARAMETERS IN SURVEYSTACK
  // example 'param' data
  // { "id": "standard-6", "protocol": "calibrateBlankLocal"}
  // serial.write(JSON.stringify(protocol));
  // how to pull in 'param' data into this script
  //  if (params.device_id) device_id = checkSurveyRef(params.device_id); // find place in submission which has the device ID and get it
  //  if (params.chlorophyll) chlorophyll = true; // set true, show chlorophyll at the top

  const result = {} // where we put the results to pass to processor
  result.error = {} // where we put the results to pass to processor
  result.warning = {} // where we put the results to pass to processor
  result.info = {} // where we put the results to pass to processor

  /// //////////////////////////////// SEND FIRMWARE + CONFIRM SUCCESS /////////////////////////////////
  await utils.sleep(1000)

  // make sure there's firmware available to load
  if (!fw) {
    console.log('unable to load firmware')
  }
  console.log(`fw len: ${fw.length}`)

  // get device information and current device_firmware
  let check = {}
  let count = 0
  serial.write('print_memory+')
  try {
    check = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1
      app.progress(0.1 + (count / 617.0) * 100.0 * 0.9)
    })
  } catch (err) {
    result.error.checkVersion = 'Error checking firmware version on the device'
    console.error(`error reading json: ${err.message} ${check}`)
  }

  // check the device version, compare to new verson (stored in params)

  // version information must be in format X.X.X (major.minor.patch)
  result.device_firmware_old = check.device_firmware
  result.device_firmware_new = params.device_firmware
  let update = false // set to true if update is needed
  for (let i = 0; i < params.device_firmware.split('.').length; i++) {
    if (Number(params.device_firmware.split('.')[i]) > Number(check.device_firmware.split('.')[i])) update = true
  }

  // update device if it is needed
  if (update === true) {
    count = 0
    try {
      app.progress(10)

      const lines = fw.split('\n')

      // start flash
      // serial.write('[{}]');
      // serial.write('print_memory+');
      serial.write('1078+')

      for (; ;) {
        const r = await serial.readLine()
        console.log(r)
        if (r === 'waiting for intel hex lines') {
          break
        }
      }

      for (let i = 0; i < lines.length; i++) {
        serial.write(`${lines[i]}\n`)
        await utils.sleep(2)
        console.log(`sending ${lines[i]}`)
        app.progress(10 + (i * 80.0) / lines.length)
      }

      for (; ;) {
        const r = await serial.readLine()
        console.log(r)
        if (r.startsWith(`done, ${lines.length} hex lines`)) {
          break
        }
      }

      console.log(`:flash ${lines.length}`)
      serial.write(`:flash ${lines.length}\n`)

      await utils.sleep(5000)
      serial.write('device_info+\r')

      result.status = 'ok'
      result.info_updated = `v${result.device_firmware_old} -> v${result.device_firmware_new} - Firmware successfully updated`
      result.len = lines.length
    } catch (err) {
      result.error.fail = 'failure reading JSON'
      console.error(`error reading json: ${err}`)
    }
    /// //////////////////////////////////////////////////////////////////////////////////
  } else {
    result.status = 'ok'
    result.info.upToDate = `Device up to date at v${result.device_firmware_old} - Firmware not updated.`
  }
  console.log('result:')
  console.log(result)
  app.result(result)
})()
