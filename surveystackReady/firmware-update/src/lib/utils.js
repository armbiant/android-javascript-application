export function checkSurveyRef(thing, submission) {
  if (thing.includes('submission.')) { // if it contains submission., then get the reference
    //  console.log(location);
    // NOTE!! TO DO: this should use a local reference, otherwise we have to pase the whole submission every time...
    // ALSO! this prevents this chunk of code to be reusable in others people's surveys
    const location = thing.split(".").splice(1);
    console.log(location);
    let finalLocation = JSON.parse(JSON.stringify(submission)); // copy submission into final location
    console.log(finalLocation);
    try {
      console.log('looking for info to send to device in...');
      for (let a = 0; a < location.length; a++) {
        finalLocation = finalLocation[location[a]];
        console.log(location[a]);
        // console.log(location[a]);
        // console.log(finalLocation);
      }
    } catch (err) {
      console.error(`error finding location from which to get data to send to device`);
    }
    console.log('found it...');
    console.log(finalLocation);
    return finalLocation;
  } else { // if it doesn't contain submission. just return the original thing
    return thing;
  }
}

export function sleep(duration) {
  return new Promise(resolve =>
    setTimeout(() => {
      resolve();
    }, duration));
}

export function filter(obj, cb) {
  const len = Object.values(obj)[0].length;
  const keys = Object.keys(obj);

  const indices = [];

  for (let i = 0; i < len; i += 1) {
    const cur = {};
    keys.forEach((key) => {
      cur[key] = obj[key][i];
    });

    if (cb(cur) === true) {
      indices.push(i);
    }
  }

  const resultSet = {};
  Object.keys(obj).forEach((key) => {
    const values = obj[key].filter((_, idx) => indices.includes(idx));

    resultSet[key] = values;
  });

  return resultSet;
}

export function zip(obj) {
  const results = [];

  const len = Object.values(obj)[0].length;
  const keys = Object.keys(obj);

  for (let i = 0; i < len; i += 1) {
    const cur = {};
    keys.forEach((key) => {
      cur[key] = obj[key][i];
    });

    results.push(cur);
  }

  return results;
}

export function countDistinct(obj, field) {
  return zip(obj).reduce((prev, next) => {
    const ret = prev;

    if (!next[field]) {
      return prev;
    }

    if (typeof prev[next[field]] === 'undefined') {
      ret[next[field]] = 0;
    }

    ret[next[field]] += 1;
    return ret;
  }, {});
}

export function distinct(obj) {
  return obj.filter((v, i, a) => a.indexOf(v) === i).filter(v => v);
}

export function count(obj) {
  return Object.values(obj)[0].length;
}

export function countIfAnswered(obj, field) {
  let cnt = 0;
  obj[field].forEach((item) => {
    if (item) {
      cnt += 1;
    }
  });

  return cnt;
}

export function groupBy(obj, field) {
  return distinct(obj[field]).map(treeId => filter(obj, row => row[field] === treeId));
}
