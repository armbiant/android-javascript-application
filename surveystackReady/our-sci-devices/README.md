# REWORKED VERISON FOR SURVEYSTACK

***This is under Construction beware!***

Do not use any of the upload / auth features for the moment.

1. `mock` still works for emulating a device
2. Don't use `app.getAnswer()` or `app.isAnswered()` anymore ...
3. Instead use `submission`, `survey` and `params` which are returned by `app.getInput()`.

# Measurement Scripts: Hello World

Welcome to the _Hello World_ example for developing a first _Measurement Script_. This tutorial aims to get you started with setting up the _Our-Sci_ development environment, get to know the tools and scripts that help you develop your first readout for your sensor and later deploy your _Measurement Script_ on an Android device for running measurements on the go.

> If you are the first time here, please consider taking a look at the [more general introduction](../README.md) to the Our-Sci platform prior to engaging in the below tutorial.

**Video: Running the Hello World Script with an Arduino Uno Rev. 3**

[![Video: Running the Hello World Script with an Arduino Uno Rev. 3](https://img.youtube.com/vi/ZMMgclQcZ7M/0.jpg)](https://www.youtube.com/watch?v=ZMMgclQcZ7M)

## The Goal

After following this guide, you will be able to take measurements of soil moist for instance of the soil inside a plant pot. As a simple setup, the resistance of soil will be measurable using just a couple of wires, a 1k resistor and a 100uF capacitor.

The experiment consists of the analysis of a step response through soil and leads to an estimation its resistance.

| Measurement Setup                                             | Measurement on the Android application                                                  |
| ------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| ![Measurement Setup](../.readme-assets/measurement_setup.jpg) | ![Running the measurement on the Android application](../.readme-assets/soil_moist.gif) |

## Hard- and Software Requirements

In order to get started, there are a couple of prerequisites for soft- and hardware.

> Please have a look at the overarching guide for [Measurement Scripts](../README.md) in general before getting started. It will show the basic idea behind why and how to develop code for gathering, processing and visualizing data.

1.  **Arduino compatible device:** at the core of taking measurements, a sensor device is inevitable. For this _Hello World_ example, we recommend to utilize an Arduino compatible device for ease of development. Chosing wether or not to use a 5V or 3.3V based Arduino board will have an effect on compatibility with a BT-Serial adapter, as well as an impact on the visualization part. For this tutorial an Arduino Uno R3 (5V) has been used, however with some minor adjustments, any other Arduino compatible board will be adequate.

2.  **Components and Setup:** aside from an Arduino compatible board, a 1kOhm Resistor and 100uF capacitor are required. These can usually be found in a starter kits for the Arduino. Other than that, some jumper wires and copper wire is necessary for the setup as well. Below the detailed setup of the measurement. ![Experiment Setup](../.readme-assets/circuit.png) Naturaly some soil is also required as the subject of measurement, which in this case is probably the easiest to come by.

3.  **USB OTG Converter or BT-Serial Adapter:** As described in the hardware section of the [Measurement Scripts](../README.md) Readme, either a USB-OTG converter or BT-Serial Adapter are necessary for communicating with the Android application. Note that in case of USB, it is not guaranteed, that an OTG converter will work with your Android device. More recent Android devices however show much broader support for it. One particular benefit of using USB is being able to supply the external Sensor with power and therefore not being dependent on an additional battery unit.

4.  **Software Setup:** To use the source code for the hello world example, an installation of the _NodeJS_ (https://nodejs.org) environment is necessary. Make sure to install a recent version (**min v8.6.0**). There are a couple of predefined helper tasks available to work with Visual Studio Code (https://code.visualstudio.com/), however any code editor and terminal will also be sufficient. Apart from the setup of NodeJS and Editor, the Arduino IDE will be required for uploading the sketch found in the [./arduino-sketch](./arduino-sketch) folder.

## Directory Structure

In order to get started with the Hello World example, let's have a look at the directory structure

```bash
.
├── arduino-sketch # here is the arduino sketch for the arduino UNO
│   └── arduino-sketch.ino
├── build # directory for intermediary build files
│   ├── ... [many]
├── data
│   └── result.json # result from running the sensor
├── dist # output directory for uploading the measurement script to a device
│   ├── hello-world.zip # zip that is uploaded to the device
│   ├── manifest.json # copy of the manifest
│   ├── processor-browser-debug.js # compiled processor script for preview on the host
│   ├── processor.html # processor html for preview
│   ├── processor.js # compiled version of the processor script
│   └── sensor.js # compiled version of the sensor script
├── node_modules # nodejs dependencies
│   ├── ... [many]
├── package.json # nodejs packages
├── package-lock.json # nodejs packages
├── README.md # this readme
├── src # source code of the measurement script
│   ├── css
│   ├── lib
│   ├── manifest.json # manifest file describing the measurement script
│   ├── processor.js # processor script
│   └── sensor.js # sensor script
└── webpack # webpack files for compiling the measurement script
    ├── deploy.js
    ├── pack-processor.js
    ├── pack-project.js
    └── pack-sensor.js
```

The most important files for development are `src/sensor.js` as well as `src/processor.js` and `src/manifest.js`. It is the fundamental code that defines the _Measurement Script_.

Also note that the firmware for the Arduino UNO is available in the `arduino-sketch` directory. The same code will run on other Arduino platforms as well that are pin compatible.

The second most important directory is the `dist` folder, it contains the compiled JS files which are used for deployment on the Android device. The `hello-world.zip` packs the compiled `sensor.js`, `processor.js` and `manifest.json`.

> If you are getting started with the dev environment, really your primariy concern will be `src/sensor.js` and `src/processor.js`. As you are getting more advanced, working with `src/css/style.css` and `src/lib/*.js` will help you customize your evaluation and visualization.

## First time compilation

Before looking at the source code of the _Measurement Script_ it is advised to check if the project compiles. Therefore change inside a terminal into the directory of this readme and execute the following commands.

```bash
node --version
# make sure this outputs something equal or greater
# than v8.6.0, otherwise reinstall nodejs from
# https://nodejs.org

# download the example if not already
git clone https://gitlab.com/our-sci/measurement-scripts.git

#change into the hello world dir
cd measurement-scripts/hello-world

npm install
# this will install all js dependencies and may take a while

npm run build-project
# this command actually compiles the files and creates
# a 'dist' directory with the output sensor.js and
# processor.js
```

If above commands complete successfully, then the nodejs environment has been set up properly. If everything ran smoothly the file `dist/hello-world.zip` is generated and contains `sensor.js`, `processor.js` and `manifest.json`.

## Arduino Setup

Before working on the _Measurement Script_ a sensor device, in this case an Arduino with copper wires as soil moisture sensore are necessary. The Arduino firmware code inside [`arduino-sketch/arduino-sketch.ino`](./arduino-sketch/arduino-sketch.ino).

For setting up the hardware

1.  attach a 100uF capacitor to pin 13, connect it with some deisolated copper wire
2.  place a 1kOhm resistor betweek 5V and pin A0
3.  attach a deisolated copper wire to pin A0

![Experiment Setup](../.readme-assets/circuit.png)

If using a Bluetooth-Serial converter, wire it up to RX/TX and pay attention if it is 5V tolerant in order to not damage the converters input pins. If not 5V tolerant, use either a 3.3V level shifter or a voltage dividing circuit.

> Alternatively you could also use a 3.3V based Teensy board (https://www.pjrc.com/teensy/) or an Arduino DUE (https://store.arduino.cc/usa/arduino-due).

For testing this experiment a USB OTG adapter has been used.

## Code Editing and Compiling

Any text editor will be sufficent to work on the JS code, however lately Visual Studio Code has been making quite a compelling case in terms of its JS and NodeJS features. Having also the ability to run build and debug tasks from within the IDE, vscode adds quite some benefits to the developer experience.

### Working with any Editor and the Command Line

The most important commands used for compiling and deploying the _Measurement Script_ are

* `npm run sensor` runs the sensor script on the dev environment.
* `npm run build-project` builds the output files for the project either for upload or the android target
* `npm run run-processor-dev` start the dev server for debugging the processor (server is spawned on http://localhost:9091, if in VS Code a Debug instance of chrome launched with F5, chrome debug extension is necessary for this inside vscode)
* `npm run deploy` will build the entire project and prompt to launch the measurement script on an android device in the same network (webserver needs to be enabled in the Android app for this)

### Making use of the shortcuts in VS Code

If you are using VS Code and are opening the `hello-world` folder, use `Tasks > Run Build Task ...` to execute above npm commands from within the editor.

![Project in Visual Studio Code](../.readme-assets/vscode.png)

## Sensor and Processor Script

The two main components of a _Measurement Script_ are the _Sensor Script_ and the _Processor Script_, they are written in Javascript and run inside a Webview on an Android device. With NodeJS as basis of the scripts, countless libraries are at the disposal for development.

First, the _Sensor Script_ acquires the data from the sensor and stores it inside a file. Subsequently, the _Processor Script_ can be called any time later on and processes and visualizes the data stored in the file.

Both, the Sensor and Processor Script are run inside a Webview on the Android device. The _Sensor Script_ however has no visible user interface. However, the _Processor Script_ has full access to render any HTML or CSS to the user.

> On the Android device, the _Sensor Script_ is only ever called once per measurement, whereas the _Processor Script_ is executed every single time the user opens a measurement that has been taken before. \_Therefore the Processor Script is always stateless and acts as a consumer of data, the Sensor Script on the other hand acts as producer of data.

### Developing and debuging _Measurement Scripts_ without Android

Before deploying scripts to Android, it makes sense to get started with debugging and developing on the host system and preview what kind of plots and graphs the processor creates and also have a look at the data received from the sensor.

> In our case, the sensor (Arduino running `arduino-sketch.ino` really just reads out the voltage at pin `A0` and converts it to a value between 0 and 1023 corresponding to 0V and 5V or `Vcc`). The sampling streches out over roughly 1 second and offers 512 values.

The output from the sensor device is encoded as `JSON` for easy processing later on. It has the following structure:

```js
{
    data: [0,1,2,3,4,5 ...]
}
```

Note that while the firmware readout provides a very simple output structure, the JSON format allows for adding more parameters while still keeping the raw data in a human readable format which will be very helpful for debugging.

#### Running the _Sensor Script_ and _Processor Script_ on the host machine

Take a look at `src/sensor.js`, the first couple of lines configure the serial port.

```js
serial.port("/dev/ttyACM0"); // if using the dev environment
serial.init();
```

After connecting the Arduino, change the serial port from `/dev/ttyACM0` to whatever is used on the host System by the Arduino IDE. On Windows this could be `COMx` on Mac OS `/dev/tty.usbmodemXYZ`.

After testing the Arduino firmware with the Arduino IDE Serial Monitor by sending the letter `a`, make sure to close the Arduino Serial Monitor.

> Closing the window is really important, since leaving it open will keep the serial port busy and block the sensor script from completing successfully.

To acquire a set of data, run the sensor script by executing the following command in the local (`hello-world`) directory.

```bash
# launch  the dev server for the processor script
npm run run-processor-dev

# launch a chrome instance once compiled at http://localhost:9091
# alternatively hit F5 if you have the chrome debug extension installed
# in vs code

# fetch data, once done, the browser will automatically refresh
npm run sensor
```

The second command if successful will create a new file `data/result.json` which contains the output of the sensor. In our case (with the `arduino.sketch.ino` firmware) the values of the A/D conversion of the input pin `A0` of the Arduino.

### Working with the _Sensor Script_

Let's dive into the code of the sensor script found at `src/sensor.js`.

First a couple of libraries provided are loaded.

```js
import app from "./lib/app";
import serial from "./lib/serial";
// import { sleep } from './lib/utils';
export default serial;
```

First of all, `serial` allows to write and read from the serial connection to the sensor. It also has functionality for waiting until an entire JSON string has been received.

Second, the `app` library is the primary way to talk to he Android application. For instance, calling `app.progress(20)` will update the progress bar the user seens on the screen to 20% during the measurement.

Once all the data has been received, it is _mandatory_ to tell the Android application that we are finished and have received a result we would like to store. Calling `app.result(res)`, will not only store the contents of the variable `res` to a file, it will also at a later stage provide those contents to the _Processor Script_ for visualization.

Furthermore, once the measurement is uploaded to the cloud or exported to a CSV, the contents of `res` will always be stored as well, allowing for recalling the resulting data at any later stage.

It is in no way mandatory to directly store the output of the sensor inside `res`, it is perfectly allowed to preprocess the data collected before storing it via `app.result(...)`, yet be aware that only the contents while are passed to `app.result(...)` are saved and made available at a later stage.

> Preprocessing the data has the benefit that less storage is needed for saving results, however if your evaluation method changes on the way, having all the original results available may prove to be the saver way to go. Thus a balance needs to be found between what processing code goes into `src/sensor.js`and `src/processor.js`.

Let's move on the the main part of the script:

```js
(async () => {
  await serial.init("/dev/ttyACM0", {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: "none"
  });

  // instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  console.log("sending a");
  serial.write("a");

  try {
    const res = await serial.readJson();
    app.progress(100);

    console.log(`res ${JSON.stringify(res)}`);
    app.result(res);
  } catch (err) {
    console.error(`error reading json: ${err}`);
  }
})();
```

The above code is wrapped inside what is in JS called an `async function`. While this may be daunting at first, the construct really helps the code to behave one would expect when programming in simple sequential manner.

At first, for running the code in the development environment, it has proven to be necessary to wait a couple of seconds until the serial connection is fully established, thus `await utils.sleep(2000)` halts the program for 2000[ms].

For actually stopping the code at this point, using the await keyword is really important. If `await`is left out, the code would not actually wait until 2 seconds have passed.

Next, `serial.write('a');` sends the character `a` to the Arduino in order to kick off a measurement.

Moving forward we are reading out the data and since it is in a JSON format we have the option of using either `await serial.readJson()` or `await serial.readStreamingJson()`, the former waits until an entire JSON structure has been received, the later also allows for intercepting elements inside the JSON by specifying a callback for a certain path that is triggered once a value is received. This is especially useful when waiting for many elements over a long period of time, in order to inform the user about the ongoing progress. It also allows for early dismissal of data.

> The `await serial.resdStreamingJson()` function utilizes the [`oboe.js` (http://oboejs.com)](http://oboejs.com) library, have a look at their documentation in order to gain deeper knowledge about capabilities.

In the example, `app.progress()` publishes the current progress of the measurement to the Android application. This way the user has an indication of how far along the measurement is.

> For debugging purposes, the function `console.log()` may be used at any time. Inside the Android application, press on the console icon in the application bar to access the debug output during the measurement.

Once done with all data acquisition and preprocessing, `app.result()` will immediately save the measurement and move to the processor script.

### Working with the _Processor Script_

As a result of running the _Sensor Script_, there are two common tasks left to do. First, visualize the measurement to the user and second process and export the result for later evaluation.

After a measurement has been saved via `app.result()`, the processor script at `src/processor.js` will have full access to the values stored. Since the processor script has control over the user interface via DOM inside the webview of the Android application, it has full freedom to draw and render any kind of HTML and CSS.

Note that every single time the user looks at the measurement result inside the Android app, the _Processor Script_ is executed again, therefore it should always remain _stateless_ and only base calculations and display on final values inside the _result_ acquired by the sensor.

For the purpose of keeping things simple, a basic library is provided to ease the creation of the first line graphs, donut charts and message boxes.

> Both _Processor Script_ and _Sensor Script_ can be run on the host machine from the development environment to ease debugging. After running the _Sensor Script_, use `run-processor-dev` to create a valid processor script, start the development webserver and open ['http://localhost:9091'](./dist/processor.html) to test the visualization.

> ![../.readme-assets/processor_browser.png](../.readme-assets/processor_browser.png)

Let's take a look at the script [`./src/processor.js`](./src/processor.js).

```js
import { sprintf } from "sprintf-js";
import app from "./lib/app";
import * as ui from "./lib/ui";
```

First a couple of libraries and dependencies for ui and math are included. Note that you can include css files as well in order to style HTML.

```js
const result = (() => {
  if (typeof processor === "undefined") {
    return require("../data/result.json");
  }

  return JSON.parse(processor.getResult());
})();
```

Above code is relevant to let the user mock some data in order to develop the script. `processor` is only defined on an Android target device, thus running this on the development web server locally will use the data found in `../data/result.json` as basis for the processing.

Remember to run the _Sensor Script_ first before running the processor to create some data beforehand.

If on the other hand this script is executed on an Android device, the real result will be used and interpreted as JSON instead of a mock variant.

```js
const len = result.data.length;

const voltage = [];
for (let i = 0; i < len; i += 1) {
  const v = 5 * (result.data[i] / 1024.0); // convert to [V] unit
  voltage.push(v);
}

// plot first 50 measured voltage levels
const data = {
  series: [voltage]
};

ui.plot(data, "ADC Voltage in [V] (entire)", { min: 0, max: 5 });
ui.plot(data, "ADC Voltage in [V] (4.5 - 5)", { min: 4.5, max: 5 });
ui.plot(data, "ADC Voltage in [V] (lower peak)", { min: 0, max: 3.3 });
```

The ADC values from the sensor range from 0V to 5V (in case of a 3.3V based sensor such as the Teensy 3.2 or Teensy LC, to 3.3V). Therefore the values are first scaled to represent actual voltage.

`ui.plot(data, "ADC Voltage in [V]")` creates a line graph with the voltage as input, the library used for this is [chartist.js](https://gionkunz.github.io/chartist-js/).

```js
let avg = 0;
// calculate average
for (let i = 0; i < len; i += 1) {
  avg += voltage[i] / len;
}

// calculate rms
let rms = 0;
for (let i = 0; i < len; i += 1) {
  const tmp = voltage[i] - avg;
  const sqr = tmp * tmp;
  rms += sqr * (1 / len);
}

ui.donut(
  "RMS AC in percent",
  sprintf("%.1f", 100 * (rms / 0.95)),
  rms / 0.95,
  ""
);

rms = Math.sqrt(rms);

ui.donut("Average in [V]", sprintf("%.3f", avg), avg / 5, "");
```

For evaluation we are going to look at the average value as well as the RMS value of the AC contents of the signal (also known as variance in statistic terms). It will be an indication of how well the soil conducts.

After calculation, a donut chart will be drawn with a label and a percentage.

```js
app.csvExport("avg", avg);
app.csvExport("rms AC", rms);

ui.info("raw rms value calculated", rms);
ui.info("average calculated", "average has been calculated exported");
ui.info("rms calculated", "rms has been calculated exported");
ui.warning(
  "rms out of range",
  sprintf("rms %.3f seems to be out of range", rms)
);
ui.error("avg looks weird", sprintf("avg %.3f is clearly funky", avg));

app.save();
```

After calculation and visualization, the AVG and RMS value is exported in CSV format for later evaluation, this happens with the call `app.csvExport(key, value)`.

> Note that this feature is used inside _Surveys_, therefore a Survey that establishes a reference to the script is required to fully use the feature, more on this further down the road.

To display a simple message, the `ui.info()` `ui.warning()` and `ui.error()` functions are used. They create a message box for the user.

Last but not least, a call to `app.save()` needs to be made in order to finalize the exported CSV values.

> On the Android device, only the first time the _Processor Script_ runs on a single measurement, the values are actually exported with `app.save()`.


## Deploying the script

In order to deploy the script on [https://app.our-sci.net](https://app.our-sci.net) you can either manually upload the output inside the dist directory on the web app or use

```
# log in with your our-sci account
npm run auth

# upload / deploy script
npm run upload
```

## Referencing Script in ODK-Build

To reference the script in ODK-Build, add a `media` question of type `video` (we don't support video uploads for now) and then in the `hint` field add your instruction for the user. Afterwards follow the instruction with a semicolon and the `id` of the measurement supplied in the `./src/manifest.json`

![../.readme-assets/howto-odk.png](../.readme-assets/howto-odk.png)
