/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable semi */

import app from './lib/app';
import serial from './lib/serial';
// import wavelengthProcessor from './lib/process-wavelengths';
import sendDevice from './lib/sendDevice';
// import * as MathMore from './lib/math';
// import sleep from './lib/utils'; // use: await sleep(1000);
import * as utils from './lib/utils';

export default serial; // expose this to android for calling onDataAvailable

(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none'
  });

  // NEXT STEPS

  // review passed...
  // ///////////////////////////////////////////////////////
  // NAMING CONVENTIONS FOR Reflectometer VERSIONING
  // We are using the x1.x2.x3 versioning system, where:
  // x3 = immediate patch (data from device will be backwards compatible)
  // x2 = minor version (data from device will be backwards compatible, but there are feature improvements, UI changes, new outputs etc.)
  // x1 = major version (data from device will not be backwards compatible... LED intensity change, detector response change, changed calculation of important output, built for new device, etc.)
  // when collecting data or running experiments, it's important to not switch major versions if you want to ensure comparable data.

  // ///////////////////////////////////////////////////////
  // OPTIONAL instead of reading from the serial port mock some data
  // serial.feed('./mock/mock_sensor_output.json');

  //    "protocol": "submission.data.teflon_cal.value.toDevice"

  const { submission, survey, params, parent } = app.getInputs().props;

  //  console.log('submission', JSON.stringify(submission));

  let result = {}; // where we put the results to pass to processor
  result.error = {}; // where we put the results to pass to processor

  // think about passing information
  // now we have 'pass'
  // which works for leaf, sample_type, and blank...

  let device_id = null;
  let leaf = 0;
  let sample_type = null;
  let blank = null;
  //  let card_qr = null;

  if (params.device_id)
    { device_id = utils.get(params.device_id, submission, parent); } // find place in submission which has the device ID and get it
  if (params.leaf) leaf = 1; // set true, show leaf info at the top
  // for sample_type, you can enter a reference or a direct value (like polyphenols, antioxidants, or lowry_proteins)
  if (params.sample_type)
    { sample_type = utils.get(params.sample_type, submission, parent); } // find place in submission which has the sample type and get it
  if (params.blank) blank = utils.get(params.blank, submission, parent); // find place in submission which has the sample type and get it
  //  if (params.card_qr)  card_qr = submission.card_qr.value;

  // example 'param'
  // { "id": "standard-6", "protocol": "calibrateBlankLocal"}
  // { "id": "standard-6", "protocol": "standard", "sample_type": "submission.data.sample_type.value"}
  // { "id": "standard-6", "protocol": "[{}]"}
  // { "id": "standard-6", "protocol": "submission.data.some_calibration_question.value"}

  /*
  Basic Logic...
  - if you do not define param.protocol, then you send standard
  - if you define param.protocol, you send that protocol (like calibrateTeflon)
  - if you define param.protocol w/ a survey reference, you send that reference
  - if you define param.protocol w/ anything else, you send whatever you entered.
  - if you get an object back (json), then run everything as normal
  - if you get not an object back, then just print the results in custom_result
  */

  // List of all protocols to run
  //  console.log(Object.keys(sendDevice()));

  /// ///////////////////////// SET PROTOCOL //////////////////////////////////
  let protocol;
  if (typeof params.protocol === 'undefined') {
    // make sure this protocol exists, else run default
    console.log(
      `Params not defined. Running default protocol ${params.protocol}`
    );
    protocol = sendDevice().standard;
  } else if (Object.keys(sendDevice()).includes(params.protocol)) {
    // run if on list of allowable protocols
    console.log(`Running ${params.protocol}`);
    protocol = sendDevice()[params.protocol];
  } else if (params.protocol.includes('submission.') || params.protocol.includes('parent.')) {
    // if this is referencing a submission object... let's find it and save what it's referencing
    console.log(`Found ${params.protocol}`);
    protocol = utils.get(params.protocol, submission, parent);
    console.log(`Running ${protocol}`);
  } else {
    // otherwise, just run whatever is inside the protocol param (user defined in the 'param' spot in SurveyStack)!
    console.log(`Params set custom protocol. Running ${params.protocol}`);
    protocol = params.protocol;
  }

  // If it's a protocol to set device ID, then send the protocol to the device.
  if (device_id && protocol === sendDevice().set_device_info) {
    console.log(`${device_id}+`);
    protocol += `${device_id}+`;
  }

  // console.log(device_id);
  // console.log(protocol);
  // console.log(sendDevice().configure_bluetooth);
  // console.log(sendDevice().configure_bluetooth_115200);
  // If setting bluetooth, then add the device ID as hex to the bluetooth name
  if (
    device_id &&
    (protocol === sendDevice().configure_bluetooth ||
      protocol === sendDevice().configure_bluetooth_115200)
  ) {
    const parsed = parseInt(device_id, 10);
    const hex = parsed.toString(16);
    protocol = protocol.replace('NAME', `${hex}`);
    console.log(protocol);
  }

  /// //////////////////////////////// SEND PROTOCOL TO DEVICE /////////////////////////////////
  if (typeof protocol === 'object') {
    // if the protocol is a valid JSON object
    // if you want to adjust on the fly based on local environmental variables, add if statements here!
    console.log(JSON.stringify(protocol));
    serial.write(JSON.stringify(protocol));
  } else {
    // otherwise, just send whatever it is
    console.log(protocol);
    serial.write(protocol);
  }

  /// //////////////////////////////// GET RESULTS BACK FROM DEVICE /////////////////////////////////
  let count = 0;
  try {
    app.progress(0.1);
    result = await serial.readStreamingJson('data_raw[*]', () => {
      count += 1;
      app.progress(0.1 + (count / 617.0) * 100.0 * 0.9);
    });

    app.progress(100);

    if (result.error) {
      // if there is an error (like battery too low) tell the user
      app.result(result);
      return;
    }
    // if this is setting the device ID, then also put in result the non-hex version of the ID
    if (device_id) {
      result.device_id_number = `${device_id}`;
    }
    if (typeof protocol === 'object') {
      // ///////////////////////////////////////////////////////
      // if this is just a single measurement, then define meas as the measurement.  If there are multiple, note that they also need to be pulled out here.
      for (let j = 0; j < result.sample.length; j++) {
        // if there are any recalled userdef's, then always pass them on
        const meas = result.sample[j];
        if (meas.recall) {
          //          console.log(`saved ${JSON.stringify(meas.recall)}`);
          app.csvExport('recall', JSON.stringify(meas.recall));
        }

        meas.passed = {};
        // // default values to zero, save over if they exist (otherwise we're dealing with typeof 'undefined' all the time which is annoying)
        meas.passed.leaf = '';
        meas.passed.card_qr = '';
        meas.passed.blank = '';
        // // Go get any information from previous questions and pass it along for the processor script to use.
        // // May also save things using environment variable (env.get, env.set)
        if (typeof meas.calibration === 'undefined') {
          // default calibration to zero.
          meas.calibration = 0;
        } // if you can't find it, set it to zero
        if (leaf) {
          meas.passed.leaf = leaf;
        }
        if (sample_type) {
          meas.passed.sample_type = sample_type;
          //          console.log('this is lab test ' + meas.passed.sample_type);
        }
        if (blank && meas.calibration === 0) {
          // only keep the median values, which will get subtracted in processor
          Object.keys(blank).forEach((item) => {
            if (!item.includes('median_')) delete blank[item];
          });
          //            meas.passed.blank = JSON.parse(app.getEnv('blank'));
          meas.passed.blank = blank;
          // console.log(
          //   'this measurement will use a blank saved in the local environment variables.  The saved blank is... '
          // );
          // console.log(JSON.stringify(meas.passed.blank));
        }
      }
    }
  } catch (err) {
    result.error.readingResult = `error reading result.  I sent: ${protocol} and got ${JSON.stringify(
      result
    )}.  Error: ${err.message}.`;
    console.error(`error reading json: ${err.message} ${result}`);
  }
  app.result(result);
})();
