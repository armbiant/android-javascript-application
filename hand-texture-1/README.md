# Measurement Script Template

This repository contains the boilerplate for creating a new measurement script

It is intended to be used with [`degit`](https://github.com/Rich-Harris/degit) to scaffold new scripts.

## Usage

```bash
# Install degit
npm install -g degit
# use degit to scaffold measurement script boilerplate
degit https://gitlab.com/our-sci/measurement-script-template my-new-script
# Change directory to project folder
cd my-new-script
# install dependencies
yarn install # or `npm install`
# open project in your editor of choice, we support VS Code
code .
```

Then go mess with `sensor.js` and `processor.js` and run using the VS Code GUI by pressing `Ctrl + Shift + B` and selecting the build task or using the terminal as follows:

```bash
# Run sensor script initially to generate /data/results.json
yarn sensor # or npm run sensor
# Run processor dev server
yarn run-processor-dev # or npm run run-processor-dev
```

Once the processor dev server is running you can access it by navigating to [`http://localhost:9091`](http://localhost:9091/) in your browser.

Here's a list of all the possible actions you can run on the measurement script, using `yarn {action}` where `{action}` is one of the following entries.

- `run-processor-dev`: Start the processor script in a development server.
- `build-processor-browser`: Build the processor script bundled for the browser.
- `build-processor`: Build the processor script bundled for Android
- `build-sensor`: Build the sensor script
- `sensor`: Run the sensor script
- `run-sensor-build-processor`: Run the sensor script then build the processor script for the browser
- `build-project`: Build the measurement script project: build processor and sensor script and add to zip archive
- `deploy`: Deploy built measurement script to Android device
- `build-and-deploy`: Build the measurement script project then deploy it to an Android device
- `upload`: Upload the built measurement script bundle to OurSci platform
- `auth`: Authenticate with OurSci platform
- `compile-upload`: Build the sensor and processor scripts, then upload bundle to OurSci platform

## Writing a sensor script

- Use `app.getAnswer('field_id')` to retreive a response from the survey by field ID, where `field_id` is a unique string identifier for a survey field.
- Call `appendResult(title, content)` to add an entry to the results array
- Call `app.progress(progressVal)` to set the script progress in the app, where progressVal is an integer from 0 to 100
- `app.getMeta()` can be used to reference the survey's structure (see docs for more info)

## Writing a processor script

...

## Using with OurSci VSCode Extension

The OurSci VSCode extension lets you download sample data from existing OurSci survey results and reference them in the measurement script dev environment

- Install OurSci VSCode extension
- `Ctrl` + `Shift` + `P` and type "oursci", hit `Enter`
- Select the survey results you'd like to download, hit `Enter`
- The selected survey results will be downloaded to the `.oursci-surveys` folder as `survey.csv` along with helper script (`survey.js`) for accessing these results via a measurement script
- In your measurement script, you can now reference the existing results as follows

```javascript
import surveyHelper from "../.oursci-surveys/survey";
export const survey = surveyHelper();
```

#TODO: finish this... how does this help you autocomplete survey results?

## Using Survey Metadata with `extractOptions`

- In ODKBuild select `File` > `Save Form to File`
- Rename/move form file to `mock/odkbuild.json`

```bash
cd mock
node extractOptions.js > meta.json
```

#TODO: finish this... show example of consuming meta.json
