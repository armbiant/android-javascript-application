/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
// import sendDevice from './lib/sendDevice';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  // Create a space to store values created here (warnings, etc.) in the result.json.
  const meas = result.sample[0];
  const processed = meas.processed;
  const warnings = [];
  const info = [];
  // Get the co2 sample values in an array from the result.json
  const co2 = result.sample[0].co2;

  // ///////////////////////////////////////////////////////
  // Clean up data
  // Convert the values from strings (which they currently are) to numbers so we can do stuff with them
  for (let i = 0; i < co2.length; i++) {
    co2[i] = Number(co2[i]);
  }
  // Clean up the data a bit, sometimes there are outliers due to the sensor noise, so let's replace them with null values
  // deal with spikes on the first or last measurement, and deal with 3 spikes in a row, and deal with drops to 0 or below...
  let countWarnings = 0;
  for (let i = 0; i < co2.length; i++) {
    if (i === 0) { // for the first co2 measurement...
      // if it's higher than 10,000 or less than or equal to zero, it's out of range and we should set it to whatever the previous value was.
      // keep doing this in case there was several spikes in a row
      if (co2[i] > 10000 || co2[i] <= 0) {
        co2[i] = co2[i + 1];
        countWarnings++;
      }
      if (co2[i] > 10000 || co2[i] <= 0) {
        co2[i] = co2[i + 2];
        countWarnings++;
      }
      if (co2[i] > 10000 || co2[i] <= 0) {
        co2[i] = co2[i + 3];
        countWarnings++;
      }
    } else if (i === co2.length - 1) { // for the last co2 measurement
      if (co2[i] > 10000 || co2[i] <= 0) {
        co2[i] = co2[i - 1];
        countWarnings++;
      }
    } else if (i > 0 && i < co2.length - 1) {
      if (co2[i] > 10000 || co2[i] <= 0) {
        co2[i] = co2[i - 1];
        countWarnings++;
      }
      // now let's handle unexpected spikes which may still happen in the expected range (0 - 10,000)
      // it's hard to tell spikes if they are the first or last measurement, so let's exclude those
      const co2_before = Math.abs(co2[i] - co2[i - 1]);
      const co2_after = Math.abs(co2[i] - co2[i + 1]);
      //      ui.info("difs", co2_after + "," + co2_before);
      if (co2_before > 5000 && co2_after > 5000) {
        // if this point is a spike up or down, then set it to null
        co2[i] = (co2[i + 1] + co2[i - 1]) / 2;
        const warningText = 'CO2 spike smoothed';
        ui.warning(
          warningText,
          'An individual spike in the CO2 sensor was removed and replaced with an average of the co2 measurements directly before and after it.  If you are receiving many of these notifications, you may have an issue with you experimental setup, sample, or instrument',
        );
        warnings.push(warningText);
      }
    }
  }
  //  ui.warning(`count = ${countWarnings}`)
  if (countWarnings > 4) {
    ui.error(
      'Too many spikes',
      'This sample has 5 or more CO2 measurements out of range.  You should re-run this sample'
    )
  }
  for (let i = 0; i < countWarnings; i++) {
    const warningText = 'Value > 10,000 or < 0 removed';
    ui.warning(
      warningText,
      'A single CO2 value was greater than 10000 or less than 0 and was set to the previous CO2 value',
    );
    warnings.push(warningText);
  }
  console.log(co2);

  // add 7g soil to 1ml water
  /*
  //  /* 60ml Syringe setup 2.5g only
  // ///////////////////////////////////////////////////////
  // Define the experimental parameters for the CO2 method
  const sample_time = 5; // time between samples in seconds
  const Headspace = 58;
  const Temp = 298;
  const Rconstant = 82.05;
  const conversionfactor = 2500; // conversion factor is assuming 2.5g soil (soil g x 1000)
  const conversionfactor2 = 12000; //  converts to weight of C only and not of CO2
  // ///////////////////////////////////////////////////////
  //  */

  /* Pint Jar setup 25g only
  // ///////////////////////////////////////////////////////
  // Define the experimental parameters for the CO2 method
  const sample_time = 5; // time between samples in seconds
  const Headspace = 470;
  const Temp = 298;
  const Rconstant = 82.05;
  const conversionfactor = 25000; // conversion factor is assuming 25g soil (soil g x 1000)
  const conversionfactor2 = 12000; //  converts to weight of C only and not of CO2
  // ///////////////////////////////////////////////////////
   */

  const co2max = MathMore.MathMAX(co2);
  const co2min = MathMore.MathMIN(co2);
  // const co2increase = co2max - co2min;
  // const ugCgsoil = MathMore.MathROUND((((co2increase * Headspace) / conversionfactor) / (Temp * Rconstant)) * conversionfactor2);

  if (co2max >= 10000) {
    // if the device is working correctly, it'll max out at exactly 10,000.  If there is a noisy spike that needs to be removed, it's often > 10,000
    const warningText = 'CO2 too high';
    ui.warning(
      warningText,
      'CO2 reading on the device maxed sensors range at 10,000 ppm.  Check the experimental setup for other sources of CO2, or if the soil is extremely active, consider changing the protocol to account for high CO2 activity.',
    );
    warnings.push(warningText);
    // ui.info("CO2 increase", null);
    // ui.info("ugC per g soil", null);
    // app.csvExport("co2_increase", null);
    // ui.csvExport("ugc_gsoil", null);
  } else {
    ui.info('Max CO2', co2max);
    ui.info('Min CO2', co2min);
    // ui.info("CO2 increase", co2increase);
    // ui.info("ugC per g soil", ugCgsoil);
    app.csvExport('max_co2', co2max);
    app.csvExport('min_co2', co2min);
    // app.csvExport("co2_increase", co2increase);
    // app.csvExport("ugc_gsoil", ugCgsoil);
  }

  // ///////////////////////////////////////////////////////
  // Print all errors and info staetments's up to this point and save them to the database
  if (typeof warnings[0] !== 'undefined') {
    for (let i = 0; i < warnings.length; i++) {
      app.csvExport('warnings', warnings.toString());
    }
  }
  if (typeof info[0] !== 'undefined') {
    for (let i = 0; i < info.length; i++) {
      app.csvExport('info', info.toString());
    }
  }
  // ///////////////////////////////////////////////////////

  ui.plot({
      series: [co2],
    },
    'CO2 over time', {
      min: co2min,
      max: co2max,
    },
  );
  app.save();
})();

/*
"id": "carbon-mineralization",
"name": "Carbon Mineralization via CO2 emission from soils",
"description": "This uses a single K-30 CO2 sensor (0 - 10,000ppm) to measure the CO2 accumulated by moistened soil after a 24 hours incubation",
"version": "1.0",
"action": "Run Measurement (takes 2 min)",
"requireDevice": "true"
*/

/*
"id": "carbon-mineralization-25",
"name": "Carbon Mineralization via CO2 emission from soils, using pint jar",
"description": "This uses a single K-30 CO2 sensor (0 - 10,000ppm) to measure the CO2 accumulated by moistened soil after a 24 hours incubation",
"version": "1.0",
"action": "Run Measurement (takes 2 min)",
"requireDevice": "true"
*/