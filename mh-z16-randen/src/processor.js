/* global processor */

import { sprintf } from 'sprintf-js';

import regression from 'regression';
import app from './lib/app';
import * as ui from './lib/ui';

const highResInterval = 2; // 2 measurements per second
const maxMeasurementDuration = 300; // 60s measurement

const Chartist = require('chartist');

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

let label = '';
if (result.sample_id) {
  label = `${result.sample_id}: `;
}

const co2lres = result.low_res.map(v => v[0]);

const tvoc = result.high_res.map(v => v[2]);
const eco2 = result.high_res.map(v => v[3]);
const hum = result.high_res.map(v => v[4]);
const press = result.high_res.map(v => v[5]);
const co2vals = result.high_res.map(v => v[0]);

const time = co2vals.map((v, i) => i * 2);
console.log(time);

const co2 = {
  series: [co2vals],
};

const startIdx = result.started_index;
console.log(`start index is: ${result.started_index}`);

const co2avg = co2vals.reduce((a, b) => a + b, 0) / co2vals.length;
const co2min = Math.min(...co2vals);
const co2max = Math.max(...co2vals);

const temp = {
  series: [result.high_res.map(v => v[1])],
};

/*
const lres = {
  series: [result.low_res],
};
*/

const totalDuration = co2vals.length * highResInterval;

if (startIdx != null && startIdx >= 0 && startIdx < co2vals.length) {
  let endIdx = startIdx + maxMeasurementDuration / highResInterval;
  console.log(endIdx);
  if (endIdx > co2vals.length) {
    endIdx = co2vals.length;
  }

  const vals = co2vals.slice(startIdx, endIdx);

  // const fit = regression.exponential(vals);

  /*
  const fitted = [...vals.keys()].map(idx => {
    Math.exp();
  });
  */

  console.log(endIdx - startIdx);
  const duration = (endIdx - startIdx) * highResInterval;

  const valsCo2min = Math.min(...vals);
  const valsCo2max = Math.max(...vals);

  ui.plotxy(
    time.slice(startIdx, endIdx),
    vals,
    `${label}Zoomed CO2 concentration in [ppm] over ${duration} [s]`,
    {
      min: valsCo2min * 0.9,
      max: valsCo2max * 1.1,
      axisX: {
        type: Chartist.FixedScaleAxis,
        divisor: 5,
        labelInterpolationFnc(v) {
          const t = v - time[startIdx];
          const mins = Math.floor(t / 60);
          const seconds = Math.floor(t) % 60;

          return `${mins} min ${seconds} s`;
        },
      },
    },
  );

  ui.plotxy(
    time.slice(startIdx, endIdx),
    vals,
    `${label}CO2 concentration in [ppm] over ${duration} [s]`,
    {
      min: 0,
      max: 3000,
      axisX: {
        type: Chartist.FixedScaleAxis,
        divisor: 5,
        labelInterpolationFnc(v) {
          const t = v - time[startIdx];
          const mins = Math.floor(t / 60);
          const seconds = Math.floor(t) % 60;

          return `${mins} min ${seconds} s`;
        },
      },
    },
  );
}

ui.plotxy(time, co2vals, `${label}Zoomed CO2 concentration in [ppm] over ${totalDuration} [s]`, {
  min: co2min * 0.9,
  max: co2max * 1.1,
  axisX: {
    type: Chartist.FixedScaleAxis,
    divisor: 5,
    labelInterpolationFnc(v) {
      return `${(v / 60).toFixed(0)} min`;
    },
  },
});

ui.plotxy(time, co2vals, `${label}CO2 concentration in [ppm] over ${totalDuration} [s]`, {
  min: 0,
  max: 4000,
  axisX: {
    type: Chartist.FixedScaleAxis,
    divisor: 5,
    labelInterpolationFnc(v) {
      return `${(v / 60).toFixed(0)} min`;
    },
  },
});

ui.plot({ series: [co2lres] }, `${label}CO2 concentration in [ppm] over ${totalDuration} [s]`, {
  min: 0,
  max: 4000,
});

ui.plot({ series: [eco2] }, `${label}ECO2 5 min`, { min: 0, max: 50000 });

ui.plotxy(time, result.high_res.map(v => v[1]), `emp in [°C] over ${totalDuration} [s]`, {
  min: -10,
  max: 50,
  axisX: {
    type: Chartist.FixedScaleAxis,
    divisor: 5,
    labelInterpolationFnc(v) {
      return `${(v / 60).toFixed(0)} min`;
    },
  },
});

ui.info(sprintf('%.0f', co2avg), 'CO2 avg [ppm]');
ui.info(sprintf('%.0f', Math.max(...co2vals)), 'CO2 max [ppm]');
// ui.plot(lres, 'CO2 concentration in ppm, Low Resolution over 60h', { min: 0, max: 5000 });

ui.plot({ series: [hum] }, 'Air humidity in %', { min: 0, max: 100 });
ui.plot({ series: [press] }, 'Air pressure in [kPa]', { min: 30000, max: 110000 });

app.save();
