/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// ///////////////////////////////////////////////////////
// Import libraries and get the 'result' information from sensor.js
import app from './lib/app';
import * as ui from './lib/ui';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Device info, but if there are several measurements, just print this once (not every time)
  ui.info('Device Info', 'Name: PNX-203');
  ui.info('Weight', result.weight);
  if (isNaN(result.weight)) {
    ui.error('Result is not a number', 're-weigh or check the scale for other problems');
  } else {
    app.csvExport('weight_grams', result.weight); // there are some extra empty bits and the 'g' symbol
  }
})();

// result.slice(0, 5)
// final save of all app.csvExport calls -- make sure this is always at the very end!
app.save();