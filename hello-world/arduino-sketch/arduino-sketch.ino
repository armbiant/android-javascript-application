#define PIN_IN 0
#define PIN_OUT 13
#define LEN 512

uint16_t buffer[LEN] = { 0 };

void setup() {
  Serial.begin(115200);
  pinMode(PIN_IN, INPUT);
  pinMode(PIN_OUT, OUTPUT);
  digitalWrite(PIN_OUT, HIGH);
}


void loop() {
  uint32_t i = 0;

  for (;;) {
    if (Serial.available()) {
      if (Serial.read() == 'a') {
        break;
      }
    }
  }

  for (i = 0; i < LEN; i++) {
    if (i == 64) {
      digitalWrite(PIN_OUT, LOW);
    }
    buffer[i] = analogRead(PIN_IN);
    delay(2);
  }


  printBuffer();
  digitalWrite(PIN_OUT, HIGH);
}

void printBuffer() {

  Serial.print("{\"data\":[");
  uint16_t i = 0;
  for (i = 0; i < LEN; i++) {
    Serial.print(buffer[i]);
    if (i < LEN - 1) {
      Serial.print(",");
    }
  }

  Serial.println("]}\r\n");
}
