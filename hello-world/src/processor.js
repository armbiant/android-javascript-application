/* global processor */

import { sprintf } from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';

const result = (() => {
  if (typeof processor === 'undefined') {
    return require('../data/result.json');
  }

  return JSON.parse(processor.getResult());
})();

const len = result.data.length;

const voltage = [];
for (let i = 0; i < len; i += 1) {
  const v = 5 * (result.data[i] / 1024.0); // convert to [V] unit
  voltage.push(v);
}

// plot first 50 measured voltage levels
const data = {
  series: [voltage],
};

ui.plot(data, 'ADC Voltage in [V] (entire)', { min: 0, max: 5 });
ui.plot(data, 'ADC Voltage in [V] (4.5 - 5)', { min: 4.5, max: 5 });
ui.plot(data, 'ADC Voltage in [V] (lower peak)', { min: 0, max: 3.3 });

let avg = 0;
// calculate average
for (let i = 0; i < len; i += 1) {
  avg += voltage[i] / len;
}

// calculate rms
let rms = 0;
for (let i = 0; i < len; i += 1) {
  const tmp = voltage[i] - avg;
  const sqr = tmp * tmp;
  rms += sqr * (1 / len);
}

ui.donut('RMS AC in percent', sprintf('%.1f', 100 * (rms / 0.95)), rms / 0.95, '');

rms = Math.sqrt(rms);

ui.donut('Average in [V]', sprintf('%.3f', avg), avg / 5, '');

app.csvExport('avg', avg);
app.csvExport('rms AC', rms);

ui.info('raw rms value calculated', rms);
ui.info('average calculated', 'average has been calculated exported');
ui.info('rms calculated', 'rms has been calculated exported');
ui.warning('rms out of range', sprintf('rms %.3f seems to be out of range', rms));
ui.error('avg looks weird', sprintf('avg %.3f is clearly funky', avg));

app.save();
