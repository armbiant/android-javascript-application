/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */
/* eslint-disable no-param-reassign */

import { sprintf } from 'sprintf-js';
import _ from 'lodash';

import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Something was not right', result.error);
    return;
  }
  // Calculates moisture content;
  const moisture_content =
    (result.pre_weight - result.post_weight) / (result.pre_weight - result.cup_wt);
  if (moisture_content < 0 || moisture_content > 1) {
    ui.error(
      'The moisture content was out of range',
      'Please check that the entered weights are correct, otherwise we will set the correction factor to 1',
    );
  }

  if (moisture_content < 0.6 && moisture_content > 0) {
    ui.warning(
      'The moisture content was very low',
      'If the vegetable was very dry this may be fine, otherwise please check that the entered weights are correct',
    );
  }

  ui.info('Moisture Content %', MathMore.MathROUND(moisture_content, 2));
  app.csvExport('Moisture Content %', MathMore.MathROUND(moisture_content, 2));

  // Define parameters to calculate polyphenol, antioxidant and protein;
  const sp_norm = 0.914; // Fresh weight for spinach, based on USDA nutritional database;
  const car_norm = 0.8829; // Fresh weight for carrot, based on USDA nutritional database;
  const sp_ext = 8; // ml of methanol used to extract spinach;
  const car_ext = 6; // ml of methanol used to extract carrot;
  const poly_conv = 0.1; // needed to convert ug ml-1 to mg 100 g FW for polyphenol;
  const sp_naoh = 50; // ml of NaOH used to dissolve protien in spinach;
  const car_naoh = 40; // ml of NaOH used to dissolve protein in carrot;
  const pro_conv = 0.001; // needed to convert protein into mg protein per 100 g FW;

  // Calculate final Polyphenol, Antioxidant and Protein Content;
  if (result.sample_type === 'spinach') {
    let CF = sp_norm / moisture_content;
    if (moisture_content < 0 || moisture_content > 1) {
      CF = 1;
    }
    ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
    app.csvExport('Correction Factor', MathMore.MathROUND(CF, 2));

    const polyphenols =
      ((result.polyphenols.data.polyphenols_estimate * sp_ext) / result.sample_weight) *
      poly_conv *
      CF;
    ui.info('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));
    app.csvExport('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));

    const antioxidants =
      ((result.antioxidants.data.antioxidants_estimate * sp_ext) / result.sample_weight) * CF;
    ui.info('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));
    app.csvExport('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));

    const protein =
      ((result.protein.data.protein_estimate * sp_naoh) / result.sample_weight) * pro_conv * CF;
    ui.info('Total Protein mg per 100g FW', MathMore.MathROUND(protein, 2));
    app.csvExport('Total Protein mg per 100g FW', MathMore.MathROUND(protein, 2));
  } else if (result.sample_type === 'carrot') {
    let CF = car_norm / moisture_content;
    if (moisture_content < 0 || moisture_content > 1) {
      CF = 1;
    }
    ui.info('Correction Factor', MathMore.MathROUND(CF, 2));
    app.csvExport('Correction Factor', MathMore.MathROUND(CF, 2));

    const polyphenols =
      ((result.polyphenols.data.polyphenols_estimate * car_ext) / result.sample_weight) *
      poly_conv *
      CF;
    ui.info('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));
    app.csvExport('Total Polyphenols mg GAE 100g FW', MathMore.MathROUND(polyphenols, 2));

    const antioxidants =
      ((result.antioxidants.data.antioxidants_estimate * car_ext) / result.sample_weight) * CF;
    ui.info('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));
    app.csvExport('Total Antioxidants FRAP value', MathMore.MathROUND(antioxidants, 2));

    const protein =
      ((result.protein.data.protein_estimate * car_naoh) / result.sample_weight) * pro_conv * CF;
    ui.info('Total Protein mg per 100g FW', MathMore.MathROUND(protein, 2));
    app.csvExport('Total Protein mg per 100g FW', MathMore.MathROUND(protein, 2));
  }
  app.save();
})();
