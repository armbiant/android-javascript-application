import { app, formparser, serial } from '@oursci/scripts';
// import * as coeffs from './coefficients';
import * as defs from '../.oursci-surveys/survey';
import { slopes, soils } from './consts';

export default serial; // expose this to android for calling onDataAvailable

// https://app.our-sci.net/#/survey/by-form-id/build_Soil-C-Prediction-and-Recommendation_1568117894
// if in dev environment, take this survey result as example
const uuidSnippet = 'uuid:43c03d10-6fc2-49aa-a181-84926f7e8544';
const form = 'milk-butterfat-measurement-v1';

if (app.initSubmittedMock && uuidSnippet && form) {
  app.initSubmittedMock(form, uuidSnippet);
  console.log(`fetched survey result for uuid: ${uuidSnippet}`);
}

const result = {};
result.log = [];
result.error = [];
const survey = defs.survey();
// hello!
// export const survey = surveyHelper();
export function err(msg) {
  result.error.push(msg);
}

function appendResult(title, content) {
  result.log.push({
    title,
    content,
  });
}

function errorExit(error) {
  app.error();
  throw Error(error);
}

(async () => {
  try {
    // Use `app.getAnswer('field_id')` to retreive a response from the survey by field ID
    // Call `appendResult(title, content)` to add an entry to the results array
    // Call `app.progress(progressVal)` to set the script progress in the app, where progressVal is an integer from 0 to 100
    // `app.getMeta()` can be used to reference the survey's structure (see docs for more info)

    // show spad 632 of scan 1

    // https://gitlab.com/our-sci/resources/blob/master/resources/soil_classes.csv

    // Coefficients for linear regression model

    const linear_intercept = 5.85380745902957;

    const coeffs_lin = {
      median_365: -2.52411541151822,
      median_385: 7.88628222934729,
      median_450: -3.54559734431692,
      median_500: -18.9478963819458,
      median_530: -0.671494938406223,
      median_587: -9.81438484216093,
      median_632: 28.3999144939635,
      median_850: -0.549169560876632,
      median_880: -2.16572090411036,
    };

    // Coefficients for over/under 3.5% butterfat content logistic regression model
    const logistic_intercept_avg = -68.6356128168811;

    const coeffs_log_avg = {
      median_365: 25.2609920126743,
      median_385: -54.2401268558451,
      median_450: 14.272832331143,
      median_500: 91.7098946368803,
      median_530: -32.0711863755257,
      median_587: 172.160371168547,
      median_632: -193.126094932171,
      median_850: 64.1407187252659,
      median_880: -30.0768862664323,
    };
    // Coefficients for over/under 4.2% butterfat contentlogistic regression model
    const logistic_intercept_high = 56.2556882630161;

    const coeffs_log_high = {
      median_365: -17.5721002360321,
      median_385: 57.1338502736648,
      median_450: 37.9453244102928,
      median_500: -164.5030941462,
      median_530: -147.032912575595,
      median_587: 74.9553289636456,
      median_632: 170.267859859355,
      median_850: -28.0364294393697,
      median_880: -37.674166398403,
    };

    const getMeasurementAnswers = (id) => {
      try {
        const a = app.getAnswer(id);
        if (a === '' || a === undefined || a === null || a === {}) {
          return null;
        }
        return JSON.parse(app.getAnswer(id)).data;
      } catch (exception) {
        console.log(exception);
        return null;
      }
    };
    console.log(JSON.stringify(survey, null, 2));

    const milk_scan = getMeasurementAnswers('milk_scan');

    const nm365 = Math.sqrt(milk_scan['median_365'] / milk_scan['median_940']);
    console.log('365 nm: ', nm365);
    result.nm365 = nm365;

    const nm385 = Math.sqrt(milk_scan['median_385'] / milk_scan['median_940']);
    console.log('385 nm: ', nm385);
    result.nm385 = nm385;

    const nm450 = Math.sqrt(milk_scan['median_450'] / milk_scan['median_940']);
    console.log('450 nm: ', nm450);
    result.nm450 = nm450;

    const nm500 = Math.sqrt(milk_scan['median_500'] / milk_scan['median_940']);
    console.log('500 nm: ', nm500);
    result.nm500 = nm500;

    const nm530 = Math.sqrt(milk_scan['median_530'] / milk_scan['median_940']);
    console.log('530 nm: ', nm530);
    result.nm530 = nm530;

    const nm587 = Math.sqrt(milk_scan['median_587'] / milk_scan['median_940']);
    console.log('587 nm: ', nm587);
    result.nm587 = nm587;

    const nm632 = Math.sqrt(milk_scan['median_632'] / milk_scan['median_940']);
    console.log('632 nm: ', nm632);
    result.nm632 = nm632;

    const nm850 = Math.sqrt(milk_scan['median_850'] / milk_scan['median_940']);
    console.log('850 nm: ', nm850);
    result.nm850 = nm850;

    const nm880 = Math.sqrt(milk_scan['median_880'] / milk_scan['median_940']);
    console.log('880 nm: ', nm880);
    result.nm880 = nm880;

    // Run the model to determine if butterfat content is over or under 3.5%

    result.logistic_intercept_avg = logistic_intercept_avg;

    const sum_coeff_log_avg =
      coeffs_log_avg.median_365 * nm365 +
      coeffs_log_avg.median_385 * nm385 +
      coeffs_log_avg.median_450 * nm450 +
      coeffs_log_avg.median_500 * nm500 +
      coeffs_log_avg.median_530 * nm530 +
      coeffs_log_avg.median_587 * nm587 +
      coeffs_log_avg.median_632 * nm632 +
      coeffs_log_avg.median_850 * nm850 +
      coeffs_log_avg.median_880 * nm880 +
      logistic_intercept_avg;

    console.log('Sum of Coefficients [avg]: ', sum_coeff_log_avg);

    const prob_avg =
      Math.exp(sum_coeff_log_avg) / (1 + Math.exp(sum_coeff_log_avg));

    console.log(Math.round(prob_avg, 2));
    result.prob_avg = prob_avg;

    // Run the model to determine if butterfat content is over or under 4.2%

    result.logistic_intercept_high = logistic_intercept_high;

    const sum_coeff_log_high =
      coeffs_log_high.median_365 * nm365 +
      coeffs_log_high.median_385 * nm385 +
      coeffs_log_high.median_450 * nm450 +
      coeffs_log_high.median_500 * nm500 +
      coeffs_log_high.median_530 * nm530 +
      coeffs_log_high.median_587 * nm587 +
      coeffs_log_high.median_632 * nm632 +
      coeffs_log_high.median_850 * nm850 +
      coeffs_log_high.median_880 * nm880 +
      logistic_intercept_high;

    console.log('Sum of Coefficients [high]: ', sum_coeff_log_high);

    const prob_high =
      Math.exp(sum_coeff_log_high) / (1 + Math.exp(sum_coeff_log_high));

    console.log(Math.round(prob_high, 2));
    result.prob_high = prob_high;

    // Run the model to predict estimated butterfat content range

    result.linear_intercept = linear_intercept;

    const butterfat_content =
      coeffs_lin.median_365 * nm365 +
      coeffs_lin.median_385 * nm385 +
      coeffs_lin.median_450 * nm450 +
      coeffs_lin.median_500 * nm500 +
      coeffs_lin.median_530 * nm530 +
      coeffs_lin.median_587 * nm587 +
      coeffs_lin.median_632 * nm632 +
      coeffs_lin.median_850 * nm850 +
      coeffs_lin.median_880 * nm880 +
      linear_intercept;

    console.log('Estimated butterfat content: ', butterfat_content);
    const min_butterfat = butterfat_content - 0.5;
    const max_butterfat = butterfat_content + 0.5;

    result.butterfat_content = butterfat_content;
    result.min_butterfat = min_butterfat;
    result.max_butterfat = max_butterfat;

    app.result(result);
  } catch (error) {
    console.error(error);
    result.error = [error.message];
    app.result(result);
  }
})();
