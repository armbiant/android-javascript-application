/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// ///////////////////////////////////////////////////////
// Import libraries and get the 'result' information from sensor.js
import sprintf from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Device info, but if there are several measurements, just print this once (not every time)
  ui.info('sent', result.toDevice);
  //  ui.info('memory', JSON.parse(result.memory));
  ui.info(
    'Device Info',
    ' ID: ' +
    result.memory.device_id);
  /* + // change this when all device firmware is updated to 1.24
  '<br>Name: ' +
  result.memory.device_name +
  ', Version: ' +
  result.memory.device_version +
  ', Firmware: ' +
  result.memory.device_firmware);
  */
  app.csvExport('device_id', result.memory.device_id);
  if (typeof result.memory.device_battery !== 'undefined') {
    app.csvExport('device_battery', result.memory.device_battery);
    app.csvExport('device_version', result.memory.device_version);
    app.csvExport('device_firmware', result.memory.device_firmware);
  }
  // ///////////////////////////////////////////////////////

  // reset the local environment 'toDevice' to empty... this should be up in sensor.js ideally
  app.setEnv('toDevice', '', 'data to send to device');
  ui.info('Local variable reset', 'toDevice has been reset to empty.  Rerun measurements that produced the results if needed.  Confirmation that the contents of toDevice are empty: ' + app.getEnv('toDevice'));

  // ///////////////////////////////////////////////////////
  // Print results from saving the data, do some sanity checks and let the user know what's going on.
  if (!result.toDevice) {
    ui.warning('No values found', 'No values were found to be saved.  Confirm that you have recently run a measurement which produces values to save to the device (called \'toDevice\' usually).  Check the \'variables\' screen from the main menu, you should find toDevice with the information to send to the device.  If not, rerun the measurement');
  } else {
    ui.warning('Values saved - confirm save below', 'Below are what you sent to the device to save, and the contents of the devices memory.  Please confirm that the values you intended to save appear in the the right place');
    ui.info('You sent -->', result.toDevice);
    ui.info('You received -->', '');
    for (let i = 0; i < Object.keys(result.memory).length; i++) {
      const key = Object.keys(result.memory)[i];
      ui.info(key, result.memory[key]);
    }
  }
  if (!result.memory) {
    ui.warning('Device did not respond', 'We asked for the information stored in the device memory, but it did not provide it.  This may be due to the fact that we sent it messed up information, it is stopped for some other reason, or its the wrong type of device.');
  }
})();
// final save of all app.csvExport calls -- make sure this is always at the very end!  }
app.save();