/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

import app from './lib/app';
import serial from './lib/serial';

export default serial; // expose this to android for calling onDataAvailable

// ///////////////////////////////////////////////////////
// Make sure you have the right connection settings for device (used in development mode only)
(async () => {
  await serial.init('/dev/ttyACM0', {
    //  await serial.init('/dev/rfcomm0', {
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: 'none',
  });

  // ///////////////////////////////////////////////////////
  // Get any text to send to device from the environment variable, and send it
  const toDevice = app.getEnv('toDevice');
  //  const toDevice = 'set_user_defined+1+0.12121+2+-0.02994244826+3+-0.009556416858+4+-0.03041425713+5+-0.0284995999+6+-0.13904124003+7+-0.05330357141+8+-0.032539952424+9+-0.043224032846+10+-0.02815642855+11+-0.000021254999993+12+0.000024742799848+13+0.00002766875+14+0.00013199999992+15+0.00014182999996+16+0.0003625000002+17+0.00010306799993+18+-0.00001616000007+19+0.0000190999999+20+0.00008244552987+-1+'; // for testing
  // Mark the progress of pulling in the data
  if (toDevice) {
    serial.write(toDevice);
    console.log(toDevice);
    // Then ask device for memory to sanity check
    serial.write('print_memory+');
    // Mark the progress of pulling in the data
    let count = 0;
    try {
      app.progress(0.1);
      const memory = await serial.readStreamingJson('data_raw[*]', () => {
        count += 1;
        app.progress(0.1 + (count / 617.0) * 100.0 * 0.9);
      });
      app.progress(100);
      // Now, pass the devices response to 'print_memory' over to processor, to display to user.
      const result = {};
      result.memory = memory;
      result.toDevice = toDevice;
      console.log(result);
      app.result(result);
      // Error check in case it went wrong.
    } catch (err) {
      console.error(`error reading json: ${err}`);
    }
  } else {
    app.result('');
  }
})();