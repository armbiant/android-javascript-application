/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

import {
  sprintf
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();
  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }

  //  /*

  // change # objects to 12, save and test... does it work?
  // then double check everything make dependent on # objects in JSON

  // HEAT CALIBRATION ONLY
  //  /*
  // ///////////////////////////////////////////////////////
  // create fit data for the temperature and median response, display the results, save them to CSV, and prepare them to be saved back to the device
  ui.warning(
    'Heat Calibration Information',
    'In order to account for the effects of heat on the light intensity of the device, a correction is identified by correlating temperature and intensity, using a 4 th order polynomial fit.Below are the coefficients for that fit for each light.  Note that when saved to the device, the 2nd and 3rd values are multiplied by 100000 while the 4th and 5th values are multiplied by 100000000 to reduce the decimal places so saving to the device as a float does not loose precision.',
  );

  const regData = [];
  const regFit = [];
  const tempData = [];
  const reflectanceData = [];
  //  const wavelengths = result.sample[0].processed.wavelengths; // we use this a lot below, so declare here to make things less ugly.
  const wavelengths = [370, 395, 420, 530, 605, 650, 730, 850, 880, 940];
  const normalTemp = 25; // normalize the results so this temp in C is == 1;
  let toDevice_heatcal1 = 'set_heatcal1+';
  let toDevice_heatcal2 = 'set_heatcal2+';
  //  const toDevice_heatcal3 = 'set_heatcal3+';
  //  const toDevice_heatcal4 = 'set_heatcal4+';
  //  const toDevice_heatcal5 = 'set_heatcal5+';
  // loop through all of the wavelengths to run regressions, display, and save to csv
  for (let z = 0; z < wavelengths.length; z++) {
    regData.push([]);
    tempData.push([]);
    reflectanceData.push([]);
    // loop through the number of measurements collected to put into the heat correction model
    for (let i = 1; i < 13; i++) {
      const meas = result['measurement_1'].data;
      const meas = result.measurement_1.data;
      const median = meas['median_' + wavelengths[z]];
      //      console.log(median);
      //      console.log(meas['temperature']);
      regData[z].push([Number(meas['temperature']), Number(median)]);
      tempData[z].push(Number(meas['temperature']));
      reflectanceData[z].push(Number(median));
    }
    // run the model (linear regression)

    /*
        regression = {
          m,
          b,
          r,
          r2: r * r
        };
    */
    ui.info('Data', 'temperature: ' + tempData[z] + '<br>reflectance: ' + reflectanceData[z]);
    const regResults = MathMore.MathLINREG(tempData[z], reflectanceData[z]);
    // created normalized outputs so results at 25C == 1
    const slopesNormalized = [];
    const resultAtTwentyFiveC =
      //      regResults.slopes[4] * normalTemp ** 4 +
      //      regResults.slopes[3] * normalTemp ** 3 +
      //      regResults.slopes[2] * normalTemp ** 2 +
      regResults.m * normalTemp + regResults.b;
    //    ui.info('25c', resultAtTwentyFiveC);
    //    for (let i = 0; i < regResults.m.length; i++) {
    slopesNormalized[0] = regResults.b / resultAtTwentyFiveC;
    slopesNormalized[1] = regResults.m / resultAtTwentyFiveC;
    //    }
    // sanity check --> this should always be == 1
    //    const normalizedAtTwentyFiveC = slopesNormalized[4] * normalTemp ** 4 + slopesNormalized[3] * normalTemp ** 3 + slopesNormalized[2] * normalTemp ** 2 + slopesNormalized[1] * normalTemp + slopesNormalized[0];
    // save results to csv and display
    regFit.push(regResults.r2);
    app.csvExport('heatcal_raw_' + wavelengths[z], regResults.m);
    app.csvExport(
      'heatcal_norm_' + wavelengths[z],
      slopesNormalized[0] + ',' + slopesNormalized[1],
    );
    app.csvExport('heatcal_r2_' + wavelengths[z], regFit[z]);
    // Error check if r2 is too low
    let poorFit = 0;
    if (regFit[z] < 0.9) {
      ui.warning(
        'Poor fit! r2 = ' + MathMore.MathROUND(regFit[z], 3) + ' at ' + wavelengths[z] + 'nm',
        'Evaluate data and consider repeating the calibration',
      );
      poorFit += 1;
    }
    ui.info(
      'Heat Calibration at ' + wavelengths[z],
      'r2: ' +
      regFit[z] +
      '<br>' +
      'y intercept: ' +
      slopesNormalized[0] +
      '<br>' +
      'slope' +
      slopesNormalized[1] +
      '<br>' +
      'y intercept raw: ' +
      regResults.b +
      '<br>' +
      'slope raw' +
      regResults.m,
      //    '<br>' +
      //    slopesNormalized[2]
      //      '<br>' +
      //      slopesNormalized[3] +
      //      '<br>' +
      //      slopesNormalized[4],
    );
    // Note the number of poor fits.
    app.csvExport('poorFit', poorFit);
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_heatcal1 += z + 1 + '+' + slopesNormalized[0] + '+';
    toDevice_heatcal2 += z + 1 + '+' + slopesNormalized[1] + '+';
    //  toDevice_heatcal3 += z + 1 + '+' + slopesNormalized[2] * 1000 + '+';
    //    toDevice_heatcal4 += z + 1 + '+' + slopesNormalized[3] * 1000000 + '+';
    //    toDevice_heatcal5 += z + 1 + '+' + slopesNormalized[4] * 1000000000 + '+';
    // output individual plots for each wavelength
    ui.plotxy(tempData[z], reflectanceData[z], 'Fit for ' + wavelengths[z]);
  }
  // assemble the plots so they show all in one graph
  ui.multixy(
    tempData,
    reflectanceData,
    'Fits for Wavelengths',
    wavelengths.map(v => 'Fit for ' + v),
  );
  // close out the text to be sent to device, export to CSV (important, also gets picked up by next measurement script), and display for santify check
  toDevice_heatcal1 += '-1+';
  toDevice_heatcal2 += '-1+';
  app.csvExport('toDevice_heatcal1', toDevice_heatcal1);
  app.csvExport('toDevice_heatcal2', toDevice_heatcal2);
  //  toDevice_heatcal3 += '-1+';
  //  toDevice_heatcal4 += '-1+';
  //  toDevice_heatcal5 += '-1+';
  ui.warning(
    'Note on saved values',
    'Heat calibration values can be very small (e-10)... this is not feasible to send or save to eeprom due to limitations to c++ data types and serial communication.  So some values have been multiplied before saving to device.  If you are externally using these values, note that the saved values to the device are larger than the actual model coefficients.  Specifically, heatcal5 is multiplied by 1,000,000,000, heatcal4 is multiplied by 1,000,000, and heatcal3 is multiplied by 1,000 .  heatcal2 and heatcal1 are saved without modification.',
  );
  ui.info('Save to Device, coefficient 1', toDevice_heatcal1);
  ui.info('Save to Device, coefficient 2', toDevice_heatcal2);
  //  ui.info('Save to Device coefficient 3', toDevice_heatcal3);
  //  ui.info('Save to Device coefficient 4', toDevice_heatcal4);
  //  ui.info('Save to Device coefficient 5', toDevice_heatcal5);
})();
// make sure this is at the very end!
app.save();

/*
    // run the model
    const regResults = MathMore.MathPOLYREG(regData[z], 1);
    // created normalized outputs so results at 25C == 1
    const slopesNormalized = [];
    const resultAtTwentyFiveC =
      //      regResults.slopes[4] * normalTemp ** 4 +
      //      regResults.slopes[3] * normalTemp ** 3 +
      //      regResults.slopes[2] * normalTemp ** 2 +
      regResults.slopes[1] * normalTemp +
      regResults.slopes[0];
    for (let i = 0; i < regResults.slopes.length; i++) {
      slopesNormalized[i] = regResults.slopes[i] / resultAtTwentyFiveC;
    }
    // sanity check --> this should always be == 1
    //    const normalizedAtTwentyFiveC = slopesNormalized[4] * normalTemp ** 4 + slopesNormalized[3] * normalTemp ** 3 + slopesNormalized[2] * normalTemp ** 2 + slopesNormalized[1] * normalTemp + slopesNormalized[0];
    // save results to csv and display
    regFit.push(regResults.rsquared);
    app.csvExport('heatcal_raw_' + wavelengths[z], regResults.slopes);
    app.csvExport('heatcal_norm_' + wavelengths[z], slopesNormalized);
    app.csvExport('heatcal_r2_' + wavelengths[z], regFit[z]);
    // Error check if r2 is too low

    let poorFit = 0;
    if (regFit[z] < 0.9) {
      ui.warning(
        'Poor fit! r2 = ' + MathMore.MathROUND(regFit[z], 3) + ' at ' + wavelengths[z] + 'nm',
        'Evaluate data and consider repeating the calibration',
      );
      poorFit += 1;
    }
    app.csvExport('poorFit', poorFit);
    ui.info(
      'Heat Calibration at ' + wavelengths[z],
      'r2: ' +
      regFit[z] +
      '<br>' +
      slopesNormalized[0] +
      '<br>' +
      slopesNormalized[1]
      //    '<br>' +
      //    slopesNormalized[2]
      //      '<br>' +
      //      slopesNormalized[3] +
      //      '<br>' +
      //      slopesNormalized[4],
    );
    // prepare results to save to device, to be assembled and pushed in another measurement script.
    toDevice_heatcal1 += z + 1 + '+' + slopesNormalized[0] + '+';
    toDevice_heatcal2 += z + 1 + '+' + slopesNormalized[1] + '+';
    //  toDevice_heatcal3 += z + 1 + '+' + slopesNormalized[2] * 1000 + '+';
    //    toDevice_heatcal4 += z + 1 + '+' + slopesNormalized[3] * 1000000 + '+';
    //    toDevice_heatcal5 += z + 1 + '+' + slopesNormalized[4] * 1000000000 + '+';
    // output individual plots for each wavelength
    ui.plotxy(tempData[z], reflectanceData[z], 'Fit for ' + wavelengths[z]);
  }
  // assemble the plots so they show all in one graph
  ui.multixy(
    tempData,
    reflectanceData,
    'Fits for Wavelengths',
    wavelengths.map(v => 'Fit for ' + v),
  );
  // close out the text to be sent to device, and display for santify check
  toDevice_heatcal1 += '-1+';
  toDevice_heatcal2 += '-1+';
  //  toDevice_heatcal3 += '-1+';
  //  toDevice_heatcal4 += '-1+';
  //  toDevice_heatcal5 += '-1+';
  ui.warning('Note on saved values', 'Heat calibration values can be very small (e-10)... this is not feasible to send or save to eeprom due to limitations to c++ data types and serial communication.  So some values have been multiplied before saving to device.  If you are externally using these values, note that the saved values to the device are larger than the actual model coefficients.  Specifically, heatcal5 is multiplied by 1,000,000,000, heatcal4 is multiplied by 1,000,000, and heatcal3 is multiplied by 1,000 .  heatcal2 and heatcal1 are saved without modification.');
  ui.info('Save to Device, coefficient 1', toDevice_heatcal1);
  ui.info('Save to Device, coefficient 2', toDevice_heatcal2);
  //  ui.info('Save to Device coefficient 3', toDevice_heatcal3);
  //  ui.info('Save to Device coefficient 4', toDevice_heatcal4);
  //  ui.info('Save to Device coefficient 5', toDevice_heatcal5);
*/