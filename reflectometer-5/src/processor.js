/* eslint-disable no-inner-declarations */
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
/* eslint-disable max-len */
/* eslint-disable prefer-template */
/* eslint-disable brace-style */
/* eslint-disable eol-last */
/* eslint-disable prefer-destructuring */
/* eslint-disable object-curly-newline */
/* eslint-disable function-paren-newline */
/* eslint-disable dot-notation */
/* eslint-disable indent */

/* global processor */

// ///////////////////////////////////////////////////////
// Import libraries and get the 'result' information from sensor.js
import {
  sprintf,
} from 'sprintf-js';
import app from './lib/app';
import * as ui from './lib/ui';
import * as MathMore from './lib/math';
import wavelengthProcessor from './lib/process-wavelengths';

const Chartist = require('chartist');

(() => {
  const result = (() => {
    if (typeof processor === 'undefined') {
      return require('../data/result.json');
    }
    return JSON.parse(processor.getResult());
  })();

  if (result.error) {
    ui.error('Error', result.error);
    app.save();
    return;
  }
  // ///////////////////////////////////////////////////////

  // ///////////////////////////////////////////////////////
  // Device info, but if there are several measurements, just print this once (not every time)

  // Color code the battery info, so the user can plan appropriately and battery status is obvious.
  let device_color = '<battery-ok>';
  let device_color_close = '</battery-ok> OK';
  if (result.device_battery <= 15) {
    device_color = '<battery-low>';
    device_color_close = '</battery-low> Battery low, charge soon!';
  } else if (result.device_battery > 15 && result.device_battery <= 30) {
    device_color = '<battery-med>';
    device_color_close = '</battery-med> OK but consider charging';
  }
  ui.info(
    'Device Info',
    device_color +
    'Battery: ' +
    result.device_battery + '%' +
    device_color_close +
    '<br>Device ID: ' +
    result.device_id +
    '<br>Name: ' +
    result.device_name +
    ', Version: ' +
    result.device_version +
    ', Firmware: ' +
    result.device_firmware,
  );

  if (typeof result.device_battery !== 'undefined') {
    app.csvExport('device_battery', result.device_battery);
  }
  app.csvExport('device_id', result.device_id);
  app.csvExport('device_version', result.device_version);
  app.csvExport('device_firmware', result.device_firmware);

  // Create variable to store anything you will want to save to the device from this measurement (usually used for calibrations)
  let toDevice = '';

  // If the result is a simple print statement (either not an object, or it's a print_memory result), just print it here and skip the rest.  Otherwise, do a normal measurement.
  if (result.isTextInput === 1) {
    ui.info('Raw Result', JSON.stringify(result));
  } else {
    // ///////////////////////////////////////////////////////
    // Loop through all of the measurements that were taken (if there were multiple measurements)
    // and calculate the median values for each wavelength
    // if there are > 2 measurements, then add the measurement number to the end of the csvExport name (median_650.0, median_650.1... etc.).  Otherwrise, just report the name (median_650)

    const numProtocols = result.sample.length;
    app.csvExport('protocols', numProtocols); // save the total number of protocols (makes it easier if you're calculating calibrations or other stuff and you need to know it)

    for (let j = 0; j < numProtocols; j++) {
      if (numProtocols > 2) {
        // only show which measurement this is if there is more than two, otherwise it's an annoying info box!
        ui.info('Results of measurement ' + (j + 1) + ' of ' + numProtocols, '');
      }

      // for the sake of simplicty, let's rename some common variables we're going to use for each loop...
      const meas = result.sample[j]; // full response from device for this protocol j
      let processed = {}; // the raw device response processed into median, stdev, etc., populated below by process-wavelengths.js
      const passed = meas.passed; // information passed from sensor.js (local environment variables, questions, etc.)
      const calibration = meas.calibration; // infomation passed from device about calibration status of this measurement.
      let voltage = null;
      if (meas.voltage_raw) {
        voltage = meas.voltage_raw; // only show if voltage is outputted by device, used to check heat calibration
      }
      const data = meas.data_raw; // the detector response only from the device
      if (typeof data[0] !== 'undefined') { // if there is data_raw to be processed and shown, show it... otherwise skip to the end)
        // Determine what the wavelengths for each pulse set are and add it to the result.
        let wavelengths = [];
        let detectors = [];
        if (
          calibration === 1 || // white user calibration
          calibration === 2 || // black user calibration
          calibration === 3 || // blank calibration saving to EEPROM memory on device
          calibration === 4 || // white master calibration
          calibration === 5 // black master calibration
        ) {
          wavelengths = [
            850,
            880,
            365,
            385,
            450,
            530,
            587,
            632,
            500,
            940,
            850,
            880,
            365,
            385,
            450,
            530,
            587,
            632,
            500,
            940,
          ];
          detectors = [
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '0_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
            '1_',
          ]; //          wavelengths = [365, 365, 365, 365, 385, 385, 385, 385, 450, 450, 450, 450, 500, 500, 500, 500, 530, 530, 530, 530, 587, 587, 587, 587, 632, 632, 632, 632, 850, 850, 850, 850, 880, 880, 880, 880, 940, 940, 940, 940];
        } else if (calibration === 7) {
          wavelengths = [365, 365, 365, 365, 385, 385, 385, 385, 450, 450, 450, 450, 500, 500, 500, 500, 530, 530, 530, 530, 587, 587, 587, 587, 632, 632, 632, 632, 850, 850, 850, 850, 880, 880, 880, 880, 940, 940, 940, 940];
          detectors = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
        } else {
          // includes if it's undefined
          wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
          detectors = ['', '', '', '', '', '', '', '', '', ''];
        }

        // ///////////////////////////////////////////////////////
        // Process the results - clean up outliers, straighten due to heating, and produce medians, etc. for each wavelength and see if it's a calibration script
        processed = wavelengthProcessor(meas, wavelengths, detectors);

        // ///////////////////////////////////////////////////////
        // If it's a calibration script, figure out which and save data back to device accordingly
        // Assemble the data to save to device and save back to device
        if (calibration === 1) {
          let set_ir_high = 'set_ir_high+';
          for (let i = 0; i < 10; i++) {
            set_ir_high += i + 1 + '+';
            set_ir_high += processed.median[i] + '+';
          }
          set_ir_high += -1 + '+';
          let set_vis_high = 'set_vis_high+';
          for (let i = 0; i < 10; i++) {
            set_vis_high += i + 1 + '+';
            set_vis_high += processed.median[i + 10] + '+';
          }
          set_vis_high += -1 + '+';
          console.log(set_ir_high);
          console.log(set_vis_high);
          // Send the data to the device in the next measurement
          toDevice += set_ir_high + set_vis_high;
        } else if (calibration === 2) {
          // Assemble the data to save to device and save back to device
          let set_ir_low = 'set_ir_low+';
          for (let i = 0; i < 10; i++) {
            set_ir_low += i + 1 + '+';
            set_ir_low += processed.median[i] + '+';
          }
          set_ir_low += -1 + '+';
          let set_vis_low = 'set_vis_low+';
          for (let i = 0; i < 10; i++) {
            set_vis_low += i + 1 + '+';
            set_vis_low += processed.median[i + 10] + '+';
          }
          set_vis_low += -1 + '+';
          console.log(set_ir_low);
          console.log(set_vis_low);
          // Send the data to the device
          toDevice += set_ir_low + set_vis_low;
          // Also identify the QR code which contains the card's master values, and save them to the device
          toDevice += passed.cardQr;
        } else if (calibration === 3) {
          // blank used for cuvettes
          // Assemble the data to save to device and save back to device
          let set_ir_blank = 'set_ir_blank+';
          for (let i = 0; i < 10; i++) {
            set_ir_blank += i + 1 + '+';
            set_ir_blank += processed.median[i] + '+';
          }
          set_ir_blank += -1 + '+';
          let set_vis_blank = 'set_vis_blank+';
          for (let i = 0; i < 10; i++) {
            set_vis_blank += i + 1 + '+';
            set_vis_blank += processed.median[i + 10] + '+';
          }
          set_vis_blank += -1 + '+';
          console.log(set_ir_blank);
          console.log(set_vis_blank);
          toDevice += set_ir_blank + set_vis_blank;
          // Send the data to the device
          //      serial.write(set_ir_blank);
          //      serial.write(set_vis_blank);dilution
        } else if (calibration === 4 || calibration === 5) {
          // master calibrations only for creating calibration cards
          // master for white card
          if (calibration === 4) {
            let set_ir_high_master = 'set_ir_high_master+';
            for (let i = 0; i < 10; i++) {
              set_ir_high_master += i + 1 + '+';
              set_ir_high_master += MathMore.MathROUND(processed.median[i], 3) + '+';
            }
            set_ir_high_master += -1 + '+';
            let set_vis_high_master = 'set_vis_high_master+';
            for (let i = 0; i < 10; i++) {
              set_vis_high_master += i + 1 + '+';
              set_vis_high_master += MathMore.MathROUND(processed.median[i + 10], 3) + '+';
            }
            set_vis_high_master += -1 + '+';
            console.log(set_ir_high_master);
            console.log(set_vis_high_master);
            // Save data to processed so is displayed an can be printed as QR codes
            processed.masterCard = set_ir_high_master + set_vis_high_master;
          } else if (calibration === 5) {
            // master calibration using black card (min signal)
            let set_ir_low_master = 'set_ir_low_master+';
            for (let i = 0; i < 10; i++) {
              set_ir_low_master += i + 1 + '+';
              set_ir_low_master += MathMore.MathROUND(processed.median[i], 3) + '+';
            }
            set_ir_low_master += -1 + '+';
            let set_vis_low_master = 'set_vis_low_master+';
            for (let i = 0; i < 10; i++) {
              set_vis_low_master += i + 1 + '+';
              set_vis_low_master += MathMore.MathROUND(processed.median[i + 10], 3) + '+';
            }
            set_vis_low_master += -1 + '+';
            console.log(set_ir_low_master);
            console.log(set_vis_low_master);
            // Save data to processed so is displayed an can be printed as QR codes
            processed.masterCard = set_ir_low_master + set_vis_low_master;
          }
        }
        /*
    // FOR BLACK MASTER MASTER ONLY
    // manually set device to master (0 - 100 for low - high)
    // set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+
    const set_ir_high_master =
      'set_ir_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_ir_low_master = 'set_ir_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    const set_vis_high_master =
      'set_vis_high_master+0+100+1+100+2+100+3+100+4+100+5+100+6+100+7+100+8+100+9+100+10+100+-1+';
    const set_vis_low_master = 'set_vis_low_master+0+0+1+0+2+0+3+0+4+0+5+0+6+0+7+0+8+0+9+0+10+0+-1+';
    toDevice += set_ir_high_master + set_ir_low_master + set_vis_high_master + set_vis_low_master;

//    serial.write(set_ir_high_master);
//    serial.write(set_ir_low_master);
//    serial.write(set_vis_high_master);
//    serial.write(set_vis_low_master);
        */

        console.log(JSON.stringify('Processed data: '));
        console.log(JSON.stringify(processed));
        console.log(JSON.stringify('Passed data from sensor.js: '));
        console.log(JSON.stringify(passed));
        console.log(JSON.stringify('Calibration status sensor.js: '));
        console.log(JSON.stringify(calibration));

        /*
        for (let k = 0; k < wavelengths.length; k++) {
          ui.info('sample values flat', processed.sample_values_flat[k]);
          ui.info('sample values perc', processed.sample_values_perc[k]);
          ui.info('sample values flat', processed.sample_values_flat[k]);
          ui.info('sample values raw', processed.sample_values_raw[k]);
          ui.info('sample values voltage', processed.sample_voltage_raw[k]);
          ui.info('sample values adjustments', processed.sample_values_adjustments[k]);

          ui.info('sample values perc', JSON.parse(processed.sample_values_perc[k]));
          ui.info('sample values raw', JSON.parse(processed.sample_values_raw[k]));
          ui.info('sample values voltage', JSON.parse(processed.sample_voltage_raw[k]));
          ui.info('sample values adjustments', JSON.parse(processed.sample_values_adjustments[k]));

          ui.info('sample values slope', JSON.stringify(processed.sample_values_slope[k]));
          ui.info('sample values yint', JSON.stringify(processed.sample_values_yint[k]));
          ui.info('sample values center', JSON.stringify(processed.sample_values_center[k]));
          ui.info('sample values time', JSON.stringify(processed.sample_values_time[k]));
        }
          */

        // ///////////////////////////////////////////////////////
        // Output the data, save the CSV

        // ///////////////////////////////////////////////////////
        // Error checking
        // Make sure this is the right device for this protocol.  Save errors to the warnings array to be displayed and saved later
        if (typeof result.device_name === 'undefined') {
          const warningText =
            'device_name is expected from the device, but I dont see it in the output result.  Check the protocol and ensure it is requested';
          processed.warnings.push(warningText);
        }
        if (result.device_name.toString() !== 'Reflectometer') {
          const warningText =
            'This script was intended to be used on the reflectometer!  This device is a ' +
            result.device_name.toString();
          processed.warnings.push(warningText);
        }
        if (typeof result.device_version === 'undefined') {
          const warningText =
            'device_version is expected from the device, but I dont see it in the output result.  Check the protocol and ensure it is requested';
          processed.warnings.push(warningText);
        }
        if (typeof result.device_id === 'undefined') {
          const warningText =
            'device_id is expected from the device, but I dont see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.  If using Our Sci firmware, use the set_device_info+<deviceID>+ command to assigned ID';
          processed.warnings.push(warningText);
        }
        if (typeof result.device_firmware === 'undefined') {
          const warningText =
            'device_firmware is expected from the device, but I dont see it in the output result.  This device may have been modified from its original version, or is not built by Our Sci, or something is wrong.';
          processed.warnings.push(warningText);
        }

        // Print all errors and info statements's up to this point and save them to the database
        if (typeof processed.warnings[0] !== 'undefined') {
          for (let i = 0; i < processed.warnings.length; i++) {
            ui.warning('Warning', processed.warnings[i]);
            if (numProtocols <= 2) {
              app.csvExport('warnings', processed.warnings.toString());
            } else {
              app.csvExport('warnings.' + j, processed.warnings.toString());
            }
          }
        }
        if (typeof processed.info[0] !== 'undefined') {
          for (let i = 0; i < processed.info.length; i++) {
            ui.warning('Info', processed.info[i]);
            if (numProtocols <= 2) {
              app.csvExport('info' + j, processed.info.toString());
            } else {
              app.csvExport('info.' + j, processed.info.toString());
            }
          }
        }
        // ///////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////
        // identify calibration type for this measurement (if any), and save to the database
        if (calibration === 1) {
          ui.info('this is a white card calibration', ' ');
        } else if (calibration === 2) {
          ui.info('this is a black card calibration', ' ');
        } else if (calibration === 3) {
          ui.info('this is a blank using device memory', ' ');
        } else if (calibration === 4) {
          ui.info('this is a white MASTER card calibration', ' ');
        } else if (calibration === 5) {
          ui.info('this is a black MASTER card calibration', ' ');
        } else if (calibration === 6) {
          ui.info('this is a blank using the local environment variable', ' ');
        }
        if (typeof calibration !== 'undefined') {
          app.csvExport('calibration', calibration);
        }
        // ///////////////////////////////////////////////////////

        // ///////////////////////////////////////////////////////
        // Display and save the data
        // If it's a calibration 1 or 2, then it's 0 - 65535 for the results
        // If it's a calibration 3 or 0/undefined (normal measurement), it's -20 - 120
        // If master calibration of cards, display the calibration values so they can be copied to a QR code and printed on the card.
        // If it's a calibation 6 (blank), then save results as 'blank' in the local environment variables

        // If it's a calibration 0 and it sees populated variable named 'blank' among the local environment variables, then subtract the blank from the signal, let the user know that you are applying the blank, and then clear the blank (it's one time use only).
        //      ui.info(passed.blank);
        //      ui.info(calibration);
        // passed.blank = JSON.parse(passed.blank);
        if (passed.blank !== '' && calibration === 0) {
          for (let i = 0; i < processed.median.length; i++) {
            //        ui.info('median pre-blank', processed.median[i]); // debug
            processed.median[i] -= passed.blank.median[i];
            //        ui.info('median post-blank', processed.median[i]); // debug
          }
          if (j === 0) {
            // if its the first protocol, let the user know that the blank was found and applied.
            ui.info(
              'Blank found and applied',
              'A blank was found in the local environment variables and it was applied to this measurement.  The blank has now been deleted from memory, so if you measure again you will need to re-blank.',
            );
          } else if (j === numProtocols - 1) {
            // if its the last protocol, then wipe 'blank' clean in the local environment variable
            app.setEnv(
              'blank',
              '',
              'Blank, and is applied to the next standard measurement.  Reset after each measurement when it is applied.',
            );
            ui.info('Blank saved to Env as... ', app.getEnv('blank'));
          }
        }

        if (calibration === 4 || calibration === 5) {
          ui.info('Calibration Card Values', processed.masterCard);
          app.csvExport('calibration_card_values', processed.masterCard);
        }
        // Save the median and stdev from each wavelength
        for (let i = 0; i < processed.median.length; i++) {
          if (numProtocols <= 2) {
            app.csvExport(`median_${processed.detectors[i]}` + processed.wavelengths[i], processed.median[i]);
          } else {
            app.csvExport(`median_${processed.detectors[i]}` + processed.wavelengths[i] + '.' + j, processed.median[i]);
          }
        }
        for (let i = 0; i < processed.three_stdev.length; i++) {
          if (numProtocols <= 2) {
            app.csvExport(`three_stdev_${processed.detectors[i]}` + processed.wavelengths[i], processed.three_stdev[i]);
          } else {
            app.csvExport(
              `three_stdev_${processed.detectors[i]}` + processed.wavelengths[i] + '.' + j,
              processed.three_stdev[i],
            );
          }
        }
        if (calibration === 0) {
          for (let i = 0; i < processed.spad.length; i++) {
            if (numProtocols <= 2) {
              app.csvExport(`spad_${processed.detectors[i]}` + processed.wavelengths[i], processed.spad[i]);
            } else {
              app.csvExport(`spad_${processed.detectors[i]}` + processed.wavelengths[i] + '.' + j, processed.spad[i]);
            }
          }
        }
        // If this is a blank using local Environment variables, then save the medians via setEnv to 'blank'.
        if (calibration === 6) {
          const blank = {};
          blank.median = [];
          for (let i = 0; i < processed.median.length; i++) {
            blank.median.push(processed.median[i]);
          }
          app.setEnv(
            'blank',
            JSON.stringify(blank),
            'Blank, and is applied to the next standard measurement.  Reset after each measurement when it is applied.',
          ); // this will not save the Env variable on the web (only on mobile), but it will also not break :)
        }

        // VOLTAGE - Save the median and stdev from each wavelength
        if (voltage) {
          for (let i = 0; i < processed.median.length; i++) {
            if (numProtocols <= 2) {
              app.csvExport(`voltage_${processed.detectors[i]}` + processed.wavelengths[i], processed.voltage_median[i]);
            } else {
              app.csvExport(
                `voltage_${processed.detectors[i]}` + processed.wavelengths[i] + '.' + j,
                processed.voltage_median[i],
              );
            }
          }
          for (let i = 0; i < processed.three_stdev.length; i++) {
            if (numProtocols <= 2) {
              app.csvExport(
                `voltage_three_stdev_${processed.detectors[i]}` + processed.wavelengths[i],
                processed.voltage_three_stdev[i],
              );
            } else {
              app.csvExport(
                `voltage_three_stdev_${processed.detectors[i]}` + processed.wavelengths[i] + '.' + j,
                processed.voltage_three_stdev[i],
              );
            }
          }
        }

        // FOR NOW this is auto-scaling... but that makes readability hard for users... better to pre-scale consistently.
        // problem is, we need another identifier for the scale (we can base it on calibration, but when we request raw normal measurements it gets out of scale)
        // needs some work here.
        const minAvg = Math.min(...processed.median);
        const maxAvg = Math.max(...processed.median);

        // If this is a question indicating a antioxidant or polyphenol calibration should be applied and there are some calibration values stored...
        const sample_type_list = {
          polyphenols: 'userdef[8]',
          antioxidants: 'userdef[6]',
          proteins: 'userdef[6]',
          lowry_proteins: 'userdef[7]',
          //          tocopherols: 'userdef[8]',
        };
        if (typeof passed.sample_type !== 'undefined' && Object.keys(sample_type_list).includes(passed.sample_type)) {
          if (calibration === 0 &&
            typeof meas.recall !== 'undefined') {
            if (meas.recall[sample_type_list[passed.sample_type]] !== null) {
              // userdef1-10 is y intercepts, userdef11-20 are the slopes.  They are collected using the same set of wavelengths as the standard measurement
              // FYI - wavelengths = [365, 385, 450, 500, 530, 587, 632, 850, 880, 940];
              // - 587nm antioxidents / protein ;    850nm polyphenols    632 lowry protein
              // 2nd order polynomial fit (yint, and 2 slopes).  smallest slope is stored on device as value = stored_value/1000, so must multiple by 1000 here to correct.

              // if we know the ranges, we can let folks know when things are too high or too low... make sure to update this!
              const ranges = {
                polyphenols: [undefined, 12.5, 25, 50, 75, 125, 150, 200], // ug gallic acid per ml
                antioxidants: [undefined, 12.5, 25, 50, 100, 200], // uM iron equivalents
                proteins: [undefined, 250, 125, 62.5, 31.25, 15.63], // uL protein per ml
                lowry_proteins: [undefined, 50, 150, 250, 400, 500], // ug protein per mL
              };

              // Useful function to check ranges, notify user if out of range, and save results to csv
              // eslint-disable-next-line no-loop-func
              function check_range_save(value, name, saveName, units) {
                let color = 'green';
                let note = ' OK';
                if (value < 0) {
                  color = 'orange';
                  note = ' <-- if way below zero, rerun';
                } else if (value < MathMore.MathMIN(ranges[name])) {
                  color = 'orange';
                } else if (value > MathMore.MathMAX(ranges[name])) {
                  const dilute = MathMore.MathROUND(value / ((MathMore.MathMAX(ranges[name]) + MathMore.MathMIN(ranges[name])) / 2), 0);
                  app.csvExport('high_range_error', 'high_range_error');
                  color = 'red';
                  note = ` <-- redo at ${dilute}:1 dilution`;
                }

                const style = `<span style="background-color:${color};">`; // apply the default style
                ui.info('Results and Range', `This is a raw value.  This value should ALWAYS be within the range shown below.  The final value (adjusted for dilution and moisture) will be returned in the next survey.<br><br>
                ${passed.sample_type}<br><br>
                Range Min: &nbsp;${MathMore.MathMIN(ranges[name])}<br>
                This Value: ${style}${value}</span>${note}<br>
                Range Max: &nbsp;${MathMore.MathMAX(ranges[name])}`);
                app.csvExport(`${saveName}`, value);

                // recheck these to output the text... helps make the actual range min and max more visible to user this way.
                if (value < 0) {
                  ui.warning('Below zero', 'Sample value is below zero.  If slightly below zero no action is required, this may be within measurement error of zero.  If way below zero, re-run sample.');
                } else if (value < MathMore.MathMIN(ranges[name])) {
                  ui.info('Lower than range', 'Sample is lower than the lowest calibration standard.  No action needed, just FYI!');
                } else if (value > MathMore.MathMAX(ranges[name])) {
                  const dilute = MathMore.MathROUND(value / ((MathMore.MathMAX(ranges[name]) + MathMore.MathMIN(ranges[name])) / 2), 0);
                  ui.error('Higher than range', `Sample is higher than the highest calibration standard.  Unless sample is very very close to the highest standard, you should dilute roughy ${dilute}:1 and re-run.`);
                }
              }

              // Ok - now let's calculate each of the test types, check ranges, show results + save results
              if (passed.sample_type === 'polyphenols') {
                const nm850 = processed.median[7];
                const polyphenols = MathMore.MathROUND( // userdef[8]/18/28 is slopes for 850nm
                  (meas.recall['userdef[28]'] / 1000) * nm850 * nm850 +
                  meas.recall['userdef[18]'] * nm850 +
                  meas.recall['userdef[8]'],
                  3,
                );
                check_range_save(polyphenols, 'polyphenols', 'polyphenols', 'ug gallic acid per ml');
              } else if (passed.sample_type === 'antioxidants') {
                const nm587 = processed.median[5];
                const antioxidants = MathMore.MathROUND( // userdef[6]/16/26 is slopes for 587nm
                  (meas.recall['userdef[26]'] / 1000) * nm587 * nm587 +
                  meas.recall['userdef[16]'] * nm587 +
                  meas.recall['userdef[6]'],
                  3,
                );
                // antioxidants = 2; // use for testing
                check_range_save(antioxidants, 'antioxidants', 'antioxidants', 'uM iron equivalents');
              } else if (passed.sample_type === 'proteins') {
                const nm587 = processed.median[5];
                const proteins = MathMore.MathROUND( // userdef[6]/16/26 is slopes for 587nm
                  (meas.recall['userdef[26]'] / 1000) * nm587 * nm587 +
                  meas.recall['userdef[16]'] * nm587 +
                  meas.recall['userdef[6]'],
                  3,
                );
                check_range_save(proteins, 'proteins', 'proteins', 'ug protein per mL');
              } else if (passed.sample_type === 'lowry_proteins') {
                const nm632 = processed.median[6];
                const lowry_proteins = MathMore.MathROUND( // userdef[7]/17/27 is slopes for 632nm
                  (meas.recall['userdef[27]'] / 1000) * nm632 * nm632 +
                  meas.recall['userdef[17]'] * nm632 +
                  meas.recall['userdef[7]'],
                  3,
                );
                check_range_save(lowry_proteins, 'lowry_proteins', 'proteins', 'ug protein per mL');
              }
            } else {
              ui.error(`Cant find calibration for ${passed.sample_type}`, `Looked for ${sample_type_list[passed.sample_type]} on the device to apply the calibration, but couldn't find it.  Did you forget to do wet chem calibration or did you not save it?`);
            }
          }
        }
        // ///////////////////////////////////////////////////////
        // If and only if the user has a questions saying that this is a chlorophyll measurement, then display the chlorophyll + related info at the top + save info to CSV
        if (passed.is_chlorophyll === 1) {
          const spad_names = ['chlorophyll', 'carotenoids', 'anthocyanins'];
          const spad_results = [processed.spad[6], processed.spad[3], processed.spad[4]];
          for (let i = 0; i < spad_results.length; i++) {
            if (Number.isNaN(spad_results[i])) {
              spad_results[i] = -1;
              ui.warning(
                'Warning',
                spad_names + ' is out of range.  Leaf may be too thick.  Value set to -1',
              );
            }
          }
          ui.info('Chlorophyll ' + MathMore.MathROUND(processed.spad[6]), '');
          if (numProtocols <= 2) {
            app.csvExport('chlorophyll_spad', processed.spad[6]);
          } else {
            app.csvExport('chlorophyll_spad.' + j, processed.spad[6]);
          }
          ui.barchart({
              series: [spad_results],
            },
            'Leaf Information', {
              axisX: {
                labelInterpolationFnc(value, index) {
                  return `${spad_names[index]}`;
                },
              },
            },
          );
          ui.info('Carotenoids ' + MathMore.MathROUND(processed.spad[3]), '');
          ui.info('Anthocyanins ' + MathMore.MathROUND(processed.spad[4], 2), '');
          if (numProtocols <= 2) {
            app.csvExport('carotenoid_spad', processed.spad[3]);
            app.csvExport('anthocyanin_spad', processed.spad[4]);
          } else {
            app.csvExport('carotenoid_spad.' + j, processed.spad[3]);
            app.csvExport('anthocyanin_spad.' + j, processed.spad[4]);
          }
        }
        // ///////////////////////////////////////////////////////

        ui.plot({
            series: [processed.median],
          },
          'Average Spectral Values', {
            min: minAvg,
            max: maxAvg,
            axisX: {
              labelInterpolationFnc(value, index) {
                return `${wavelengths[index]}`;
              },
            },
          },
        );
        // ///////////////////////////////////////////////////////
        // Calculate CVI and NDVI, display with caveats, also display all other information
        const cvi = processed.median[9] * (processed.median[6] / processed.median[4]);
        const ndvi = (processed.median[9] - processed.median[6]) / (processed.median[9] + processed.median[6]);
        app.csvExport('cvi', cvi);
        app.csvExport('ndvi', ndvi);

        let assembleData = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ave., 3 st. dev., SPAD<br>';
        for (let i = 0; i < wavelengths.length; i++) {
          assembleData += `${wavelengths[i]}, ${sprintf('%.2f', processed.median[i])},&nbsp; ${sprintf('%.3f', processed.three_stdev[i])}, &nbsp;&nbsp;${sprintf('%.2f', processed.spad[i])}<br>`;
        }
        ui.info('Spectral values', assembleData);
        ui.info('More values (for leaves only)', `CVI, ${sprintf('%.2f', cvi)}<br>NDVI, ${sprintf('%.2f', ndvi)}`);

        ui.plot({
            series: [data],
          },
          'Raw Spectral Results', {
            //    min: 0,
            //    max: 66000,
          },
        );
        if (voltage) {
          ui.plot({
              series: [meas.voltage_raw],
            },
            'Raw Voltage Results', {
              //    min: 0,
              //    max: 66000,
            },
          );
        }
        /*
        const minStd = Math.min(...processed.three_stdev/*
        const maxStd = Math.max(...processed.three_stdev);
        ui.plot({
            series: [processed.three_stdev],
          },
          'Spectral Noise (3 st. dev.)', {
            min: minStd,
            max: maxStd,
            axisX: {
              labelInterpolationFnc(value, index) {
                return `${wavelengths[index]}`;
              },
            },
          },
        );
        */
        //        ui.info('Medians', processed.median.map(e => sprintf('%.2f,', e)).join('<br>'));
        //        ui.info('Three st. dev.', processed.three_stdev.map(e => sprintf('%.2f,', e)).join('<br>'));

        if (voltage) {
          ui.info(
            'Voltage Medians',
            processed.voltage_median.map(e => sprintf('%.2f,', e)).join('<br>'),
          );
          ui.info(
            'Voltage Three st. dev.',
            processed.voltage_three_stdev.map(e => sprintf('%.2f,', e)).join('<br>'),
          );
        }
        /*
                if (calibration === 0) {
                  ui.info('Spad', processed.spad.map(e => sprintf('%.2f,', e)).join('<br>'));
                }
        */
      } // if data_raw exists
      if (typeof meas.temperature !== 'undefined') {
        ui.info(
          'Environmental conditions',
          sprintf(
            '%.2f C, %.2f %% humidity, %.2f mB pressure, %.2f IAQ VOCs',
            meas.temperature,
            meas.humidity,
            meas.pressure,
            meas.voc,
          ),
        );
        if (numProtocols <= 2) {
          app.csvExport('temperature', meas.temperature);
          app.csvExport('humidity', meas.humidity);
          app.csvExport('pressure', meas.pressure);
          app.csvExport('voc', meas.voc);
        } else {
          app.csvExport('temperature.' + j, meas.temperature);
          app.csvExport('humidity.' + j, meas.humidity);
          app.csvExport('pressure.' + j, meas.pressure);
          app.csvExport('voc.' + j, meas.voc);
        }
      }
      // display and save toDevice if there is anything in it
    }
    if (toDevice) {
      ui.info(
        'Information about toDevice',
        'Some measurements save data to send back to the device, to update calibrations or blanks.  The following is what is currently available to save.  This is provided for information only, and your device information is not changed unless you run another measurement after this one to actually save it.',
      );
      ui.info('Contents of toDevice', toDevice);
      app.csvExport('toDevice', toDevice);
      app.setEnv('toDevice', toDevice, 'information to pass to device');
    }
  }
})();
// final save of all app.csvExport calls -- make sure this is always at the very end!
app.save();